function extendJquery() {
    jQuery.fn.htmlClean = function() {
        this.contents().filter(function() {
            if (this.nodeType != 3) {
                $(this).htmlClean();
                return false;
            }
            else {
                this.textContent = $.trim(this.textContent);
                return !/\S/.test(this.nodeValue);
            }
        }).remove();
        return this;
    }
}

function initSearchBar() {
    var $search = $('.basic-header .toolbar-item.search');
    var $searchInput = $search.find('input');
    // var $searchButton = $search.find('button');

    $search.mouseenter(function () {
        $searchInput.animate({
            "margin-left": '0',
            "margin-right": '10px'
        });
    })
}

function initSlider() {
    var $slider = $("#slider");

    if ($slider.length > 0) {
        var $sliderControls = $("#slider-controls");

        $slider.slick({
            arrows: false,
            dots: true,
            appendDots: $sliderControls,
            dotsClass: 'dots'
        });
        $sliderControls.find('.prev').click(function(){
            $slider.slick('slickPrev');
        });
        $sliderControls.find('.next').click(function(){
            $slider.slick('slickNext');
        })
    }
}

function initFilters() {
    var $filtersSections = $('.filters-section.toggled');
    var $filtersSectionsList = $filtersSections.find('.filters-section-list');

    $('.filters-section.toggled .filters-section-title').click(function () {
        var $filtersSection = $(this).parents('.filters-section');
        var $filtersSectionList = $filtersSection.find('.filters-section-list');

        $filtersSectionsList.not($filtersSectionList).slideUp();
        var isOpened = $filtersSection.hasClass('opened');
        if (isOpened) {
            $filtersSectionList.slideUp();
        } else {
            $filtersSectionList.slideDown();
        }

        $filtersSections.not($filtersSection).toggleClass('opened', false);
        $filtersSection.toggleClass('opened', !isOpened);
    })
}

function initMatchHeight() {
    var options = {
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    };

    $('.previews-item .tags .tags-list').matchHeight(options);
    $('.previews-item .heading').matchHeight(options);

}

function initTagsCutting() {
    var $tagList = $('.tags-list:not(.wrap)');

    $tagList.each(function (index, singleTagList) {
        var tagListWidth = $(singleTagList).innerWidth();
        var tagsWidth = 0;

        $(singleTagList).find('.tags-item')
            .each(function (index, tagsItem) {
                    if (tagsWidth > tagListWidth) {
                        $(tagsItem).hide();
                    }

                    var lastTagsWidth = tagsWidth;
                    var width = $(tagsItem).outerWidth(true) + parseFloat($(tagsItem).css('marginLeft')) + parseFloat($(tagsItem).css('marginRight'));
                    tagsWidth += width;

                    var withOverflow = tagListWidth - tagsWidth;

                    if (withOverflow < 0) {
                        var tagsItemText = $(tagsItem).find('a').text();
                        var catLength = Math.floor((tagListWidth - lastTagsWidth)/($(tagsItem).innerWidth()/tagsItemText.length)) - 2;

                        if (catLength < 2) {
                            $(tagsItem).hide();
                        } else {
                            // $(tagsItem).data('text', tagsItemText);
                            $(tagsItem).find('a').text(tagsItemText.slice(0, catLength) + '...');
                        }

                    }
                }
            )
    })
}

function initSpeechGenerator() {
    var speechComponent = {
        view: null,
        volumeIndicator: null,
        volumeValue: 75,
        init: function () {
            var self = this;
            responsiveVoice.cancel();
            responsiveVoice.setDefaultVoice("Russian Female");
            responsiveVoice.speech_onstart = function () {
                self.preloader('hide');
            };
            responsiveVoice.speech_onerror= function () {
                self.preloader('hide');
                self.ready();
            };
            responsiveVoice.speech_onend = function (e) {
                if(this.rvIndex >= this.rvTotal - 1) {
                    self.ready();
                }
            };
            this.volumeIndicator = this.view.find('.speech-generator-volume-indication');
            this.volumeLevel();

            this.view.find('.play-pause').click(function () {
                if (self.getState() == 'ready') {
                    self.generation();
                } else if (self.getState() == 'generation') {
                    self.paused();
                } else if (self.getState() == 'paused') {
                    self.generation('resume');
                }
            });
            this.view.find('.stop').click(function () {
                if (self.getState() == 'ready') return;

                self.ready('resume');
            });
            this.view.find('.volume-up').click(function () {
                if (self.getState() == 'ready') return;

                speechComponent.volumeUp();
            });
            this.view.find('.volume-down').click(function () {
                if (self.getState() == 'ready') return;

                speechComponent.volumeDown();
            });

            this.view.show();
        },
        getState: function () {
            if (this.view.hasClass('ready')) {
                return 'ready';
            } else if (this.view.hasClass('generation')) {
                return 'generation';
            } else if (this.view.hasClass('paused')) {
                return 'paused';
            }
        },
        ready: function () {
            responsiveVoice.cancel();
            this.view
                .toggleClass('ready', true)
                .toggleClass('generation', false)
                .toggleClass('paused', false);
        },
        generation: function (option) {
            if (option == 'resume') {
                responsiveVoice.resume();
            } else {
                this.preloader('show');
                responsiveVoice.speak(text);
            }

            this.view
                .toggleClass('ready', false)
                .toggleClass('generation', true)
                .toggleClass('paused', false);
        },
        paused: function () {
            responsiveVoice.pause();
            this.view
                .toggleClass('ready', false)
                .toggleClass('generation', false)
                .toggleClass('paused', true);
        },
        volumeUp: function () {
            this.volumeLevel(this.volumeValue + 5);
        },
        volumeDown: function () {
            this.volumeLevel(this.volumeValue - 5);
        },
        volumeLevel: function (value) {
            var volumeValue = value || this.volumeValue;

            if (value > 100) {
                this.volumeValue = 100;
            } else if (value < 0) {
                this.volumeValue = 0;
            } else {
                this.volumeValue = volumeValue;
            }

            this.volumeIndicator.text(this.volumeValue + '%');
            responsiveVoice.setVolume(0.01 * this.volumeValue);
        },
        preloader: function (state) {
            this.view.toggleClass('wait', state === 'show');
        }
    };


    var $speechSource = $('[data-text-to-speech]');
    var text = $.trim($speechSource.clone().htmlClean().text());

    if ($speechSource.length == 0 || $speechSource.length == 0 || !text) {
        return;
    }

    speechComponent.view = $('.speech-generator');
    speechComponent.init();
}

function initTabs() {
    $('.nav-tabs a').click(function(){
        $(this).tab('show');
    });
}

function initSocialLinks() {
    var $socialLinks = $('.social-button');

    $socialLinks.click(function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        if (!href) {
            return;
        }

        window.open(href,'sharer','toolbar=0,status=0,width=648,height=395');
        return true;
    })
}

function adjustFooterPosition() {
    var $wrapper = $('.basic-wrapper');
    var $footer = $('.basic-footer');
    var footerHeight = $footer.outerHeight();

    $wrapper.css({
        paddingBottom: footerHeight + 'px'
    });
    $footer.css({
        marginTop: -footerHeight + 'px'
    });
}

function resizeWindow() {
    adjustFooterPosition();
    initMatchHeight();
}

function initTooltips() {
    $('[data-toggle="tooltip"]').tooltip();
}

function initLikesButtons(){
    var sendReactionRequest = function(item, reaction){
        var news_id = item.data('news_id');
        $.ajax({
            url: '/news/' + reaction + '/' + news_id,
            method: 'get'
        }).done(function(resp){
            if (resp && resp.result) {
                var $el = $('#news_like_reactions_'+news_id);
                $el.find('span.likes_counter').text(resp.likes);
                $el.find('span.dislikes_counter').text(resp.dislikes);
                $el.find('#news_dislike_'+news_id).removeClass('active');
                $el.find('#news_like_'+news_id).removeClass('active');
                $el.find('#news_'+reaction+'_'+news_id).addClass('active');
            }
        });
    };
    $('#news_container').delegate('.likes', 'click', function(event){
        var $that = $(this);
        event.preventDefault();
        sendReactionRequest($that, 'like');
        return false;
    });
    $('#news_container').delegate('.dislikes', 'click', function(event){
        var $that = $(this);
        event.preventDefault();
        sendReactionRequest($that, 'dislike');
        return false;
    });
}

function initNewsLoadingControls(){
    var last_shown_news_id = 0;
    var filters_set = {};
    var updateLastShownNewsId = function (){
        var id = $('.previews-item').last().data('news_id');
        last_shown_news_id = id ? id : 0;
    };
    var filter_updated_callback = function(){
        filters_set = {};
        last_shown_news_id = 0;
        $('#news_container_row').html('');
        $('#filters-form input:checked').each(function(){
            var id = $(this).data('id');
            var subj = $(this).data('subj');
            if (!filters_set[subj]) filters_set[subj] = [];
            filters_set[subj].push(id);
        });
        load_more_news();
        $('#load_more_news_btn').toggle(true);
    };
    var load_more_news = function(){
        $.ajax({
            url: '/ajax/news/filter/' + last_shown_news_id,
            method: 'get',
            data: filters_set
        }).done(function(resp){
            if (resp) {
                if (resp.count > 0){
                    $('#news_container_row').append(resp.content);
                    initMatchHeight();
                    updateLastShownNewsId();
                } else {
                    $('#load_more_news_btn').toggle(false);
                    $('#news_container_row').append($(
                        '<div class="col-lg-12 col-sm-12">' +
                            '<h3>Новостей, соответствующих заданному фильтру, не найдено</h3>'+
                        '</div>'
                    ));
                }
            }
        });
    };

    $('#load_more_news_btn').on('click', load_more_news);
    $('#filters-form input').on('change', filter_updated_callback);
    updateLastShownNewsId();
};

function initBookmarkButton(){
    var toggleBookmarkRequest = function($element, news_id){
        $.ajax({
            url: '/ajax/my_account/bookmark/' + news_id,
            method: 'get',
        }).done(function(resp){
            if (resp && resp.status) {
                $element.toggleClass('active', resp.status == 'created')
            }
        });
    };

    $('.metadata').delegate('.bookmark', 'click', function(event){
        event.preventDefault();
        var $that = $(this);
        var news_id = $that.data('news_id');
        if (news_id) toggleBookmarkRequest($that, news_id);
    })
}

function initChart() {

    var data = {
        labels: [
        ],
        datasets: [
            {
                data: [],
                backgroundColor: [
                    "#3366CC",
                    "#DC3912",
                    "#FF9900",
                    "#109618",
                    "#990099",
                    "#3B3EAC",
                    "#0099C6",
                    "#DD4477",
                    "#66AA00",
                    "#B82E2E",
                    "#316395",
                    "#994499",
                    "#22AA99",
                    "#AAAA11",
                    "#6633CC",
                    "#E67300",
                    "#8B0707",
                    "#329262",
                    "#5574A6",
                    "#3B3EAC",
                ],
                hoverBackgroundColor: [
                    "#3366CC",
                    "#DC3912",
                    "#FF9900",
                    "#109618",
                    "#990099",
                    "#3B3EAC",
                    "#0099C6",
                    "#DD4477",
                    "#66AA00",
                    "#B82E2E",
                    "#316395",
                    "#994499",
                    "#22AA99",
                    "#AAAA11",
                    "#6633CC",
                    "#E67300",
                    "#8B0707",
                    "#329262",
                    "#5574A6",
                    "#3B3EAC",
                ]
            }]
    };

    var ctx = document.getElementById("myChart");

    if (!ctx) return false;

    var options = {
        maintainAspectRatio: false,
        animation:{
            animateScale:true
        },
        legend: {
            position: 'right',
            labels: {
                boxWidth: 10
            }
        }
    };

    $.ajax({
        url: '/ajax/my_account/party_stats',
        method: 'get',
    }).done(function(resp){
        if (resp) {
            data.labels = resp.keys;
            data.datasets[0].data = resp.values;

            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options
            });
        }
    });


}

function init() {
    extendJquery();
    initSearchBar();
    initSlider();
    initFilters();
    initMatchHeight();
    initTagsCutting();
    initSpeechGenerator();
    initTabs();
    initSocialLinks();
    resizeWindow();
    initTooltips();
    initNewsLoadingControls();
    initLikesButtons();
    initBookmarkButton();
    initChart();
    $(window).resize(resizeWindow);
}

$(document).ready(init);
