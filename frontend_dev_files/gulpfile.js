var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var merge = require('merge-stream');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-clean-css');
var jsmin = require('gulp-uglify');
var webserver = require('gulp-webserver');
var livereload = require('gulp-livereload');

var path = {
    html: [
        './*.html'
    ],
    styles: {
        src: {
            vendors: [
                './node_modules/bootstrap/dist/css/bootstrap.css',
                './node_modules/font-awesome/css/font-awesome.css',
                './node_modules/slick-carousel/slick/slick.css'
            ],
            custom: [
                './styles/index.sass'
            ]
        },
        // dependencies: [
        //     './styles/'
        // ],
        name: 'styles',
        dest: '../public/css'
    },
    scripts: {
        src: [
            './node_modules/jquery/dist/jquery.js',
            './node_modules/bootstrap/dist/js/bootstrap.js',
            './node_modules/slick-carousel/slick/slick.min.js',
            './node_modules/jquery-match-height/dist/jquery.matchHeight.js',
            './node_modules/chart.js/dist/chart.min.js',
            './scripts/index.js'
        ],
        name: 'scripts',
        dest: '../public/js'
    },
    fonts: {
        src: {
            vendors: [
                './node_modules/font-awesome/fonts/*',
            ],
            custom: []
        },
        dest: './fonts/'
    }
};
var watchersPath = {
    html: [
        './*.html'
    ],
    styles: [
        './styles/*.sass'
    ],
    scripts: [
        './scripts/*.js'
    ]
};

gulp.task('html', function () {
    return gulp.src(path.html)
        .pipe(livereload());
});


gulp.task('styles', function () {
    var vendorsStream;
    var customStream;

    vendorsStream = gulp.src(path.styles.src.vendors);

    customStream = gulp.src(path.styles.src.custom)
        .pipe(concat('styles.sass'))
        .pipe(sass());

    return merge(vendorsStream, customStream)
        .pipe(concat('styles.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename(path.styles.name + '.css'))
        .pipe(gulp.dest(path.styles.dest))
        .pipe(cssmin())
        .pipe(rename(path.styles.name + '.min.css'))
        .pipe(gulp.dest(path.styles.dest))
        .pipe(livereload());
});

gulp.task('scripts', function () {
    return gulp.src(path.scripts.src)
        .pipe(concat('scripts.js'))
        .pipe(rename(path.scripts.name + '.js'))
        .pipe(gulp.dest(path.scripts.dest))
        .pipe(jsmin())
        .pipe(rename(path.scripts.name + '.min.js'))
        .pipe(gulp.dest(path.scripts.dest))
        .pipe(livereload());
});

gulp.task('fonts', function () {
    return gulp.src(path.fonts.src.vendors)
        .pipe(gulp.dest(path.fonts.dest))
        .pipe(livereload());
});

gulp.task('webserver', function() {
    gulp.src('./')
        .pipe(webserver({
            livereload: true,
            open: true
        }));
});

gulp.task('watch', ['styles', 'scripts', 'webserver'], function() {
    livereload.listen();
    gulp.watch(watchersPath.html, ['html']);
    gulp.watch(watchersPath.styles, ['styles']);
    gulp.watch(watchersPath.scripts, ['scripts']);
});

gulp.task('build', ['styles', 'scripts', 'fonts']);

gulp.task('default', ['build']);


