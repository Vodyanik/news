@extends('backend.layout')

@section('title', 'Источники новостей')

@section('content')
    <div class="col-md-12">
        {{
            Form::model(
                $news_source,
                [
                    'route' => $news_source->exists ? ['news_source.update',$news_source->id] : 'news_source.store',
                    'method' => $news_source->exists ? 'PATCH' : 'POST',
                    'class' => 'form-horizontal',
                    'files' => true
                ]
            )

        }}

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('name', 'Название источника', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('name', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label($news_source->logo_form_name, 'Логотип', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                @if($news_source->logo_url)
                    <img src="{{$news_source->logo_url}}" alt="">
                @endif
                {{ Form::file($news_source->logo_form_name) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('site_url', 'Web адрес источника') }}
            </div>
            <div class="col-md-10">
                {{ Form::text('site_url', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('use_fake_rating', 'Использовать фейковый рейтинг') }}
            </div>
            <div class="col-md-10">
                {{ Form::checkbox('use_fake_rating', 1, null, ['class' => 'checkbox']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('fake_rating', 'Фейковый рейтинг') }}
            </div>
            <div class="col-md-10">
                {{ Form::number('fake_rating', 0, ['class' => 'form-control', 'step' => '0.1', 'min' => 0, 'max' => 100]) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::submit('Сохранить') }}
        </div>


        {{ Form::close() }}
    </div>
@endsection