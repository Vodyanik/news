@extends('backend.layout')

@section('title', 'Источники новостей')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('news_source.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>id <a href="{!! route('news_source.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Название <a href="{!! route('news_source.index', array_merge($pagination_params,['sort_by'=> 'name'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Ссылка</td>
                <td>Подтвержденных фактов</td>
                <td>Всего новостей</td>
                <td>Рейтинг  <a href="{!! route('news_source.index', array_merge($pagination_params,['sort_by'=> 'rating'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Лайки</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[name]', $search_fields['name'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[site_url]', $search_fields['site_url'], ['class' => 'form-control']) !!}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

                <td>
                    {!! Form::submit('Найти', ['class' => 'form-control btn btn-primary btn-sm']) !!}
                </td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($news_sources as $item)
                <tr>
                    <td>{!! $item['id'] !!}</td>
                    <td>{!! $item['name'] !!}</td>
                    <td><a href="http://{!! $item['site_url'] !!}">{!! $item['site_url'] !!}</a></td>
                    <td>{!! Faker\Factory::create()->randomNumber(2) !!}</td>
                    <td>{!! $item->news->count() !!}</td>
                    <td>{!! $item['rating'] !!}</td>
                    <td>
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                        {{$item->news()->withCount('likes')->get()->sum('likes_count')}}
                        /
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                        {{$item->news()->withCount('dislikes')->get()->sum('dislikes_count')}}
                    </td>
                    <td>
                        {!! Form::model($item, ['route' => ['news_source.edit', $item['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($item, ['route' => ['news_source.destroy', $item['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $news_sources->appends($pagination_params)->links() !!}
    </div>
@endsection