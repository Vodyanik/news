@extends('backend.layout')

@section('title', 'Комментарии')

@section('content')
    <div class="col-md-12">
        <div class="row">
            {{--<div class="col-md-10">--}}
                {{--<a href="{!! route('news.create') !!}" class="btn btn-default">Создать</a>--}}
            {{--</div>--}}
            {{--<div class="col-md-2 text-right">--}}
                {{--<a href="{!! route('news_changes_log.index') !!}" class="btn btn-default right">История изменений</a>--}}
            {{--</div>--}}
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>Id <a href="{!! route('comments.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td class="col-md-1">News ID: <a href="{!! route('comments.index', array_merge($pagination_params,['sort_by'=> 'news_id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Имя коментатора</td>
                <td>Комментарий</td>
                <td class="col-md-1">Создано: <a href="{!! route('comments.index', array_merge($pagination_params,['sort_by'=> 'created_at'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td class="col-md-1">Изменено: <a href="{!! route('comments.index', array_merge($pagination_params,['sort_by'=> 'updated_at'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Статус <a href="{!! route('comments.index', array_merge($pagination_params,['sort_by'=> 'status'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td width="50px">
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    {!! Form::submit('Найти', ['class' => 'form-control btn btn-primary']) !!}
                </td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($comments as $item)
                <tr>
                    <td>{!! $item->id !!}</td>
                    <td>{!! $item->news->id !!}</td>
                    <td>{!! $item->user->name !!}</td>
                    <td>{!! $item->text !!}</td>
                    <td>{!! $item->created_at !!}</td>
                    <td>{!! $item->updated_at !!}</td>
                    <td>{!! $item->status !!}</td>
                    <td>
                        {!! Form::model($item, ['route' => ['comments.edit', $item['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'form-control btn btn-warning btn', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($item, ['route' => ['comments.destroy', $item['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'form-control btn btn-danger btn', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $comments->appends($pagination_params)->links() !!}
    </div>
@endsection