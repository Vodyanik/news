@extends('backend.layout')

@section('title', 'Сферы деятельности')

@section('content')
    <div class="col-md-12">
        <form action="{{ route('comments.update', ['id' => $comment->id]) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group row">
                <label for="exampleTextarea">Комментарий: </label>
                <textarea class="form-control" name="text" id="exampleTextarea" rows="3">{{ $comment->text }}</textarea>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Статус</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <label class="form-check-label">
                            @if($comment->status == 0)
                                <input class="form-check-input" name="status" type="checkbox"> Не опубликован
                            @else
                                <input class="form-check-input" name="status" type="checkbox" checked> Опубликован
                            @endif
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                </div>
            </div>
        </form>
    </div>
@endsection