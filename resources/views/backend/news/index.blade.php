@extends('backend.layout')

@section('title', 'Новости')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <a href="{!! route('news.create') !!}" class="btn btn-default">Создать</a>
            </div>
            <div class="col-md-2 text-right">
                <a href="{!! route('news_changes_log.index') !!}" class="btn btn-default right">История изменений</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>Id <a href="{!! route('news.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Заголовок <a href="{!! route('news.index', array_merge($pagination_params,['sort_by'=> 'title'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Аннотация <a href="{!! route('news.index', array_merge($pagination_params,['sort_by'=> 'preview'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td class="col-md-1">Создано: <a href="{!! route('news.index', array_merge($pagination_params,['sort_by'=> 'created_at'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td class="col-md-1">Изменено: <a href="{!! route('news.index', array_merge($pagination_params,['sort_by'=> 'updated_at'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Статус <a href="{!! route('news.index', array_merge($pagination_params,['sort_by'=> 'status'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Рейтинг <a href="{!! route('news.index', array_merge($pagination_params,['sort_by'=> 'rating'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[title]', $search_fields['title'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[preview]', $search_fields['preview'], ['class' => 'form-control']) !!}
                </td>
                <td></td>
                <td></td>
                <td>
                    {!! Form::select(
                            'search_fields[status]',
                            [
                                '' => null,
                                'new' => 'new',
                                'edited' => 'edited',
                                'published' => 'published'
                            ],
                            $search_fields['status'],
                            ['class' => 'form-control']
                        )
                    !!}
                </td>
                <td></td>
                <td>
                    {!! Form::submit('Найти', ['class' => 'form-control btn btn-primary']) !!}
                </td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($news as $item)
                <tr>
                    <td>{!! $item->id !!}</td>
                    <td>{!! $item->title !!}</td>
                    <td>{!! $item->preview !!}</td>
                    <td>{!! $item->created_at !!}</td>
                    <td>{!! $item->updated_at !!}</td>
                    <td>{!! $item->status !!}</td>
                    <td>
                        {!! $item->rating !!}
                        <br>
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>{{$item->likes_count}}
                        /
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i> {{$item->dislikes_count}}
                    </td>
                    <td>
                        {!! Form::model($item, ['route' => ['news.edit', $item['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'form-control btn btn-warning btn', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($item, ['route' => ['news.destroy', $item['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'form-control btn btn-danger btn', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $news->appends($pagination_params)->links() !!}
    </div>
@endsection