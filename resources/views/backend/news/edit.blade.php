@extends('backend.layout')

@section('title', 'Новости')

@section('content')
    <div class="col-md-12">
        {{
            Form::model(
                $news,
                [
                    'route' => $news->exists ? ['news.update',$news->id] : 'news.store',
                    'method' => $news->exists ? 'PATCH' : 'POST',
                    'class' => 'form-horizontal',
                    'id' => 'news_form',
                    'files' => true
                ]
            )

        }}

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('title', 'Заголовок', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('title', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('tags', 'Теги', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('tags', $tags->pluck('name', 'id'), $news->tags->pluck('id')->toArray(), ['multiple', 'name' => 'tags[]', 'class' => 'form-control chosen', 'id' => 'tag_select']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('regions', 'Регионы', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('regions', \App\Region::getSelectOptions(), $news->regions->pluck('id')->toArray(), ['multiple', 'name' => 'regions[]', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('activity_fields', 'Сферы', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('activity_fields', $activity_fields->pluck('name', 'id'), $news->activity_fields->pluck('id')->toArray(), ['multiple', 'name' => 'activity_fields[]', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('parties', 'Партии', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('parties', $parties->pluck('name', 'id'), $news->parties->pluck('id')->toArray(), ['multiple', 'id' => 'parties_multiselect', 'name' => 'parties[]', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('organizations', 'Организации', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('organizations', $organizations->pluck('name', 'id'), $news->organizations->pluck('id')->toArray(), ['multiple', 'id' => 'organizations_multiselect', 'name' => 'organizations[]', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('politicians', 'Политики', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('politicians', $politicians->pluck('full_name', 'id'), $news->politicians->pluck('id')->toArray(), ['multiple', 'id' => 'politicians_multiselect', 'name' => 'politicians[]', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('confirmed_promises', 'Выполненные обещания', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('confirmed_promises', [], [], ['multiple', 'id' => "confirmed_promises", 'name' => 'confirmed_promises[]', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('failed_promises', 'Проваленные обещания', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('failed_promises', [], [], ['multiple', 'id' => "failed_promises", 'name' => 'failed_promises[]', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('news_source_id', 'Источник новости', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('news_source_id', $news_sources->pluck('name', 'id'), null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('original_news_url', 'URL оригинальной новости', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('original_news_url', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('fact_checking_level', 'Уровень достоверности', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('fact_checking_level', ['high'=>'Высокий','medium'=>'Средний','low'=>'Низкий'], $news->fact_checking_level, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('news_image', 'Изображение') }}
            </div>
            <div class="col-md-10">
                @if(!empty($news->image))
                    <img src="{{$news->image}}" alt="">
                @endif
                {{ Form::file($news->image_form_name) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('preview', 'Аннотация') }}
            </div>
            <div class="col-md-10">
                {{ Form::hidden('preview', null, ['class' => 'form-control', 'id' => 'preview_html']) }}
                <div id="news_preview" style="height: 200px">
                    {!! $news->preview !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('body', 'Текст новости') }}
            </div>
            <div class="col-md-10">
                {{ Form::hidden('body', null, ['class' => 'form-control', 'id' => 'body_html']) }}
                <div id="news_body" style="height: 400px">
                    {!! $news->body !!}
                </div>
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('status', 'Статус', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('status', ['new' => 'Новая', 'edited' => 'Отредактированная', 'published' => 'Опубликованная'], $news->status, ['name'=>'status', 'class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('show_on_slider', 'Показывать на слайдере', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::checkbox('show_on_slider', 1, null, ['class' => 'checkbox']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('publication_date', 'Дата публикации', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('publication_date', null, ['class' => 'form-control datetimepicker']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::submit('Сохранить') }}
        </div>

        {{ Form::close() }}
    </div>

    <div id="create_tag_dlg" style="display: none">
        <div class="vue_app">
            <tag-editor></tag-editor>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            var quill_config = {
                modules: {
                    'syntax': true,
                    'toolbar': [
                        [{ 'size': [] }],
                        [ 'bold', 'italic', 'underline', 'strike' ],
                        [{ 'color': [] }, { 'background': [] }],
                        [{ 'script': 'super' }, { 'script': 'sub' }],
                        [{ 'header': '1' }, { 'header': '2' }, 'blockquote', 'code-block' ],
                        [{ 'list': 'ordered' }, { 'list': 'bullet'}, { 'indent': '-1' }, { 'indent': '+1' }],
                        [ 'direction', { 'align': [] }],
                        [ 'link', 'image', 'video', 'formula' ],
                        [ 'clean' ]
                    ],
                },
                theme: 'snow'
            };
            var quill_preview = new Quill('#news_preview', quill_config);
            var quill_body = new Quill('#news_body', quill_config);
            var $create_tag_dlg = $('#create_tag_dlg');

            $('#news_form').on('submit', function(e){
                $('#preview_html').val(quill_preview.root.innerHTML);
                $('#body_html').val(quill_body.root.innerHTML);
            });

            $('#tag_select').on('chosen:no_results', function () {
                $('#tag_select_chosen .no-results').html(
                    $('#tag_select_chosen .no-results').html() +
                    ' <span class="btn btn-success btn-xs" id="create_tag"><span class="glyphicon glyphicon-plus-sign"></span> Создать</span>'
                );
            });
            $('#tag_select_chosen').delegate('#create_tag', 'click', function () {
                $create_tag_dlg.dialog();
            });
            $('body').on('tag_created', function(event, data){
                var $new_tag = $('<option value="'+data.id+'">'+data.name+'</option>').prop('selected', true);
                $('#tag_select').append($new_tag).trigger('chosen:updated');
                $create_tag_dlg.dialog('close');
            });
            $('body').on('tag_creation_error', function(event, data){
                $create_tag_dlg.dialog('close');
                alert('Не могу создать тег');
            });

            function loadPromises(){
                var parties = $('#parties_multiselect').val();
                var politicians = $('#politicians_multiselect').val();
                var organizations = $('#organizations_multiselect').val();

                $.ajax({
                    method: 'get',
                    url: '{!! route('api.news.getPromises') !!}',
                    data: {
                        parties: parties,
                        politicians: politicians,
                        organizations: organizations,
                        news_id: '{!! $news->id !!}'
                    }
                }).done(function(resp){
                    if (resp) {
                        var $confirmed_select = $('#confirmed_promises');
                        var $failed_select = $('#failed_promises');
                        $confirmed_select.html('');
                        $failed_select.html('');

                        var news_promises = resp.news_promises;

                        $.each(['parties', 'politicians', 'organizations'], function(k, v){
                            $.each(resp[v], function (subject_name, promises) {
                                var $confirmed_group = $('<optgroup label="'+ subject_name +'"></optgroup>');
                                var $failed_group = $('<optgroup label="'+ subject_name +'"></optgroup>');
                                $.each(promises, function(id, data){
                                    var $el = $('<option value="'+ id +'">' + data.description + '</option>');
                                    if (news_promises.indexOf(parseInt(id)) >= 0) {
                                        $el = $el.prop('selected', data.confirmed);
                                    }
                                    $confirmed_group.append($el);

                                    var $el = $('<option value="'+ id +'">' + data.description + '</option>');
                                    if (news_promises.indexOf(parseInt(id)) >= 0) {
                                        $el = $el.prop('selected', data.failed);
                                    }
                                    $failed_group.append($el);
                                });
                                $confirmed_select.append($confirmed_group);
                                $failed_select.append($failed_group);
                            });
                        });

                        $confirmed_select.trigger('chosen:updated');
                        $failed_select.trigger('chosen:updated');
                    }
                })
            }

            $('#parties_multiselect, #politicians_multiselect, #organizations_multiselect').on('change', loadPromises);
            loadPromises();
        });
    </script>
@endsection