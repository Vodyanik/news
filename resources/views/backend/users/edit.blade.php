@extends('backend.layout')

@section('title', 'Политик')

@section('content')
    <div class="col-md-12">
        {{
            Form::model(
                $user,
                [
                    'route' => $user->exists ? ['users.update',$user->id] : 'users.store',
                    'method' => $user->exists ? 'PATCH' : 'POST',
                    'class' => 'form-horizontal',
                    'id' => 'user_form'
                ]
            )

        }}

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('name', 'Имя', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('name', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('email', 'E-mail', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('email', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('password', 'Пароль', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::password('password', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('role', 'Роль', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('role', App\User::$role_mapping, null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <hr>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('birthday', 'Дата рождения', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::date('birthday', $user->birthday, ['class' => 'form-control datepicker']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('education', 'Образование', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('education', App\User::$education_mapping, null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('income', 'Среднемесячный доход', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('income', App\User::$income_mapping, null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('family_status', 'Семейное положение', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('family_status', App\User::$family_status_mapping, null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('children', 'Дети', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('children', App\User::$children_mapping, null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('employment', 'Трудоустройство', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('employment', App\User::$employment_mapping, null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group{{ $errors->has('employment_spheres_id') ? ' has-error' : '' }}">
            <div class="col-md-2">
                {{ Form::label('employment_spheres_id', 'Сфера деятельности', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('employment_spheres_id', App\EmploymentSphere::all()->pluck('name', 'id'), null, ['class' => 'form-control chosen']) }}
            </div>

        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('election_activity', 'Активность на выборах', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('election_activity', App\User::$election_activity_mapping, null, ['class' => 'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('settlement_type', 'Место жительства', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('settlement_type', App\User::$settlement_type_mapping, $user->settlement_type, ['class' => 'form-control chosen']) }}
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('region_id', 'Область', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('region_id', App\Region::all()->pluck('name', 'id'), null, ['class'=>'form-control chosen']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::submit('Сохранить') }}
        </div>


        {{ Form::close() }}
    </div>


    <script>

    </script>
@endsection