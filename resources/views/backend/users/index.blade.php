@extends('backend.layout')

@section('title', 'Пользователи')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('users.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>id <a href="{!! route('users.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Имя <a href="{!! route('users.index', array_merge($pagination_params,['sort_by'=> 'name'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>E-mail <a href="{!! route('users.index', array_merge($pagination_params,['sort_by'=> 'email'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Дата рождения <a href="{!! route('users.index', array_merge($pagination_params,['sort_by'=> 'birthday'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Роль <a href="{!! route('users.index', array_merge($pagination_params,['sort_by'=> 'role'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[name]', $search_fields['name'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[email]', $search_fields['email'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[birthday]', $search_fields['birthday'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::select('search_fields[role]', array_merge([''], \App\User::$role_mapping), $search_fields['role'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>{!! Form::submit('Найти', ['class' => 'form-control btn btn-primary']) !!}</td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{!! $user['id'] !!}</td>
                    <td>{!! $user['name'] !!}</td>
                    <td>{!! $user['email'] !!}</td>
                    <td>{!! $user['birthday'] ? $user->birthday->toDateString() : ' - ' !!}</td>
                    <td>{!! $user['role'] !!}</td>
                    <td>
                        {!! Form::model($user, ['route' => ['users.edit', $user['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($user, ['route' => ['users.destroy', $user['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $users->links() !!}
    </div>
@endsection