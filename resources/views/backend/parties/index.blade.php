@extends('backend.layout')

@section('title', 'Партии')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('party.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>
                    id
                    <a href="{!! route('party.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a>
                </td>
                <td>
                    Название
                    <a href="{!! route('party.index', array_merge($pagination_params,['sort_by'=> 'name'])) !!}"><span class="glyphicon glyphicon-sort"></span></a>
                </td>
                <td>
                    Дата создания
                    <a href="{!! route('party.index', array_merge($pagination_params,['sort_by'=> 'creation_date'])) !!}"><span class="glyphicon glyphicon-sort"></span></a>
                </td>
                <td>
                    Сайт
                    <a href="{!! route('party.index', array_merge($pagination_params,['sort_by'=> 'official_site_url'])) !!}"><span class="glyphicon glyphicon-sort"></span></a>
                </td>
                <td>Рейтинг</td>
                <td>
                    Число обещаний
                    <a href="{!! route('party.index', array_merge($pagination_params,['sort_by'=> 'promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a>
                </td>
                <td>
                    Выполнено обещаний
                    <a href="{!! route('party.index', array_merge($pagination_params,['sort_by'=> 'confirmed_promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a>
                </td>
                <td>
                    Провалено обещаний
                    <a href="{!! route('party.index', array_merge($pagination_params,['sort_by'=> 'failed_promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a>
                </td>
                <td colspan="2"></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[name]', $search_fields['name'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[creation_date]', $search_fields['creation_date'], ['class' => 'form-control']) !!}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    {!! Form::submit('Найти', ['class' => 'form-control btn btn-primary']) !!}
                </td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($parties as $party)
                <tr>
                    <td>{!! $party['id'] !!}</td>
                    <td>{!! $party['name'] !!}</td>
                    <td>{!! $party['creation_date'] !!}</td>
                    <td>{!! $party['official_site_url'] !!}</td>
                    <td>{!! $party->rounded_rating !!}</td>
                    <td>{!! $party->promises_count !!}</td>
                    <td>{!! $party->confirmedPromises->count() !!}</td>
                    <td>{!! $party->failedPromises->count() !!}</td>
                    <td>
                        {!! Form::model($party, ['route' => ['party.edit', $party['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($party, ['route' => ['party.destroy', $party['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $parties->appends($pagination_params)->links() !!}
    </div>
@endsection