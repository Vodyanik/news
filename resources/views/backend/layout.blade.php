<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous"> <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/chosen.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/quill.snow.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/jquery.datetimepicker.min.css') !!}">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.8.0/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.8.0/highlight.min.js"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="{!! asset('js/app.js') !!}"></script>
    <script src="{!! asset('js/chosen.jquery.js') !!}"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        @if(Auth::check() && Auth::user()->is_backend_user)
        <ul class="nav navbar-nav">
            <li @if(\Request::route()->getName() == 'news_source.index')class="active"@endif>
                <a href="{!! route('news_source.index') !!}">Источники новостей</a>
            </li>
            <li @if(\Request::route()->getName() == 'tags.index')class="active"@endif>
                <a href="{!! route('tags.index') !!}">Теги</a>
            </li>
            <li @if(\Request::route()->getName() == 'news.index')class="active"@endif>
                <a href="{!! route('news.index') !!}">Новости</a>
            </li>
            <li @if(\Request::route()->getName() == 'politician.index')class="active"@endif>
                <a href="{!! route('politician.index') !!}">Политики</a>
            </li>
            <li @if(\Request::route()->getName() == 'party.index')class="active"@endif>
                <a href="{!! route('party.index') !!}">Партии</a>
            </li>
            <li @if(\Request::route()->getName() == 'organization.index')class="active"@endif>
                <a href="{!! route('organization.index') !!}">Организации</a>
            </li>
            <li @if(\Request::route()->getName() == 'promise.index')class="active"@endif>
                <a href="{!! route('promise.index') !!}">Обещания</a>
            </li>
            <li @if(\Request::route()->getName() == 'users.index')class="active"@endif>
                <a href="{!! route('users.index') !!}">Пользователи</a>
            </li>
            <li @if(\Request::route()->getName() == 'activity_fields.index')class="active"@endif>
                <a href="{!! route('activity_fields.index') !!}">Сферы</a>
            </li>
            <li @if(\Request::route()->getName() == 'suggestions.index')class="active"@endif>
                <a href="{!! route('suggestions.index') !!}">Предложения</a>
            </li>
            <li @if(\Request::route()->getName() == 'comments.index')class="active"@endif>
                <a href="{!! route('comments.index') !!}">Комментарии</a>
            </li>
        </ul>
        @endif


        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Войти</a></li>
                <li><a href="{{ url('/register') }}">Регистрация</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Выйти
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>

    </div>
</nav>

<div class="container-fluid">
    <div class="col-md-12">
        @if(session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
        @endif
        @if(session('notification'))
            <div class="alert alert-warning">
                {!! session('notification') !!}
            </div>
        @endif
    </div>
    @yield('content')
</div>

</body>
</html>