@extends('backend.layout')

@section('title', 'Сферы деятельности')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('activity_fields.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td class="col-md-1">id</td>
                <td>Имя</td>
                <td colspan="2"></td>
            </tr>
            </thead>
            <tbody>
            @foreach ($activity_fields as $activity_field)
                <tr>
                    <td>{!! $activity_field['id'] !!}</td>
                    <td>{!! $activity_field['name'] !!}</td>
                    <td>
                        {!! Form::model($activity_field, ['route' => ['activity_fields.edit', $activity_field['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($activity_field, ['route' => ['activity_fields.destroy', $activity_field['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $activity_fields->links() !!}
    </div>
@endsection