@extends('backend.layout')

@section('title', 'Сферы деятельности')

@section('content')
    <div class="col-md-12">
        {{
            Form::model(
                $activity_field,
                [
                    'route' => $activity_field->exists ? ['activity_fields.update',$activity_field->id] : 'activity_fields.store',
                    'method' => $activity_field->exists ? 'PATCH' : 'POST',
                    'class' => 'form-horizontal',
                    'files' => true
                ]
            )

        }}

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('name', 'Имя', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('name', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label($activity_field->icon_form_name, 'Иконка', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                @if($activity_field->icon_url)
                    <img src="{{$activity_field->icon_url}}" alt="">
                @endif
                {{ Form::file($activity_field->icon_form_name) }}
            </div>
        </div>


        <div class="form-group">
            {{ Form::submit('Сохранить') }}
        </div>


        {{ Form::close() }}
    </div>
@endsection