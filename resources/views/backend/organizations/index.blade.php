@extends('backend.layout')

@section('title', 'Организации')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('organization.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>id <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Название <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'name'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Дата создания <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'creation_date'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Сайт <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'official_site_url'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Рейтинг <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'rating'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Число обещаний <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Выполнено обещаний <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'confirmed_promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Провалено обещаний <a href="{!! route('organization.index', array_merge($pagination_params,['sort_by'=> 'failed_promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[name]', $search_fields['name'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[creation_date]', $search_fields['creation_date'], ['class' => 'form-control']) !!}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    {!! Form::submit('Найти', ['class' => 'form-control btn btn-primary']) !!}
                </td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($organizations as $organization)
                <tr>
                    <td>{!! $organization['id'] !!}</td>
                    <td>{!! $organization['name'] !!}</td>
                    <td>{!! $organization['creation_date'] !!}</td>
                    <td>{!! $organization['official_site_url'] !!}</td>
                    <td>{!! $organization->rounded_rating !!}</td>
                    <td>{!! $organization->promises()->count() !!}</td>
                    <td>{!! $organization->confirmedPromises->count() !!}</td>
                    <td>{!! $organization->failedPromises->count() !!}</td>
                    <td>
                        {!! Form::model($organization, ['route' => ['organization.edit', $organization['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($organization, ['route' => ['organization.destroy', $organization['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $organizations->links() !!}
    </div>
@endsection