@extends('backend.layout')

@section('title', 'Организация')

@section('content')
    <div class="col-md-12">
        {{
            Form::model(
                $organization,
                [
                    'route' => $organization->exists ? ['organization.update',$organization->id] : 'organization.store',
                    'method' => $organization->exists ? 'PATCH' : 'POST',
                    'class' => 'form-horizontal',
                    'id' => 'politician_form',
                    'files' => true
                ]
            )

        }}

        @include('backend.organizations.edit_form')

        {{ Form::close() }}
    </div>
@endsection