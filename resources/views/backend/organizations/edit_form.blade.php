<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('name', 'Название', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('leader_id', 'Лидер', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::select('leader_id', \App\Politician::getSelectOptions(), $organization->leader_id, ['class' => 'form-control chosen']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('creation_date', 'Дата создания', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::date('creation_date', $organization->creation_date , ['class' => 'form-control datepicker']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('logo_url', 'Лого', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        @if($organization->logo_url)
            <img src="{{$organization->logo_url}}" alt="">
        @endif
        {{ Form::file($organization->logo_form_name) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label($organization->icon_form_name, 'Иконка', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        @if($organization->icon_url)
            <img src="{{$organization->icon_url}}" alt="">
        @endif
        {{ Form::file($organization->icon_form_name) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('politicians', 'Сотрудники', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::select('politicians', $politicians, $organization->politician_ids, ['name' => 'politicians[]', 'multiple'=>'multiple', 'class' => 'form-control chosen']) }}
    </div>
</div>


<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('promise_id', 'Обещания', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::select('promise_id', $promises, null, ['id' => 'promises_select', 'class' => 'form-control chosen', 'multiple', 'name' => 'promises[]']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('history', 'История') }}
    </div>
    <div class="col-md-10">
        {{ Form::hidden('history', null, ['class' => 'form-control', 'id' => 'history_html']) }}
        <div id="history_editor" style="height: 200px">
            {!! $organization->history !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('official_site_url', 'Официальный веб-сайт', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::text('official_site_url', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('facebook_url', 'Официальный Facebook', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::text('facebook_url', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('twitter_url', 'Официальный Twitter', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::text('twitter_url', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('use_fake_rating', 'Использовать фейковый рейтинг') }}
    </div>
    <div class="col-md-10">
        {{ Form::checkbox('use_fake_rating', 1, null, ['class' => 'checkbox']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('fake_rating', 'Фейковый рейтинг') }}
    </div>
    <div class="col-md-10">
        {{ Form::number('fake_rating', null, ['class' => 'form-control', 'step' => '0.1', 'min' => 0, 'max' => 100]) }}
    </div>
</div>

<div class="form-group">
    {{ Form::submit('Сохранить') }}
</div>


<div id="add_promise_dlg" ></div>

<script>
    $(document).ready(function(){
        var quill_config = {
            modules: {
                'syntax': true,
                'toolbar': [
                    [{ 'size': [] }],
                    [ 'bold', 'italic', 'underline', 'strike' ],
                    [{ 'color': [] }, { 'background': [] }],
                    [{ 'script': 'super' }, { 'script': 'sub' }],
                    [{ 'header': '1' }, { 'header': '2' }, 'blockquote', 'code-block' ],
                    [{ 'list': 'ordered' }, { 'list': 'bullet'}, { 'indent': '-1' }, { 'indent': '+1' }],
                    [ 'direction', { 'align': [] }],
                    [ 'link', 'image', 'video', 'formula' ],
                    [ 'clean' ]
                ],
            },
            theme: 'snow'
        };
        var quill_history = new Quill('#history_editor', quill_config);

        $('#politician_form').on('submit', function(e){
            $('#history_html').val(quill_history.root.innerHTML);
        });

        $('#promises_select').on('chosen:no_results', function () {
            $('#promises_select_chosen .no-results').html(
                $('#promises_select_chosen .no-results').html() +
                ' <span class="btn btn-success btn-xs" id="create_promise"><span class="glyphicon glyphicon-plus-sign"></span> Создать</span>'
            );
        });
        $('#promises_select_chosen').delegate('#create_promise', 'click', function () {
            var $that = $(this);
            var $dlg = $('#add_promise_dlg');

            $.ajax({
                url: '{{ route('api.promises.getAjaxCreateForm') }}',
                method: 'get'
            }).done(function (resp) {
                $dlg.html(resp);
                $dlg.dialog({
                    modal: true,
                    width: 600
                });
                $('#promise_form_ajax').on('submit', function(e){
                    e.preventDefault();
                    var $that = $(this);
                    $.ajax({
                        url: '{{ route('api.promises.storeAjax') }}',
                        method: 'post',
                        data: {
                            description: $that.find('#description_input').val()
                        }
                    }).done(function(resp){
                        if (resp) {
                            console.log(resp);
                            var $promise_select = $('#promises_select');
                            var $new_option = $('<option value="'+resp.id+'">'+resp.description+'</option>').prop('selected', true);
                            $promise_select.append($new_option).trigger('chosen:updated');
                            $dlg.dialog('close');
                        }
                    });
                });
            });

        });
    });
</script>