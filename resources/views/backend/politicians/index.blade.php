@extends('backend.layout')

@section('title', 'Политики')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('politician.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>id <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>ФИО <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'last_name'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Дата рождения <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'birthday'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Партия <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'party_id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Сайт <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'official_site_url'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Рейтинг</td>
                <td>Число обещаний <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Выполнено обещаний <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'confirmed_promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Провалено обещаний <a href="{!! route('politician.index', array_merge($pagination_params,['sort_by'=> 'failed_promises_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[last_name]', $search_fields['last_name'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[birthday]', $search_fields['birthday'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::select('search_fields[party_id]', $parties, $search_fields['party_id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[official_site_url]', $search_fields['official_site_url'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{!! Form::submit('Найти', ['class' => 'form-control btn btn-primary']) !!}</td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($politicians as $politician)
                <tr>
                    <td>{!! $politician['id'] !!}</td>
                    <td>{!! $politician['full_name'] !!}</td>
                    <td>{!! $politician['birthday'] !!}</td>
                    <td>{!! $politician->party_name !!}</td>
                    <td>{!! $politician['official_site_url'] !!}</td>
                    <td>{!! $politician->rounded_rating !!}</td>
                    <td>{!! $politician->promises()->count() !!}</td>
                    <td>{!! $politician->confirmedPromises->count() !!}</td>
                    <td>{!! $politician->failedPromises->count() !!}</td>
                    <td>
                        {!! Form::model($politician, ['route' => ['politician.edit', $politician['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($politician, ['route' => ['politician.destroy', $politician['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $politicians->appends($pagination_params)->links() !!}
    </div>
@endsection