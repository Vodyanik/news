@extends('backend.layout')

@section('title', 'Новости')

@section('content')
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <td>Id новости <a href="{!! route('news_changes_log.index', array_merge($pagination_params,['sort_by'=> 'news.id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Заголовок новости <a href="{!! route('news_changes_log.index', array_merge($pagination_params,['sort_by'=> 'news.title'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Имя пользователя <a href="{!! route('news_changes_log.index', array_merge($pagination_params,['sort_by'=> 'users.name'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Старый статус <a href="{!! route('news_changes_log.index', array_merge($pagination_params,['sort_by'=> 'old_status'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Новый статус <a href="{!! route('news_changes_log.index', array_merge($pagination_params,['sort_by'=> 'new_status'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Время <a href="{!! route('news_changes_log.index', array_merge($pagination_params,['sort_by'=> 'cactions_log.reated_at'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[news.id]', $search_fields['news.id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[news.title]', $search_fields['news.title'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[users.name]', $search_fields['users.name'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[old_status]', $search_fields['old_status'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[new_status]', $search_fields['new_status'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[actions_log.created_at]', $search_fields['actions_log.created_at'], ['class' => 'form-control']) !!}
                </td>
                <td>
                    {!! Form::submit('Найти', ['class' => 'form-control btn btn-primary']) !!}
                </td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($log_entries as $log_entry)
                <tr>
                    <td>{!! $log_entry->news->id !!}</td>
                    <td>{!! $log_entry->news->title !!}</td>

                    <td>{!! $log_entry->user->name !!}</td>

                    <td>{!! $log_entry->old_status !!}</td>
                    <td>{!! $log_entry->new_status !!}</td>
                    <td>{!! $log_entry->created_at !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $log_entries->appends($pagination_params)->links() !!}
    </div>
@endsection