@extends('backend.layout')

@section('title', 'Обещания')

@section('content')
    <div class="col-md-12">
        {{
            Form::model(
                $promise,
                [
                    'route' => $promise->exists ? ['promise.update',$promise->id] : 'promise.store',
                    'method' => $promise->exists ? 'PATCH' : 'POST',
                    'class' => 'form-horizontal',
                    'id' => 'promise_form'
                ]
            )

        }}

        @include('backend.promises.edit_form')

        {{ Form::close() }}

        <script>
            $(document).ready(function () {
                var $create_tag_dlg = $('#create_tag_dlg');

                $('#tag_select').on('chosen:no_results', function () {
                    $('#tag_select_chosen .no-results').html(
                        $('#tag_select_chosen .no-results').html() +
                        ' <span class="btn btn-success btn-xs" id="create_tag"><span class="glyphicon glyphicon-plus-sign"></span> Создать</span>'
                    );
                });
                $('#tag_select_chosen').delegate('#create_tag', 'click', function () {
                    $create_tag_dlg.dialog();
                });
                $('body').on('tag_created', function(event, data){
                    var $new_tag = $('<option value="'+data.id+'">'+data.name+'</option>').prop('selected', true);
                    $('#tag_select').append($new_tag).trigger('chosen:updated');
                    $create_tag_dlg.dialog('close');
                });
                $('body').on('tag_creation_error', function(event, data){
                    $create_tag_dlg.dialog('close');
                    alert('Не могу создать тег');
                });

            })
        </script>
    </div>
@endsection