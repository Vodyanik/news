<div class="col-md-12">
    {{
        Form::open(
            [
                'class' => 'form-horizontal',
                'id' => 'promise_form_ajax'
            ]
        )

    }}

    @include('backend.promises.edit_form')

    {{ Form::close() }}

</div>