@extends('backend.layout')

@section('title', 'Обещания')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('promise.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td>id</td>
                <td>Политик</td>
                <td>Партия</td>
                <td>Организация</td>
                <td>Описание</td>
                <td colspan="2"></td>
            </tr>
            </thead>
            <tbody>
            @foreach ($promises as $promise)
                <tr>
                    <td>{!! $promise['id'] !!}</td>
                    <td>{!! implode(', ', $promise->politicians ? $promise->politicians->pluck('full_name')->toArray() : []) !!}</td>
                    <td>{!! implode(', ', $promise->parties ? $promise->parties->pluck('name')->toArray() : []) !!}</td>
                    <td>{!! implode(', ', $promise->organization ? $promise->organization->pluck('name')->toArray() : []) !!}</td>
                    <td>{!! $promise['description'] !!}</td>
                    <td>
                        {!! Form::model($promise, ['route' => ['promise.edit', $promise['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($promise, ['route' => ['promise.destroy', $promise['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $promises->links() !!}
    </div>
@endsection