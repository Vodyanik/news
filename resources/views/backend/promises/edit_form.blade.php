<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('description', 'Общая формулировка') }}
    </div>
    <div class="col-md-10">
        {{ Form::text('description', null, ['class' => 'form-control', 'id' => 'description_input']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('quote', 'Конкретная цитата') }}
    </div>
    <div class="col-md-10">
        {{ Form::text('quote', null, ['class' => 'form-control', 'id' => 'quote_input']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('proof_url', 'Пруфлинка') }}
    </div>
    <div class="col-md-10">
        {{ Form::text('proof_url', null, ['class' => 'form-control', 'id' => 'proof_url']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('tags', 'Теги') }}
    </div>
    <div class="col-md-10">
        {{ Form::select('tags', App\Tag::getSelectValues(), $promise->tag_ids, ['multiple', 'name' => 'tags[]', 'class' => 'form-control chosen', 'id' => 'tag_select']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('activity_fields', 'Сферы', ['class' => 'control-label']) }}
    </div>
    <div class="col-md-10">
        {{ Form::select('activity_fields', App\ActivityField::getSelectOptions(), $promise->activity_fields->pluck('id')->toArray(), ['multiple', 'name' => 'activity_fields[]', 'class' => 'form-control chosen']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('promise_confirmed', 'Подтверждено') }}
    </div>
    <div class="col-md-10">
        {{ Form::checkbox('promise_confirmed', 1, $promise->confirmed, ['class' => 'checkbox']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('promise_failed', 'Провалено') }}
    </div>
    <div class="col-md-10">
        {{ Form::checkbox('promise_failed', 1, $promise->failed, ['class' => 'checkbox']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-md-2">
        {{ Form::label('comments', 'Комментарии') }}
    </div>
    <div class="col-md-10">
        {{ Form::textarea('comments', null, ['class' => 'form-control', 'id' => 'quote_input']) }}
    </div>
</div>

<div id="create_tag_dlg" style="display: none">
    <div class="vue_app">
        <tag-editor></tag-editor>
    </div>
</div>

<div class="form-group">
    {{ Form::submit('Сохранить') }}
</div>