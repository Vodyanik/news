@extends('backend.layout')

@section('title', 'Теги')

@section('content')
    <div class="col-md-12">
        {{
            Form::model(
                $tag,
                [
                    'route' => $tag->exists ? ['tags.update',$tag->id] : 'tags.store',
                    'method' => $tag->exists ? 'PATCH' : 'POST',
                    'class' => 'form-horizontal'
                ]
            )

        }}

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('name', 'Имя тега', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::text('name', null, ['class' => 'form-control']) }}
            </div>
        </div>



        <div class="form-group">
            {{ Form::submit('Сохранить') }}
        </div>


        {{ Form::close() }}
    </div>
@endsection