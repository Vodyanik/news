@extends('backend.layout')

@section('title', 'Теги')

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <a href="{!! route('tags.create') !!}" class="btn btn-default">Создать</a>
            </div>
        </div>
        <table class="table">
            <thead>
            <tr>
                <td class="col-md-1">id <a href="{!! route('tags.index', array_merge($pagination_params,['sort_by'=> 'id'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Имя тега <a href="{!! route('tags.index', array_merge($pagination_params,['sort_by'=> 'name'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Количество новостей <a href="{!! route('tags.index', array_merge($pagination_params,['sort_by'=> 'news_count'])) !!}"><span class="glyphicon glyphicon-sort"></span></a></td>
                <td>Лайки</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                {!! Form::open(['method' => 'GET']) !!}
                <td>
                    {!! Form::input('text', 'search_fields[id]', $search_fields['id'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td>
                    {!! Form::input('text', 'search_fields[name]', $search_fields['name'], ['class' => 'form-control col-xs-1']) !!}
                </td>
                <td></td>
                <td></td>
                <td>
                    {!! Form::submit('Найти', ['class' => 'form-control btn btn-primary btn-sm']) !!}
                </td>
                {!! Form::close() !!}
                <td>
                    {!! Form::open(['method' => 'GET']) !!}
                    {!! Form::submit('Сбросить', ['class' => 'form-control btn btn-warning btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach ($tags as $tag)
                <tr>
                    <td>{!! $tag['id'] !!}</td>
                    <td>{!! $tag['name'] !!}</td>
                    <td>
                        {!! $tag->news_count !!}
                    </td>
                    <td>
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                        {{$tag->news()->withCount('likes')->get()->sum('likes_count')}}
                        /
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                        {{$tag->news()->withCount('dislikes')->get()->sum('dislikes_count')}}
                    </td>
                    <td>
                        {!! Form::model($tag, ['route' => ['tags.edit', $tag['id']], 'method' => 'get']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-edit"></span> Изменить', ['class' => 'btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        {!! Form::model($tag, ['route' => ['tags.destroy', $tag['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $tags->appends($pagination_params)->links() !!}
    </div>
@endsection