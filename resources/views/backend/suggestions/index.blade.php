@extends('backend.layout')

@section('title', 'Предложения')

@section('content')
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <td class="col-md-1">id</td>
                <td>Регион</td>
                <td>Сфера</td>
                <td>Текст</td>
                <td colspan="1"></td>
            </tr>
            </thead>
            <tbody>
            @foreach ($suggestions as $suggestion)
                <tr>
                    <td>{!! $suggestion['id'] !!}</td>
                    <td>{!! $suggestion->region->name !!}</td>
                    <td>{!! $suggestion->activity_field->name !!}</td>
                    <td>{!! $suggestion->text !!}</td>
                    <td>
                        {!! Form::model($suggestion, ['route' => ['suggestions.destroy', $suggestion['id']], 'method' => 'DELETE']) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-remove"></span> Удалить', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="pagination">
        {!! $suggestions->links() !!}
    </div>
@endsection