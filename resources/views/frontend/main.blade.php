@extends('frontend.layout')

@section('main_content')
    <section class="main-slider">
        <div id="slider" class="main-slider-slides">
            @foreach($slider_news as $news_item)
                <article class="main-slider-item" style="background-image: url('{{$news_item->image}}')">
                    <header class="header">
                        <ul class="labels list-unstyled list-inline">
                            @foreach($news_item->parties as $party)
                                <li class="labels-item">{{$party->name}}</li>
                            @endforeach
                            @foreach($news_item->organizations as $organization)
                                <li class="labels-item">{{$organization->name}}</li>
                            @endforeach
                            @foreach($news_item->politicians as $politician)
                                <li class="labels-item">{{$politician->full_name}}</li>
                            @endforeach
                        </ul>
                    </header>
                    <main class="main hidden-xs hidden-sm" style="left: 2%; right: 60%; top: 20%;">
                        <h2 class="h1 heading" style="font-size: 22px; font-weight: 300 !important;">
                            <a href="/news/{{$news_item->slug}}" style="font-size: 22px;">
                                {{$news_item->title}}
                            </a>
                        </h2>
                        <hr>
                        <ul style="list-style-type: none;">
                            @foreach($news_item->activity_fields as $sphera)
                                <li style="float: left; margin-left: 5px;">
                                    <img src="{{ asset($sphera->icon_url) }}" alt="">
                                </li>
                            @endforeach
                        </ul>
                    </main>
                    <main class="main visible-xs visible-sm">
                        <h2 class="h1 heading">
                            <a href="/news/{{$news_item->slug}}">
                                {{$news_item->title}}
                            </a>
                        </h2>
                        <hr>
                        <ul style="list-style-type: none;">
                            @foreach($news_item->activity_fields as $sphera)
                                <li style="float: left; margin-left: 5px;">
                                    <img src="{{ asset($sphera->icon_url) }}" alt="">
                                </li>
                            @endforeach
                        </ul>
                    </main>
                    <footer class="footer">
                        <ul class="metadata list-unstyled list-inline">
                            <li class="metadata-item published">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                {{$news_item->publication_date->diffForHumans()}}
                            </li>
                            <li class="metadata-item likes">
                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                {{$news_item->likes_count}}
                            </li>
                            <li class="metadata-item dislikes">
                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                {{$news_item->dislikes_count}}
                            </li>
                        </ul>

                    </footer>
                </article>
            @endforeach

        </div>
        <div id="slider-controls" class="main-slider-controls">
            <button class="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </button>
            <button class="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </button>
        </div>
    </section>
    <section class="columns">
        <div class="row">
            <div class="col-lg-3">
                <aside class="filters">
                    <aside>
                        <section class="aside-section filters">
                            <form action="" id="filters-form">
                                <h5 class="h5 heading">ФИЛЬТРЫ</h5>
                                <button class="filters-save h5">
                                    СОХРАНИТЬ
                                </button>
                                <div class="filters-sections">
                                    <div class="filters-section">
                                        <div class="filters-section-title">
                                            <button href="#">ПО ВРЕМЕНИ</button>
                                            /
                                            <button href="#">ПО ПОПУЛЯРНОСТИ</button>
                                        </div>
                                    </div>
                                    <div class="filters-section toggled">
                                        <div class="filters-section-title">
                                            ГЕОГРАФИЯ
                                            <span class="filters-section-title-controls">
                                                        <span class="closed"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                                        <span class="opened"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
                                                    </span>
                                        </div>
                                        <ul class="filters-section-list">
                                            <li class="filters-section-list-item">
                                                <input id="region-0" type="checkbox" data-subj="region" data-id="0">
                                                <label for="region-0">Вся Украина</label>
                                            </li>
                                            @foreach(App\Region::all() as $region)
                                                <li class="filters-section-list-item">
                                                    <input id="region-{{$region->id}}" type="checkbox" data-subj="region" data-id="{{$region->id}}">
                                                    <label for="region-{{$region->id}}">{{ $region->name }}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="filters-section toggled">
                                        <div class="filters-section-title">
                                            ПАРТИИ ИЛИ СТРУКТУРЫ
                                            <span class="filters-section-title-controls">
                                                <span class="closed"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                                <span class="opened"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
                                            </span>
                                        </div>
                                        <ul class="filters-section-list">
                                            @foreach(App\Party::all() as $party)
                                                <li class="filters-section-list-item">
                                                    <input id="party-{{$party->id}}" type="checkbox" data-subj="party" data-id="{{$party->id}}">
                                                    <label for="party-{{$party->id}}">{{$party->name}}</label>
                                                </li>
                                            @endforeach
                                            <br>
                                            @foreach(App\Organization::all() as $org)
                                                <li class="filters-section-list-item">
                                                    <input id="organization-{{$org->id}}" type="checkbox" data-subj="organization" data-id="{{$org->id}}">
                                                    <label for="organization-{{$org->id}}">{{$org->name}}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="filters-section toggled">
                                        <div class="filters-section-title">
                                            РАЗДЕЛЫ
                                            <span class="filters-section-title-controls">
                                                <span class="closed"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                                <span class="opened"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
                                            </span>
                                        </div>
                                        <ul class="filters-section-list">
                                            @foreach(App\ActivityField::all() as $field)
                                                <li class="filters-section-list-item">
                                                    <input id="activity_field-{{$field->id}}" type="checkbox" data-subj="field" data-id="{{$field->id}}">
                                                    <label for="activity_field-{{$field->id}}">
                                                        <img src="{{$field->icon_url}}" width="30" height="30" alt=""><!--second class is variable according to "name"-->
                                                        </span>{{$field->name}}
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="filters-section opened">
                                        <ul class="filters-section-list no-border">
                                            <li class="filters-section-list-item">
                                                <input id="confirmed_only" data-subj="confirmed_only" data-id="1" type="checkbox">
                                                <label for="confirmed_only" class="text-uppercase">Только подтвержденные</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <h5 class="h5 heading">РЕЙТИНГИ</h5>
                            <div class="rating-sections">
                                <div class="rating-section">
                                    <div class="rating-section-title">
                                        ПОЛИТИКИ
                                        <a href="{{ route('index.reiting') }}#politic" class="all-list">Весь список</a>
                                    </div>
                                    <ul class="rating-section-list">
                                        @foreach(App\Politician::top5() as $idx => $politician)
                                            <li class="rating-section-list-item">
                                                <span class="position">{{$idx + 1}}</span>
                                                <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        {{$politician->rounded_rating}}
                                                    </span>
                                                <a href="{{route('politician', $politician->id)}}">{{$politician->short_name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="rating-section">
                                    <div class="rating-section-title">
                                        Партии
                                        <a href="{{ route('index.reiting') }}#party" class="all-list">Весь список</a>
                                    </div>
                                    <ul class="rating-section-list">
                                        @foreach(App\Party::top5() as $idx => $party)
                                            <li class="rating-section-list-item">
                                                <span class="position">{{$idx + 1}}</span>
                                                <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        {{$party->rounded_rating}}
                                                    </span>
                                                <a href="{{route('party', $party->id)}}">{{$party->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="rating-section">
                                    <div class="rating-section-title">
                                        СМИ
                                        <a href="{{ route('index.reiting') }}#news" class="all-list">Весь список</a>
                                    </div>
                                    <ul class="rating-section-list">
                                        @foreach(App\NewsSource::top5() as $idx=>$source)
                                            <li class="rating-section-list-item">
                                                <span class="position">{{$idx + 1}}</span>
                                                <span class="rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            {{$source->rounded_rating}}
                                                        </span>
                                                <a href="#">{{$source->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </aside>
                </aside>
            </div>
            <div class="col-lg-9">
                <section class="previews" id="news_container">
                    <div class="row" id="news_container_row">
                        @include('frontend.partials.news_block')
                    </div>
                    <div class="previews-more" id="load_more_news_btn" >
                        <span class="previews-more-button" href="#">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Показать еще
                        </span>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection