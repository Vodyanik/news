@extends('frontend.layout')

@section('main_content')
    <div class="col-md-12">
        {{
            Form::model(
                $suggestion,
                [
                    'route' => 'suggestion.store',
                    'method' => 'POST',
                    'class' => 'form-horizontal'
                ]
            )

        }}

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('activity_field_id', 'Сфера', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('activity_field_id', App\ActivityField::getSelectOptions(), [], ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('region_id', 'Регион', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::select('region_id', App\Region::getSelectOptions(), [], ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2">
                {{ Form::label('text', 'Текст предложения', ['class' => 'control-label']) }}
            </div>
            <div class="col-md-10">
                {{ Form::textarea('text', null, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::submit('Предложить') }}
        </div>


        {{ Form::close() }}
    </div>
@endsection