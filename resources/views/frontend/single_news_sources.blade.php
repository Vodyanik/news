@extends('frontend.layout')

@section('main_content')
    <section class="columns">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <article class="single-news sources">
                    <header>
                        <div class="metadata">
                            <div class="metadata-list">
                                <a href="#" class="metadata-item category link">Новости</a>
                                <span class="metadata-item published">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    12:56
                                </span>
                            </div>
                        </div>
                    </header>
                    <main>
                        <h1 class="h2 heading">
                            Красный Крест впервые посетил пленных в ДНР
                        </h1>
                        <ul class="sources-list">
                            <li class="sources-list-item">
                                <article>
                                    <header class="header">
                                        <img src="http://newvv.net/images/stories/Ukrop_Logo_N_ai.jpg" alt="">
                                        <span class="source-name">Название источника</span>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;12:56
                                    </header>
                                    <main class="main">
                                        <h3 class="h3 heading">
                                            <a href="#">Аваков подал е-декларацию за 2016 год и длинный заголовок</a>
                                        </h3>
                                    </main>
                                </article>
                            </li>
                            <li class="sources-list-item">
                                <article>
                                    <header class="header">
                                        <img src="http://newvv.net/images/stories/Ukrop_Logo_N_ai.jpg" alt="">
                                        <span class="source-name">Название источника</span>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;12:56
                                    </header>
                                    <main class="main">
                                        <h3 class="h3 heading">
                                            <a href="#">Аваков подал е-декларацию за 2016 год и длинный заголовок</a>
                                        </h3>
                                    </main>
                                </article>
                            </li>
                            <li class="sources-list-item">
                                <article>
                                    <header class="header">
                                        <img src="http://newvv.net/images/stories/Ukrop_Logo_N_ai.jpg" alt="">
                                        <span class="source-name">Название источника</span>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;12:56
                                    </header>
                                    <main class="main">
                                        <h3 class="h3 heading">
                                            <a href="#">Аваков подал е-декларацию за 2016 год и длинный заголовок</a>
                                        </h3>
                                    </main>
                                </article>
                            </li>
                        </ul>
                    </main>

                </article>
            </div>
        </div>
    </section>
@endsection