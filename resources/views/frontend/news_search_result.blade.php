@extends('frontend.layout')

@section('main_content')
    <section class="columns">
        <div class="row">
            <div class="col-lg-3">
                <aside class="filters">
                    <aside>
                        <section class="aside-section filters">
                            <h5 class="h5 heading">РЕЙТИНГИ</h5>
                            <div class="rating-sections">
                                <div class="rating-section">
                                    <div class="rating-section-title">
                                        ПОЛИТИКИ
                                        <a href="#" class="all-list">Весь список</a>
                                    </div>
                                    <ul class="rating-section-list">
                                        @foreach(App\Politician::top5() as $idx => $politician)
                                            <li class="rating-section-list-item">
                                                <span class="position">{{$idx + 1}}</span>
                                                <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        {{$politician->rounded_rating}}
                                                    </span>
                                                <a href="{{route('politician', $politician->id)}}">{{$politician->short_name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="rating-section">
                                    <div class="rating-section-title">
                                        Партии
                                        <a href="#" class="all-list">Весь список</a>
                                    </div>
                                    <ul class="rating-section-list">
                                        @foreach(App\Party::top5() as $idx => $party)
                                            <li class="rating-section-list-item">
                                                <span class="position">{{$idx + 1}}</span>
                                                <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        {{ $party->rounded_rating }}
                                                    </span>
                                                <a href="{{route('party', $party->id)}}">{{$party->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="rating-section">
                                    <div class="rating-section-title">
                                        СМИ
                                        <a href="#" class="all-list">Весь список</a>
                                    </div>
                                    <ul class="rating-section-list">
                                        @foreach(App\NewsSource::top5() as $idx=>$source)
                                            <li class="rating-section-list-item">
                                                <span class="position">{{$idx + 1}}</span>
                                                <span class="rating">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                            {{$source->rounded_rating}}
                                                        </span>
                                                <a href="#">{{$source->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </aside>
                </aside>
            </div>
            <div class="col-lg-9">
                <section class="previews" id="news_container">
                    @if(!empty($news))
                        <div class="row" id="news_container_row">
                            @include('frontend.partials.news_block')
                        </div>
                        <div class="previews-more" id="load_more_news_btn" >
                            <span class="previews-more-button">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Показать еще
                            </span>
                        </div>
                    @else
                        <h3>Новости не найдены</h3>
                    @endif
                </section>
            </div>
        </div>
    </section>
@endsection