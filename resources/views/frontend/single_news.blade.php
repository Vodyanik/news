@extends('frontend.layout')

@section('title')
    Smartnews :: {{ $news->title  }}
@endsection

@section('meta-description, SmartNews - Умные новости') $news->title @endsection

@section('meta-keywords'){{ implode(' ', $news->tags->pluck('name')->all()) }}@endsection

@section('news-meta')
    <meta property="article:published_time"
          content="{{ $news->publication_date->toAtomString() }}"/>
    <meta property="article:modified_time"
          content="{{ $news->updated_at->toAtomString() }}"/>
    @foreach($news->tags as $tag)
        <meta property="article:tag" content="{{$tag->name}}"/>
    @endforeach

    <meta property="og:image:width" content="740"/>
    <meta property="og:image:height" content="555"/>

    <meta property="og:title"
          content="{{$news->title}}"/>
    <meta property="og:url"
          content="{{route('news', $news->slug)}}"/>
    <meta property="og:image"
          content="{{url($news->tn_image)}}"/>
    <meta property="og:type"
          content="article"/>
    <meta property="og:site_name" content="Smartnews.com.ua"/>
    <meta property="og:description"
          content="{{strip_tags($news->preview)}}"/>
    <meta property="article:author" content="https://www.facebook.com/smartnewscomua"/>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@smartnewscomua">
    <meta name="twitter:title"
          content="{{$news->title}}">
    <meta name="twitter:description"
          content="{{strip_tags($news->preview)}}">
    <meta name="twitter:image:src"
          content="{{url($news->tn_image)}}">
    <meta name="twitter:domain" content="smartnews.com.ua">
    <meta name="twitter:creator" content="@smartnewscomua" />
    <meta itemprop="name"
          content="{{$news->title}}">
    <meta itemprop="description"
          content="{{strip_tags($news->preview)}}">
    <meta itemprop="image"
          content="{{url($news->tn_image)}}">
@endsection

@section('main_content')
    <section class="columns" id="news_container">
        <div class="row">
            <div class="col-lg-3">
                <aside>
                    <section class="aside-section subject">
                        @if($news->parties->count() != 0)
                            <h5 class="h5 heading">ПАРТИИ</h5>
                            @foreach($news->parties as $party)
                                <div class="col-md-12">
                                    @if(!empty($party->logo_url))
                                        <div class="row">
                                            <div class="col-md-8">
                                                <img src="{{ $party->logo_url }}" alt="" width="100px">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p style="font-size: 13px; margin-top: 5px;">
                                                <a href="{{route('party', $party->id)}}">{{ $party->name }}</a>
                                            </p>
                                        </div>
                                        <div class="col-md-4" style="margin-top: 6px;">
                                            <span class="rating"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;{{ $party->rounded_rating }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        @if($news->organizations->count() != 0)
                            <h5 class="h5 heading">ОРГАНИЗАЦИИ</h5>
                            @foreach($news->organizations as $organization)
                                <div class="col-md-12">
                                    @if(!empty($politician->logo_url))
                                        <div class="row">
                                            <div class="col-md-8">
                                                <img src="{{ $organization->logo_url }}" alt="" width="100px">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p style="font-size: 13px; margin-top: 5px;">
                                                <a href="{{route('organization', $organization->id)}}">{{$organization->name}}</a>
                                            </p>
                                        </div>
                                        <div class="col-md-4" style="margin-top: 6px;">
                                            <span class="rating"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;{{$organization->rounded_rating}}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        @if($news->politicians->count() != 0)
                            <h5 class="h5 heading">ПОЛИТИКИ</h5>
                            @foreach($news->politicians as $politician)
                                <div class="col-md-12">
                                    @if(!empty($politician->photo_url))
                                        <div class="row">
                                            <div class="col-md-8">
                                                <img src="{{ $politician->photo_url }}" alt="" width="100px" style="border-radius: 100px;">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-8">
                                            <p style="font-size: 13px; margin-top: 5px;">
                                                <a href="{{route('politician', $politician->id)}}">{{ $politician->full_name }}</a>
                                            </p>
                                        </div>
                                        <div class="col-md-4" style="margin-top: 6px;">
                                            <span class="rating"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;{{ $politician->rounded_rating }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        @if($news->politicians->count() != 0)
                            <h5 class="h5 heading">РЕГИОН</h5>
                            @foreach($news->regions as $region)
                                <div class="subject-card">
                                    {{ $region->name }}
                                </div>
                            @endforeach
                        @endif
                        @if($news->tags->count() != 0)
                            <h5 class="h5 heading">ТЭГИ</h5>
                            <ul style="list-style-type: none">
                                @foreach($news->tags as $tag)
                                    <li class="tags-item"><a href="{{route('news_by_tag', $tag->name)}}">#{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        @endif

                    </section>
                    <!--
                    <section class="aside-section links">
                        <h5 class="h5 heading">НА ДРУГИХ РЕСУРСАХ</h5>
                        <ul class="links-list">
                            <li class="links-list-item">
                                <a href="#">Название статьи. Длинное, совпадает тематика с новостью.</a>
                            </li>
                            <li class="links-list-item">
                                <a href="#">Ещё одна статья.</a>
                            </li>
                            <li class="links-list-item">
                                <a href="#">Ещё одна статья.</a>
                            </li>
                        </ul>
                    </section>
                    -->
                </aside>
            </div>
            <div class="col-lg-9">
                <article class="single-news">
                    <header>
                        <div class="metadata">
                            <div class="metadata-list">
                                <a href="/" class="metadata-item category link">Новости</a>
                                <span class="metadata-item published">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            {{$news->publication_date->diffForHumans()}}
                                        </span>
                            </div>
                        </div>
                    </header>
                    <main>
                        <h1 class="h2 heading">
                            {{$news->title}}
                        </h1>
                        <div class="fact-checking {{$news->fact_checking_level}}">
                            <div class="fact-checking {{$news->fact_checking_level}}" style="width: 30px; height: 30px; position: absolute; margin-top: -14px; border-radius: 20px;"></div>
                        </div><!--low/medium/high-->
                        <div class="image" data-ratio="2:1" style="background-image: url('{{$news->image}}'); position: relative;">
                            <ul style="position: absolute; list-style-type: none; padding-left: 30px; top: 30px;">
                                @foreach($news->activity_fields as $sphera)
                                    <li style="float: left; margin-left: 10px;">
                                        <img src="{{ asset($sphera->icon_url) }}" alt="">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="source">
                            <a href="{{$news->original_news_url}}"
                               class="source-resource">
                                <img src="{{$news->news_source->logo_url}}"
                                     class="source-logo" alt="">
                            </a>
                            <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,7
                        </div>
                        <div class="text"
                             data-text-to-speech="speech-src-1">
                            {!! $news->body !!}
                        </div>
                        <section class="speech-generator ready pull-right"
                                 data-speech-src="speech-src-1">
                            <span class="speech-generator-name">АУДИО</span>
                            <button class="speech-generator-control play-pause">
                                <span class="play-icon">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </span>
                                <span class="pause-icon">
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                </span>
                            </button>
                            <button class="speech-generator-control stop">
                                <i class="fa fa-stop" aria-hidden="true"></i>
                            </button>
                            <button class="speech-generator-control volume-up">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                            <span class="speech-generator-volume-indication">75%</span>
                            <button class="speech-generator-control volume-down">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                        </section>
                    </main>
                    <footer>
                        <section class="metadata">
                            <div class="metadata-list" id="news_like_reactions_{{$news->id}}">
                                <a href="#" data-news_id="{{$news->id}}"
                                   class="metadata-item likes {{$news->user_like_reaction() == 'like' ? 'active' : ''}}"
                                   id="news_like_{{$news->id}}"
                                >
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                    <span class="likes_counter">{{$news->likes_count}}</span>
                                </a>
                                <a href="#" data-news_id="{{$news->id}}"
                                   class="metadata-item dislikes {{$news->user_like_reaction() == 'dislike' ? 'active' : ''}}"
                                   id="news_dislike_{{$news->id}}"
                                >
                                    <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                    <span class="dislikes_counter">{{$news->dislikes_count}}</span>
                                </a>
                                <a href="#" data-news_id="{{$news->id}}"
                                   class="metadata-item padding-right bookmark {{$news->is_bookmarked() ? 'active' : ''}}"
                                >
                                    <i class="fa fa-bookmark" aria-hidden="true"> в закладки</i>
                                </a>
                                <span class="metadata-item addon">
                                    <span>Поделитесь вашим мнением!</span>
                                </span>
                            </div>
                        </section>
                    </footer>
                </article>

                {{-- comments --}}

                @include('frontend.partials.comments')

                @include('frontend.news_footer')
            </div>
        </div>
    </section>
@endsection