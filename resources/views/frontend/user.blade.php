@extends('frontend.layout')

@section('main_content')
    <section class="columns">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <article class="user file">
                    <header>
                        <div class="metadata">
                            <div class="metadata-list">
                                <span class="metadata-item category">ЛИЧНЫЙ КАБИНЕТ</span>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="birthday"> <span class="years-old">Всего на сайте</span> {{ str_replace(' назад', '', $user->created_at->diffForHumans())}}</div>
                                <h2 class="h2 file-heading">{{$user->name}}</h2>
                                    {{--<span class="user-name">@userusertext</span></h2>--}}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="name">
                                            <div class="name-one-row">МОИ ОЦЕНКИ</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="value user-likes">
                                            <div class="value-one-row">
                                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;{{$user->likes->count()}}
                                                &nbsp;&nbsp;&nbsp;
                                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>&nbsp;{{$user->dislikes->count()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="name">
                                            <div class="name-first-row">МОИ ШАБЛОНЫ</div>
                                            <div class="name-second-row gray">Настройте ленту под себя</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="value">
                                            <div class="edit-box inline">
                                                <a href="#"
                                                   data-toggle="modal"
                                                   data-target="#user-template">
                                                    <span class="edit-box-text">ШАБЛОН 1</span>
                                                    <span class="edit-box-icon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                                </a>
                                            </div>
                                            <div class="edit-box inline">
                                                <a href="#">
                                                    <span class="edit-box-text">ШАБЛОН 2</span>
                                                    <span class="edit-box-icon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                -->
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="name">
                                            <div class="name-first-row">МНЕ ИНТЕРЕСНО</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="value">
                                            <ul class="tags-list bg-light wrap">
                                                @foreach($tags as $tag)
                                                    <li class="tags-item">
                                                        <a href="{{route('news_by_tag', $tag)}}">
                                                            #{{$tag}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--
                            <div class="col-md-3">
                                <div class="image round user-avatar"
                                     data-ratio="1:1"
                                     style="background-image: url('images/user.jpg')"></div>
                                <div class="edit-box">
                                    <a href="#">
                                        <span class="edit-box-text">Редактировать профиль</span>
                                        <span class="edit-box-icon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                    </a>
                                </div>
                            </div>
                            --}}
                        </div>
                        <ul class="tabs-navigation nav nav-tabs">
                            <li class="tabs-navigation-item"><a href="#bookmarks">ЗАКЛАДКИ</a></li>
                            {{--<li class="tabs-navigation-item"><a href="#themes">МОИ ТЕМЫ<span class="counters">5/17</span></a></li>--}}
                            {{--<li class="tabs-navigation-item"><a href="#actions">МОИ ДЕЙСТВИЯ</a></li>--}}
                            <li class="tabs-navigation-item active"><a href="#matrix">МАТРИЦА ПАРТИЙ</a></li>
                            @if(auth()->user()->role == 'expert')
                                <li class="tabs-navigation-item"><a href="#opinions">МОИ МНЕНИЯ</a></li>
                            @endif
                        </ul>

                        <div class="tabs-content border tab-content">
                            <div id="bookmarks" class="tabs-content-item bookmarks tab-pane fade in">
                                <section class="previews">
                                    <div class="row">
                                        @foreach($bookmarks as $bookmark)
                                            @include('frontend.partials.news_item', ['news_item' => $bookmark->news])
                                        @endforeach
                                    </div>
                                </section>
                            </div>
                            {{--
                                <div id="themes" class="tabs-content-item themes tab-pane fade in">
                                </div>
                            --}}

                            {{--
                            <div id="actions" class="tabs-content-item actions tab-pane fade in">
                            </div>
                            --}}

                            <div id="matrix" class="tabs-content-item matrix tab-pane fade in active">
                                <h3 class="h3 header">Сферы пересечений моих интересов к партиям</h3>
                                <h4 class="h4 sub-header">Вычисляется исходя из голосований за новости</h4>
                                <div class="row">
                                    <div class="col-lg-8 col-md-10">
                                        <div class="matrix-chart">
                                            <canvas id="myChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(auth()->user()->role == 'expert')
                            <div id="opinions" class="tabs-content-item bookmarks tab-pane fade in">
                                <section class="previews">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a href="#"
                                               class="form-button"
                                               onclick="$('#opinion_form_container').toggleClass('hidden'); return false;"
                                               style="
                                                    border: medium none;
                                                    box-sizing: border-box;
                                                    display: block;
                                                    font-size: 13px;
                                                    height: 42px;
                                                    line-height: 42px;
                                                    outline: medium none;
                                                    padding: 0 16px;
                                                    text-align: center;
                                                    text-decoration: none;
                                                    background: #304dc1 none repeat scroll 0 0;
                                                    color: #fff;
                                                    cursor: pointer;
                                                    margin-bottom: 20px;
                                            ">
                                                Опубликовать мнение
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <section class="opinion_form hidden" id="opinion_form_container">
                                            <div class="form-container">
                                                <div class="form-heading">
                                                    Добавить мнение
                                                </div>
                                                <div class="form-note">
                                                    Ваше мнение будет опубликовано на сайте после проверки модератором
                                                </div>

                                                {{ Form::open(
                                                    [
                                                        'class' => 'form-block border-bottom',
                                                        'route' => 'post_opinion',
                                                        'method' => 'POST',
                                                        'id' => 'opinion_form'
                                                    ]
                                                )}}

                                                    <div class="form-row{{ $errors->has('title') ? ' error' : '' }}">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-label">
                                                                    Заголовок
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="form-element form-input">
                                                                    <input type="text"
                                                                           placeholder="Заголовок"
                                                                           name="title"
                                                                           value="{{ old('title') }}"
                                                                           required
                                                                           autofocus>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if ($errors->has('title'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('title') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-row{{ $errors->has('body') ? ' error' : '' }}">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                    {{ Form::hidden('body', null, ['class' => 'form-control', 'id' => 'body_html']) }}
                                                                    <div id="news_body" style="height: 400px">
                                                                        {!! old('body') !!}
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        @if ($errors->has('body'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('body') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-sub-block">
                                                        <div class="form-element form-button blue inline">
                                                            <button type="submit">Опубликовать</button>
                                                        </div>
                                                    </div>
                                                {{ Form::close() }}
                                            </div>
                                        </section>
                                    </div>
                                    <h3 class="h3 header">Мои мнения</h3>

                                    <div class="row">
                                        @foreach($my_opinions as $opinion)
                                            @include('frontend.partials.news_item', ['news_item' => $opinion])
                                        @endforeach
                                    </div>
                                </section>
                            </div>
                            @endif
                        </div>
                    </main>
                </article>
            </div>
        </div>
    </section>

    <script src="//cdn.quilljs.com/1.2.4/quill.min.js"></script>
    <script>
        var quill_config = {
            modules: {
                'toolbar': [
                    [{ 'size': [] }],
                    [ 'bold', 'italic', 'underline', 'strike' ],
                    [{ 'color': [] }, { 'background': [] }],
                    [{ 'script': 'super' }, { 'script': 'sub' }],
                    [{ 'header': '1' }, { 'header': '2' }, 'blockquote', 'code-block' ],
                    [{ 'list': 'ordered' }, { 'list': 'bullet'}, { 'indent': '-1' }, { 'indent': '+1' }],
                    [ 'direction', { 'align': [] }],
                    [ 'clean' ]
                ],
            },
            theme: 'snow'
        };
        var quill_body = new Quill('#news_body', quill_config);

        $('#opinion_form').on('submit', function(e){
            $('#body_html').val(quill_body.root.innerHTML);
        });

    </script>
@endsection