@extends('frontend.layout')

@section('main_content')
    <section class="columns">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <article class="expert file">
                    <header>
                        <div class="metadata">
                            <div class="metadata-list">
                                <span class="metadata-item category">ЭКСПЕРТ</span>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="birthday">Всего на сайте <span class="years-old">9 месяцев 24 дней</span></div>
                                <h2 class="h2 file-heading">Николай Экспертов</h2>
                                <div class="site">
                                    <a class="site" href="#">Деловое аналитическое издание «Эксперт Северо-Запад»</a>
                                </div>
                                <ul class="ratings-list">
                                    <li class="ratings-list-item">
                                        <span class="text">РЕЙТИНГ ПОЛЬЗОВАТЕЛЕЙ</span>
                                        <span class="rating"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,7</span>
                                    </li>
                                    <li class="ratings-list-item">
                                        <span class="text">РЕЙТИНГ ЭКСПЕРТОВ</span>
                                        <span class="rating"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,7</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="image"
                                     data-ratio="8:9"
                                     style="background-image: url('images/avakov.jpg')"></div>
                            </div>
                        </div>

                        <ul class="tabs-navigation nav nav-tabs">
                            <li class="tabs-navigation-item active"><a href="#biography">БИОГРАФИЯ</a></li>
                            <li class="tabs-navigation-item"><a href="#publication">Публикации<span class="counters">238</span></a></li>
                        </ul>

                        <div class="tabs-content border tab-content">
                            <div id="biography" class="tabs-content-item biography tab-pane fade in active">
                                <p>Родился в Баку в семье военнослужащего. В Украине проживает с 1966 года. В 1988-м окончил Харьковский политехнический университет по специальности автоматизированные системы управления (получил квалификацию инженер-системотехник).</p>
                                <p>В период обучения в институте, летом 1985 года, Аваков работал мастером (командиром отряда) строительного управления №18 треста Новоуренгойгазстрой в Тюменской области. Летом 1986-го — командиром студенческого стройотряда БМП-522 ПСМО Тюменьдорстой треста Уренгойтрансстрой.</p>
                                <p>В 1987-1990 гг. — инженер Харьковского НИИ охраны вод. Занимался научной деятельностью, в частности, вопросами математического моделирования.</p>
                                <p>В 1990-м основал одно из первых в Украине акционерных обществ - АО Инвестор, в котором занял пост президента. В 1992 году создал и возглавил коммерческий банк Базис. В 2002-м был избран членом исполнительного комитета Харьковского городского совета.</p>
                            </div>
                            <div id="publication" class="tabs-content-item publications tab-pane fade">
                                <ul class="marked-list">
                                    <li class="marked-list-item p">
                                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                        <a href="#">Музей шоколада в Одессе: сладкие картины, шоколадный бог и дракон в 130 кг</a>
                                    </li>
                                    <li class="marked-list-item p">
                                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                        <a href="#">Президент объявил конкурс на занятие должностей председателей райгосадминистраций</a>
                                    </li>
                                    <li class="marked-list-item p">
                                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                        <a href="#">Завтра в Украине будет еще теплее</a>
                                    </li>
                                    <li class="marked-list-item p">
                                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                        <a href="#">Бюро находок: одесситы теряют котов, квадрокоптеры и дорогой алкоголь</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </main>
                </article>
                @include('frontend.news_footer')
            </div>
        </div>
    </section>
@endsection