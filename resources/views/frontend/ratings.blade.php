@extends('frontend.layout')

@section('main_content')
    <section class="columns">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <section class="rating-sections">
                    <h2 class="h3 text-center space-bottom-25">РЕЙТИНГИ</h2>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="rating-section">
                                <div class="rating-section-title">
                                    ПОЛИТИКИ
                                </div>
                                <ul class="rating-section-list">
                                    <li class="rating-section-list-item">
                                        <span class="position">1</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;5,0
                                                </span>
                                        <a href="#">Аваков А. Б.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">2</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,9
                                                </span>
                                        <a href="#">Константинопольский К.K.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">3</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,6
                                                </span>
                                        <a href="#">Петренко В. А.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">4</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,2
                                                </span>
                                        <a href="#">Иваненко А. С.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">5</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;3,0
                                                </span>
                                        <a href="#">Сидаренко Н. В.</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="rating-section">
                                <div class="rating-section-title">
                                    Партии
                                </div>
                                <ul class="rating-section-list">
                                    <li class="rating-section-list-item">
                                        <span class="position">1</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;5,0
                                                </span>
                                        <a href="#">Аваков А. Б.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">2</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,9
                                                </span>
                                        <a href="#">Константинопольский К.K.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">3</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,6
                                                </span>
                                        <a href="#">Петренко В. А.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">4</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,2
                                                </span>
                                        <a href="#">Иваненко А. С.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">5</span>
                                        <span class="rating">
                                                    <i class="fa fa-star" aria-hidden="true"></i>&nbsp;3,0
                                                </span>
                                        <a href="#">Сидаренко Н. В.</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="rating-section">
                                <div class="rating-section-title">
                                    СМИ
                                </div>
                                <ul class="rating-section-list">
                                    <li class="rating-section-list-item">
                                        <span class="position">1</span>
                                        <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>&nbsp;5,0
                                                    </span>
                                        <a href="#">Аваков А. Б.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">2</span>
                                        <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,9
                                                    </span>
                                        <a href="#">Константинопольский К.K.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">3</span>
                                        <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,6
                                                    </span>
                                        <a href="#">Петренко В. А.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">4</span>
                                        <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>&nbsp;4,2
                                                    </span>
                                        <a href="#">Иваненко А. С.</a>
                                    </li>
                                    <li class="rating-section-list-item">
                                        <span class="position">5</span>
                                        <span class="rating">
                                                        <i class="fa fa-star" aria-hidden="true"></i>&nbsp;3,0
                                                    </span>
                                        <a href="#">Сидаренко Н. В.</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection