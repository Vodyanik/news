@extends('frontend.layout')

@section('main_content')
    <?php /* @var \App\Organization $organization */ ?>
    <section class="columns">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <article class="party file">
                    <header>
                        <div class="metadata">
                            <div class="metadata-list">
                                <span class="metadata-item category">Организация</span>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row">
                            <div class="col-md-8">
                                <h2 class="h2 file-heading">{{ $organization->name }}</h2>
                                <div class="site">
                                    <a href="#{{$organization->official_site_url}}">{{$organization->official_site_url}}</a> (офиц.)
                                    {{--, <a href="#">http://nfront-no-ofic.org.ua</a>--}}
                                </div>
                                <ul class="socials border bg-light">
                                    @if(!empty($organization->twitter_url))
                                        <li class="socials-item twitter">
                                            <a href="{{ $organization->twitter_url }}" style="background: #3399CC; color: white; width: 50px; height: 50px;"><i class="fa fa-twitter fa-3x" style="line-height: 50px;" aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                    @if(!empty($organization->facebook_url))
                                        <li class="socials-item facebook">
                                            <a href="{{ $organization->facebook_url }}" style="background: #336699; color: white; width: 50px; height: 50px;"><i class="fa fa-facebook-f fa-3x" style="line-height: 50px;" aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                </ul>
                                <div id="reit" style="background-image: url('{{ asset('images/reit.png') }}'); background-size: 100%; background-repeat: no-repeat; max-width: 550px; height: 50px; margin-top: 50px;">

                                </div>
                                @if($organization->leader)
                                    <ul class="pairs-name-value">
                                        <li class="pairs-name-value-item">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="name">
                                                        РУКОВОДИТЕЛЬ
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="value border photo" style="padding-left: 0px;">
                                                        {{--<span class="img" style="background-image: url('{{$organization->leader->photo_url}}')"></span>--}}
                                                        <img src="{{ asset($organization->leader->photo_url) }}" width="150px" style="border-radius: 50%; border: 2px solid #999999">
                                                        <span class="text" style="margin-top: 20px;"><a href="{!! route('politician', $organization->leader->id) !!}">{{$organization->leader->full_name}}</a>
                                                            <i class="fa fa-star" aria-hidden="true" style="margin-left: 20px;"></i>&nbsp;{{$organization->leader->rounded_rating}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="image-wrapper">
                                    <img src="{{ $organization->logo_url }}" alt="">
                                </div>
                            </div>
                        </div>

                        <ul class="tabs-navigation nav nav-tabs">
                            <li class="tabs-navigation-item active"><a href="#history">ИСТОРИЯ</a></li>
                            <li class="tabs-navigation-item">
                                <a href="#promises">ОБЕЩАНИЯ
                                    <span class="counters">
                                        {{$organization->confirmed_promises->count()}}/{{$organization->promises->count()}}
                                    </span>
                                </a>
                            </li>
                            <li class="tabs-navigation-item">
                                <a href="#employees">
                                    СОТРУДНИКИ
                                    <span class="counters">
                                        {{$organization->politicians->count()}}
                                    </span>
                                </a>
                            </li>
                        </ul>

                        <div class="tabs-content border tab-content">
                            <div id="history" class="tabs-content-item history tab-pane fade in active">
                                {!! $organization->history !!}
                            </div>
                            <div id="promises" class="tabs-content-item promises tab-pane fade">
                                <ul class="marked-list">
                                    @foreach($organization->promises as $promise)
                                        <li class="marked-list-item p">
                                            <i class="fa @if($promise->confirmed) fa-check @elseif($promise->failed) fa-minus @else fa-ellipsis-h @endif" aria-hidden="true"></i>
                                            {{$promise->description}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div id="deputies" class="tabs-content-item deputies tab-pane fade in">

                            </div>
                            <div id="employees" class="tabs-content-item employees tab-pane fade in">
                                <div class="pairs-name-value">
                                    <div class="row">
                                        @foreach($organization->politicians as $politician)
                                            <div class="col-sm-6 col-md-4">
                                                <div class="pairs-name-value-item">
                                                    <div class="value border photo">
                                                        <span class="img" style="background-image: url('{{$politician->photo_url}}')"></span>
                                                        <span class="text">
                                                            <a href="{!! route('politician', $politician->id) !!}">
                                                                {{$politician->full_name}}
                                                            </a>
                                                        </span>
                                                        <span class="rating"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;{{$politician->rounded_rating}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </article>
                {{--@include('frontend.news_footer')--}}

                <section class="previews">
                    <h2 class="h2 previews-heading text-center">{{ $organization->name }} новости:</h2>
                    <div class="row">
                        @foreach($organizationNews->take(3) as $news_item)
                            <div class="col-lg-4 col-sm-6">
                                <article class="previews-item">
                                    <span class="image" data-ratio="4:3" style="background-image: url({{ $news_item->image }})"></span>
                                    <main>
                                        <h3 class="h3 heading">
                                            <a href="{{route('news', [$news_item->slug])}}">
                                                {{$news_item->title}}
                                            </a>
                                        </h3>
                                    </main>
                                    <footer>
                                        <section class="tags">
                                            <ul class="tags-list">
                                                @foreach($news_item->tags as $tag)
                                                    <li class="tags-item"><a href="{{route('news_by_tag', $tag->name)}}">#{{$tag->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </section>
                                        <section class="metadata">
                                            <form action="" class="metadata-list">
                                                <a href="{{ route('like_news', $news_item->id) }}" class="metadata-item likes">
                                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                    {{$news_item->likes_count}}
                                                </a>
                                                <a href="{{route('dislike_news', $news_item->id)}}" class="metadata-item dislikes">
                                                    <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                                    {{$news_item->dislikes_count}}
                                                </a>
                                                <span class="metadata-item published pull-right">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    {{$news_item->created_at}}
                                                    </span>
                                            </form>
                                        </section>
                                    </footer>
                                </article>
                            </div>
                        @endforeach()
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $( document ).ready(function() {
            const minReit = 1;
            const maxReit = 5;
            var width = $('#reit').width();
            var politicanReit = "{{ $organization->rating }}";

            var widthMark = width * politicanReit / 100;

            var reitMark = widthMark * 4 / width;

            var html = '<div style="margin-left: \n' + (widthMark - 12) + 'px;"><i class="fa fa-map-marker fa-3x" style="margin-top: -22px;" aria-hidden="true"></i><p style="margin-top: 10px;"><b>\n' + (reitMark + 1).toFixed(1) + '</b></p></div>\n';

            $('#reit').append(html);
        });
    </script>
@endsection