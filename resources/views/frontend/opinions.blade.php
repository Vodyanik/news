@extends('frontend.layout')

@section('main_content')
    <h1>Уважаемые читатели.</h1>
    <p>
        Предложите тему для обсуждения для наших экспертов. Если Вы хотите высказать свое мнение в формате развернутого материала — напишите нам
        <a href="mailto:info@smartnews.in.ua">info@smartnews.in.ua</a>.
        Мы обязательно с Вами свяжемся
    </p>

    <div class="col-lg-12">
        <section class="previews" id="news_container">
            <div class="row" id="news_container_row">
                @include('frontend.partials.news_block', ['news' => $opinions])
            </div>
        </section>
    </div>
@endsection
