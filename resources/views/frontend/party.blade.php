@extends('frontend.layout')

@section('main_content')
    <section class="columns">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <article class="party file">
                    <header>
                        <div class="metadata">
                            <div class="metadata-list">
                                <span class="metadata-item category">ПАРТИЯ</span>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="birthday">Основана <span class="years-old">{{$party->creation_date->diffForHumans()}}</span></div>
                                <h2 class="h2 file-heading">{{$party->name}}</h2>
                                <div class="site">
                                    <a href="#">{{$party->official_site_url}}</a>
                                </div>
                                <ul class="socials border bg-light">
                                    @if(!empty($party->twitter_url))
                                        <li class="socials-item twitter">
                                            <a href="{{ $party->twitter_url }}" style="background: #3399CC; color: white; width: 50px; height: 50px;"><i class="fa fa-twitter fa-3x" style="line-height: 50px;" aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                    @if(!empty($party->facebook_url))
                                        <li class="socials-item facebook">
                                            <a href="{{ $party->facebook_url }}" style="background: #336699; color: white; width: 50px; height: 50px;"><i class="fa fa-facebook-f fa-3x" style="line-height: 50px;" aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                </ul>
                                <div id="reit" style="background-image: url('{{ asset('images/reit.png') }}'); background-size: 100%; background-repeat: no-repeat; max-width: 550px; height: 50px; margin-top: 50px;">

                                </div>
                                <ul class="pairs-name-value">
                                    @if($party->leader)
                                        <li class="pairs-name-value-item">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="name">
                                                        ЛИДЕР
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="value border photo" style="padding-left: 0px;">
                                                        {{--<span class="img" style="background-image: url('{{$party->leader->photo_url}}'); border-radius: 50%; width: 100px; height: 100px;"></span>--}}
                                                        @if(!empty($party->leader->photo_url))
                                                            <img src="{{ asset($party->leader->photo_url) }}" width="150px" style="border-radius: 50%; border: 2px solid #999999;">
                                                        @endif
                                                        <span class="text" style="margin-top: 20px;"><a href="{{route('politician', [$party->leader->id])}}">{{$party->leader->full_name}}</a>
                                                            <i class="fa fa-star" aria-hidden="true" style="margin-left: 20px;"></i>&nbsp;{{$party->leader->rounded_rating}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                    <li class="pairs-name-value-item">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="name">
                                                    ИДЕОЛОГИЯ
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="value">
                                                    {{$party->ideology}}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="pairs-name-value-item">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="name">
                                                    МЕСТ В ПАРЛАМЕНТЕ
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="value">
                                                    149
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="image-wrapper">
                                    <img src="{{$party->logo_url}}" alt="">
                                </div>
                            </div>
                        </div>

                        <ul class="tabs-navigation nav nav-tabs">
                            <li class="tabs-navigation-item active"><a href="#biography">БИОГРАФИЯ</a></li>
                            <li class="tabs-navigation-item"><a href="#promises">ОБЕЩАНИЯ<span class="counters">5/17</span></a></li>
                            <li class="tabs-navigation-item"><a href="#deputies">ДЕПУТАТЫ</a></li>
                            <li class="tabs-navigation-item"><a href="#history">ИСТОРИЯ</a></li>
                        </ul>

                        <div class="tabs-content border tab-content">
                            <div id="biography" class="tabs-content-item biography tab-pane fade in active">
                                {!! $party->history !!}
                            </div>
                            <div id="promises" class="tabs-content-item promises tab-pane fade">
                                <ul class="marked-list">
                                    @foreach($party->promises as $promise)
                                        <li class="marked-list-item p">
                                            <i class="fa @if($promise->confirmed) fa-check @elseif($promise->failed) fa-minus @else fa-ellipsis-h @endif" aria-hidden="true"></i>
                                            {{$promise->description}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div id="deputies" class="tabs-content-item deputies tab-pane fade in">

                            </div>
                            <div id="history" class="tabs-content-item history tab-pane fade in">
                                <ul class="tabs-navigation border-around nav nav-tabs">
                                    <li class="tabs-navigation-item active"><a href="#history-2016">2016</a></li>
                                    <li class="tabs-navigation-item"><a href="#history-2015">2015</a></li>
                                    <li class="tabs-navigation-item"><a href="#history-2014">2014</a></li>
                                    <li class="tabs-navigation-item"><a href="#history-2013">2013</a></li>
                                    <li class="tabs-navigation-item"><a href="#history-old">2010-2005</a></li>
                                </ul>
                                <div class="tabs-content border-around tab-content">
                                    <div id="history-2016" class="tabs-content-item tab-pane fade in active">
                                        <p>2016...</p>
                                    </div>
                                    <div id="history-2015" class="tabs-content-item tab-pane fade in">
                                        <p>2015...</p>
                                    </div>
                                    <div id="history-2014" class="tabs-content-item tab-pane fade in">
                                        <p>2014...</p>
                                    </div>
                                    <div id="history-2013" class="tabs-content-item tab-pane fade in">
                                        <p>2013...</p>
                                    </div>
                                    <div id="history-old" class="tabs-content-item tab-pane fade in">
                                        <p>2010-2005...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </article>
                @include('frontend.news_footer')
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $( document ).ready(function() {
            const minReit = 1;
            const maxReit = 5;
            var width = $('#reit').width();
            var politicanReit = "{{ $party->rating }}";

            var widthMark = width * politicanReit / 100;

            var reitMark = widthMark * 4 / width;

            var html = '<div style="margin-left: \n' + (widthMark - 12) + 'px;"><i class="fa fa-map-marker fa-3x" style="margin-top: -22px;" aria-hidden="true"></i><p style="margin-top: 10px;"><b>\n' + (reitMark + 1).toFixed(1) + '</b></p></div>\n';

            $('#reit').append(html);
        });
    </script>
@endsection