@if(empty(auth()->user()))
<p>
    <a href="http://smartnews.dev/login">Зайдите на сайт</a> или <a href="http://smartnews.dev/register">зарегистрируйтесь</a>, чтобы оставить комментарий и получить доступ к дополнительным функциям
</p>
@endif
<form action="{{ route('front.comment', ['id' => $news->id]) }}" method="GET">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="exampleTextarea"><b>Оставить отзыв:</b> </label>
        <textarea name="text" class="form-control" id="exampleTextarea" rows="3"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Отправить</button>
</form>

@if($comments->count() != 0)
    <h3>Комментарии пользователей:</h3>
@endif
<ul class="list-group">
    @foreach($comments as $comment)
        <div>
            <span>
                <b>{{ $comment->user->name }}:</b>
            </span>
            <span style="float: right">
                {{ $comment->updated_at }}
            </span>
        </div>
        <li class="list-group-item" style="margin-bottom: 20px;">{{ $comment->text }}</li>
    @endforeach
</ul>

@if($comments->count() >= 5)
    <div class="col-md-12">
        <p style="text-align: center">
            <button type="button" class="btn btn-primary ajax-load-more" itter="1">Больше коментариев</button>
        </p>
    </div>
@endif

@section('script')
<script>
    $( document ).ready(function() {
        $('.ajax-load-more').click(function () {
            var itter = $('.ajax-load-more').attr('itter');
            $.ajax({
                url: 'load-more-comments/' + {{ $news->id }} + '/' + itter,
                success: function (data) {
                    var html = '';
                    for (var i = 0; i < data.data.length; i++) {
                        html += '<div>\n' +
                        '            <span>\n' +
                        '                <b>\n' + data.data[i].user.name + ':</b>\n' +
                        '            </span>\n' +
                        '            <span style="float: right">\n' +
                        '                \n' + data.data[i].created_at + '\n' +
                        '            </span>\n' +
                        '        </div>\n' +
                        '        <li class="list-group-item" style="margin-bottom: 20px;">\n' + data.data[i].text + '</li>';
                    }

                    $('.list-group').append(html);
                    itter++;
                    $('.ajax-load-more').attr('itter', itter);
                }
            });
        });
    });
</script>
@endsection
