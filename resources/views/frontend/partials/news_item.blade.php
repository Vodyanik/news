<div class="col-lg-4 col-sm-6">
    <article class="previews-item" data-news_id="{{$news_item->id}}">
        <header>
{{--            <div class="fact-checking {{$news_item->fact_checking_level}}" style="opacity: 1; border-radius: 40px; border-width: 60px 60px 0 0; margin-left: -20px; margin-top: -20px;"></div><!--low/medium/high-->--}}
            <div class="fact-checking {{$news_item->fact_checking_level}}"></div><!--low/medium/high-->
            <ul class="labels list-unstyled list-inline">
                @foreach($news_item->parties as $party)
                    @if(!empty($party->icon_url))
                        <li class="labels-item square" style="background-image: url('{{ $party->icon_url }}')">
                        </li>
                    @endif
                @endforeach
                @foreach($news_item->organizations as $organization)
                    @if(!empty($organization->icon_url))
                        <li class="labels-item square" style="background-image: url('{{ $organization->icon_url }}')">
                        </li>
                    @endif
                @endforeach
            </ul>
        </header>
        <main>
            <a href="/news/{{$news_item->slug}}">
                                            <span class="image"
                                                  data-ratio="4:3"
                                                  style="background-image: url('{{$news_item->tn_image}}')">
                                            </span>
            </a>
            <h3 class="h3 heading">
                <a href="/news/{{$news_item->slug}}">
                    {{$news_item->title}}
                </a>
            </h3>
        </main>
        <footer>
            <section class="tags">
                <ul class="tags-list">
                    @foreach($news_item->tags as $tag)
                        <li class="tags-item"><a href="{{route('news_by_tag', $tag->name)}}">#{{$tag->name}}</a></li>
                    @endforeach
                </ul>
            </section>
            <section class="metadata">
                <div id="news_like_reactions_{{$news_item->id}}" class="metadata-list">
                    <a href="#" data-news_id="{{$news_item->id}}"
                       class="metadata-item likes {{$news_item->user_like_reaction() == 'like' ? 'active' : ''}}"
                       id="news_like_{{$news_item->id}}"
                    >
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                        <span class="likes_counter">{{$news_item->likes_count}}</span>
                    </a>
                    <a href="#" data-news_id="{{$news_item->id}}"
                       class="metadata-item dislikes {{$news_item->user_like_reaction() == 'dislike' ? 'active' : ''}}"
                       id="news_dislike_{{$news_item->id}}"
                    >
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                        <span class="dislikes_counter">{{$news_item->dislikes_count}}</span>
                    </a>

                    @if(auth()->check())
                        <a href="#" data-news_id="{{$news_item->id}}"
                           class="metadata-item bookmark {{$news_item->is_bookmarked() ? 'active' : ''}}"
                           id="news_bookmark_{{$news_item->id}}"
                        >
                            <i class="fa fa-bookmark" aria-hidden="true"></i>
                        </a>
                    @endif
                    <span class="metadata-item published pull-right">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        {{$news_item->publication_date->diffForHumans()}}
                    </span>
                </div>
            </section>
        </footer>
    </article>
</div>