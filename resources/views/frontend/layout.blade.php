<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', 'SmartNews')</title>

    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="@yield('meta-description', 'SmartNews - Умные новости')">
    <meta name="keywords" content="@yield('meta-keywords', 'SmartNews Умные Новости')">

    <link rel="stylesheet" href="/css/styles.css">
    <link href="//cdn.quilljs.com/1.2.4/quill.snow.css" rel="stylesheet">

    {!! Feed::link(route('news_feed'),'rss','Feed: News','en') !!}

    @yield('news-meta')
</head>
{{--<body class="index-page">--}}
<body class="">
<div class="basic-wrapper">
    <header class="basic-header">
        <div class="toolbar-wrapper">
            <div class="container">
                <section class="toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            {{--<div class="toolbar-item langs-selector">--}}
                                {{--<a href="#">--}}
                                    {{--<i class="fa fa-globe" aria-hidden="true"></i>&nbsp;--}}
                                    {{--<span>Ru</span>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            <ul class="toolbar-item socials" style="padding: 0px;">
                                <li class="socials-item vk" style="margin: 1px;">
                                    <a class="social-button"
                                       href="https://vk.com/smartnewscomua"
                                       target="_blank"
                                       style="background: #0099CC; border-radius: 0px; width: 50px; height: 50px; line-height: 65px;">
                                        <i class="fa fa-vk fa-3x" aria-hidden="true" style="color: #FFFFFF;"></i>
                                    </a>
                                </li>
                                <li class="socials-item facebook" style="margin: 1px;">
                                    <a class="social-button"
                                       href="https://www.facebook.com/smartnewscomua"
                                       title="Share on Facebook" target="_blank"
                                       style="background: #336699; border-radius: 0px; width: 50px; height: 50px; line-height: 65px;">
                                        <i class="fa fa-facebook-f fa-3x" aria-hidden="true" style="color: #FFFFFF"></i>
                                    </a>
                                </li>
                                <li class="socials-item twitter" style="margin: 1px;">
                                    <a class="social-button"
                                       href="https://twitter.com/smartnewscomua"
                                       target="_blank" style="background: #33CCFF; border-radius: 0px; width: 50px; height: 50px; line-height: 65px;">
                                        <i class="fa fa-twitter fa-3x" aria-hidden="true" style="color: #FFFFFF"></i>
                                    </a>
                                </li>
                                <li class="socials-item telegram" style="margin: 1px;">
                                    <a class="social-button"
                                       href="http://t.me/smartnewscomua"
                                       target="_blank" style="background: #3399CC; border-radius: 0px; width: 50px; height: 50px; line-height: 65px;">
                                        <i class="fa fa-telegram fa-3x" aria-hidden="true" style="color: #FFFFFF"></i>
                                    </a>
                                </li>
                                <li class="socials-item rss" style="margin: 1px;">
                                    <a href="{{route('news_feed')}}" style="background: #FF9933; border-radius: 0px; width: 50px; height: 50px; line-height: 65px;">
                                        <i class="fa fa-rss fa-3x" aria-hidden="true" style="color: #FFFFFF"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 text-right">
                            <form action="{{ route('news_search') }}" method="GET" class="toolbar-item search" style="height: 50px; padding: 0px; margin: 1px 0px;">
                                {{ csrf_field() }}
                                <input type="text" name="search" style="height: 50px; font-size: 20px;">

                                <button class="fa fa-search fa-3x"
                                        aria-hidden="true"
                                        style="background: #FF9966; height: 50px; width: 50px;">
                                </button>
                            </form>

                            <div class="toolbar-item login" style="padding: 0px; margin: 1px 0px;">
                                @if (Auth::guest())
                                    <button type="button"
                                            data-toggle="modal"
                                            data-target="#login-popup" style="padding: 0px;">
                                        <i class="fa fa-user fa-3x" aria-hidden="true" style="background: #009933; height: 50px; width: 50px; font-size: 3em; line-height: 50px;"></i>
                                    </button>
                                @else

                                    <i class="fa fa-user" aria-hidden="true"></i>&nbsp;
                                    @if (Auth::user()->role !== 'visitor' && Auth::user()->role !== 'expert' )
                                        <a href="/backend">{!! Auth::user()->name !!}</a>
                                    @else
                                        <a href="/my_account">{!! Auth::user()->name !!}</a>
                                    @endif
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        (<span>Выйти</span>)
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="container">
            <nav role="navigation" class="menu navbar">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand" style="padding: 0; height: 70px;">
                        <img src="/images/logo.png" alt="Logo" style="height: 70px">
                    </a>
                </div>
                <!-- Навигационное меню -->
                <div id="navbarCollapse" class="collapse navbar-collapse navbar-right">
                    <ul class="menu-list nav navbar-nav">
                        <li class="active"><a href="/" style="font-size: 17px; padding-top: 25px; padding-bottom: 25px;">НОВОСТИ</a></li>
                        {{--<li><a href="{{ route('opinions') }}" style="font-size: 17px; padding-top: 25px; padding-bottom: 25px;">МНЕНИЯ</a></li>--}}
                        <li><a href="{{ route('index.reiting') }}" style="font-size: 17px; padding-top: 25px; padding-bottom: 25px;">РЕЙТИНГИ</a></li>
{{--                        <li><a href="{{ route('forum_main') }}" target="_blank">ОБСУЖДЕНИЯ</a></li>--}}
                        <li><a href="{{ route('make_suggestion') }}" style="font-size: 17px; padding-top: 25px; padding-bottom: 25px;">ПРЕДЛОЖЕНИЯ</a></li>
                        <li><a href="{{ route('take_an_action_page') }}" style="font-size: 17px; padding-top: 25px; padding-bottom: 25px;">ДЕЙСТВИЯ</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    {{--</header>--}}
    <main class="basic-main container">
        @yield('main_content')
    </main>
</div>
<footer class="basic-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="{{ asset('images/logo.png') }}" width="100%">
                <section class="copyright" style="padding-top: 0px;">
                    <div class="copyright-name">
                        Новостной портал SmartNews&copy;&nbsp;2017&nbsp;
                    </div>
                    <div class="copyright-note">
                        При копировании авторских материалов
                        ссылка на сайт обязательна.
                    </div>
                    <div class="copyright-note">
                        Мнение редакции сайта SmartNews может не совпадать
                        с мнением автором новостей и статей
                    </div>
                </section>
            </div>
            <div class="col-md-offset-1 col-md-5">
                <hr class="double">
                <div class="row">
                    <div class="col-md-6">
                        <section class="slogan">
                            Умные новости.<br>
                            Проверенные новости.<br>
                            Ваши новости
                        </section>
                        <hr>
                        <section class="menu-contacts">
                            <ul class="list-unstyled">
                                <li><a href="{{  route('about_us_page') }}">О НАС</a></li>
                                <li><a href="#">СВЯЗАТЬСЯ</a></li>
                            </ul>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <nav class="menu">
                            <ul class="list-unstyled">
                                <li><a href="/">НОВОСТИ</a></li>
                                <li><a href="{{ route('opinions') }}">МНЕНИЯ</a></li>
                                {{--<li><a href="{{ route('forum_main') }} ">ОБСУЖДЕНИЯ</a></li>--}}
                                <li><a href="{{ route('make_suggestion') }}">ПРЕДЛОЖЕНИЯ</a></li>
                                <li><a href="{{ route('take_an_action_page') }}">ДЕЙСТВИЯ</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <section class="search">
                    <h4 class="h4 heading">ПОИСК ПО НОВОСТЯМ</h4>
                    {{
                        Form::open([
                            'method' => 'GET',
                            'route' => 'news_search',
                            'class' => "form"
                        ])
                    }}
                        <input type="text" name="search" placeholder="ПОИСК">
                    {{ Form::close() }}
                </section>
                <section class="socials-links">
                    <ul class="toolbar-item socials" style="padding: 0px;">
                        <li class="socials-item vk" style="margin: 1px;">
                            <a class="social-button"
                               href="https://vk.com/smartnewscomua"
                               target="_blank"
                               style="background: #0099CC; border-radius: 0px; width: 30px; height: 30px; line-height: 40px;">
                                <i class="fa fa-vk fa-2x" aria-hidden="true" style="color: #FFFFFF;"></i>
                            </a>
                        </li>
                        <li class="socials-item facebook" style="margin: 1px;">
                            <a class="social-button"
                               href="https://www.facebook.com/smartnewscomua"
                               title="Share on Facebook" target="_blank"
                               style="background: #336699; border-radius: 0px; width: 30px; height: 30px; line-height: 40px;">
                                <i class="fa fa-facebook-f fa-2x" aria-hidden="true" style="color: #FFFFFF"></i>
                            </a>
                        </li>
                        <li class="socials-item twitter" style="margin: 1px;">
                            <a class="social-button"
                               href="https://twitter.com/smartnewscomua"
                               target="_blank" style="background: #33CCFF; border-radius: 0px; width: 30px; height: 30px; line-height: 40px;">
                                <i class="fa fa-twitter fa-2x" aria-hidden="true" style="color: #FFFFFF"></i>
                            </a>
                        </li>
                        <li class="socials-item telegram" style="margin: 1px;">
                            <a class="social-button"
                               href="http://t.me/smartnewscomua"
                               target="_blank" style="background: #3399CC; border-radius: 0px; width: 30px; height: 30px; line-height: 40px;">
                                <i class="fa fa-telegram fa-2x" aria-hidden="true" style="color: #FFFFFF"></i>
                            </a>
                        </li>
                        <li class="socials-item rss" style="margin: 1px;">
                            <a href="{{route('news_feed')}}" style="background: #FF9933; border-radius: 0px; width: 30px; height: 30px; line-height: 40px;">
                                <i class="fa fa-rss fa-2x" aria-hidden="true" style="color: #FFFFFF"></i>
                            </a>
                        </li>
                    </ul>
                </section>
                <section class="vendors">
                    <div class="vendor-speech-generator">
                        <div class="vendor-speech-generator-image">
                            <a href="https://responsivevoice.org">
                                <img src="/images/responsivevoice-logo.png" alt="">
                            </a>
                        </div>
                        <div class="vendor-speech-generator-text">
                            <a href="https://responsivevoice.org">ResponsiveVoice</a> used under<br>
                            <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Non-Commercial License</a>
                        </div>
                    </div>
                </section>
                <!--LiveInternet counter-->
                <script type="text/javascript">
                    document.write("<a href='//www.liveinternet.ru/click' "+
                        "target=_blank><img src='//counter.yadro.ru/hit?t44.6;r"+
                        escape(document.referrer)+((typeof(screen)=="undefined")?"":
                        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                        ";h"+escape(document.title.substring(0,80))+";"+Math.random()+
                        "' alt='' title='LiveInternet' "+
                        "border='0' width='31' height='31' style='visibility: hidden'><\/a>")
                </script>
                <!--/LiveInternet-->
            </div>
        </div>
    </div>
</footer>

<section id="modals"><!-- todo: must be updated to replace separate page -->
    <div class="modal fade" id="user-template" role="dialog">
        <div class="modal-dialog popup-container">
            <div class="popup user-template-popup">
                <div class="popup-close">
                    <button type="button"
                            data-dismiss="modal">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="form-container">
                    <form action="" class="form-container form-block">
                        <div class="form-heading">
                            Шаблон 1
                            <a href="#"
                               class="form-link">
                                Очистить полностью
                            </a>
                        </div>
                        <div class="form-note">
                            Настройте ленту под себя — последний сохраненный шаблон будет применен в дополнение к фильтрам на главной странице.
                        </div>
                        <div class="form-block">
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label">
                                                СОРТИРОВКА
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="form-element form-radio">
                                                        <input id="s1" name="sort" type="radio">
                                                        <label for="s1">По дате</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-8">
                                                    <div class="form-element form-radio">
                                                        <input id="s2" name="sort" type="radio">
                                                        <label for="s2">По актуальности</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label two-rows">
                                                ПОКАЗЫВАТЬ ПО ТЕГАМ<br>
                                                <span>То, что интересно Вам</span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-element form-input underline">
                                                <input type="text" placeholder="Начните вводить тег">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label">
                                                <a href="#" class="form-link">Очистить</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <ul class="tags-list bg-light wrap">
                                                <li class="tags-item"><a href="#">#тег</a>&nbsp;<a href="#"><i class="fa fa-close" aria-hidden="true"></i></a></li>
                                                <li class="tags-item"><a href="#">#тег</a>&nbsp;<a href="#"><i class="fa fa-close" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label two-rows">
                                                ПОПУЛЯРНЫЕ ТЕГИ<br>
                                                <span>То, что интересно людям</span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <ul class="tags-list bg-light wrap bordered-items">
                                                <li class="tags-item"><a href="#">#тег</a>&nbsp;<a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                                                <li class="tags-item"><a href="#">#тег</a>&nbsp;<a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                                                <li class="tags-item"><a href="#">#2016</a>&nbsp;<a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label">
                                                <a href="#" class="form-link">Очистить</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <ul class="tags-list bg-light wrap">
                                                <li class="tags-item"><a href="#">#тег</a>&nbsp;<a href="#"><i class="fa fa-close" aria-hidden="true"></i></a></li>
                                                <li class="tags-item"><a href="#">#тег</a>&nbsp;<a href="#"><i class="fa fa-close" aria-hidden="true"></i></a></li>
                                                <li class="tags-item"><a href="#">#2016</a>&nbsp;<a href="#"><i class="fa fa-close" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label">
                                                БОЛЬШЕ ТЕГОВ
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-element form-input underline">
                                                <input type="text" placeholder="Начните вводить тег">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label">
                                                МЕНЬШЕ ТЕГОВ ТЕГОВ
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-element form-input underline">
                                                <input type="text" placeholder="Начните вводить тег">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label two-rows">
                                                КОЛИЧЕСТВО НОВОСТЕЙ<br>
                                                <span>Отображаются последние новости, подходящие по фильтрам</span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-element form-input underline">
                                                <input type="text" placeholder="Введите количество новостей">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-label two-rows">
                                                ВРЕМЯ ЧТЕНИЯ <br>
                                                <span>Время, которое Вы можете уделять на одну новость.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-element form-select">
                                                <select name="">
                                                    <option value="no_selected" disabled selected>Время (в минутах)</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="5">5</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-sub-block">
                                <div class="form-row">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-element form-button blue">
                                                <button>СОХРАНИТЬ</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="login-popup" role="dialog">
        <div class="modal-dialog popup-container">
            <div class="popup login-popup">
                <div class="popup-close">
                    <button type="button"
                            data-dismiss="modal">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="form-container">
                    <form role="form" method="POST" action="{{ url('/login') }}"
                          class="form-container form-block border-bottom">
                        {{ csrf_field() }}
                        <div class="form-heading space-bottom-25">
                            Войти в систему
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-row{{ $errors->has('email') ? ' error' : '' }}">
                                    <div class="form-label">
                                        ЛОГИН
                                    </div>
                                    <div class="form-element form-input">
                                        <input placeholder="Введите ваш логин"
                                               id="email"
                                               type="email"
                                               name="email"
                                               value="{{ old('email') }}"
                                               required>
                                    </div>
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-row{{ $errors->has('password') ? ' error' : '' }}">
                                    <div class="form-label">
                                        ПАРОЛЬ
                                    </div>
                                    <div class="form-element form-input">
                                        <input id="password" type="password" name="password" placeholder="Введите ваш пароль" required>
                                    </div>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-label">
                                    &nbsp;
                                </div>
                                <div class="form-element form-button blue">
                                    <button type="submit">ВОЙТИ</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Запомнить меня
                                </label>
                            </div>
                        </div>
                        <div class="form-row">
                            <a class="form-link" href="{{ url('/password/reset') }}">Вспомнить пароль</a>
                        </div>
                    </form>
                </div>
                <div class="form-container">
                    <form action="" class="form-container form-block">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-heading">
                                    Зарегистрироваться
                                </div>
                                <div class="form-note">
                                    Добавляйте в закладки, участвуйте в обсуждениях и выражайте своё мнение
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-element form-button blue">
                                    <a href="{{ url('/register') }}">СОЗДАТЬ ПРОФИЛЬ</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="http://code.responsivevoice.org/responsivevoice.js"></script>
<script src="/js/scripts.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43736679 = new Ya.Metrika({
                    id:43736679,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43736679" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

@yield('script')

</body>
</html>