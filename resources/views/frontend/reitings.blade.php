@extends('frontend.layout')

@section('main_content')

    <hr>

    <div class="row">
        <ol style="list-style-type: none;">
            <h3><a name="party"></a>Партии</h3>
            @foreach($parties as $partysCol)
                <div class="col-md-4">
                    @foreach($partysCol as $party)
                        <div class="row" style="margin-top: 10px;">
                            <li>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <img src="{{ asset($party->logo_url) }}" width="100%" style="max-width: 100px;">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-8" style="margin-top: 35px;">
                                    <a href="{{ route('party', ['id' => $party->id]) }}">{{ $party->name }}</a>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4" style="margin-top: 35px;">
                                    <span style="color: #666; position: absolute">
                                        <i class="fa fa-star" aria-hidden="true"></i>{{ $party->rounded_rating }}
                                    </span>
                                </div>
                            </li>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </ol>
    </div>

    <hr>

    <div class="row">
        <ol style="list-style-type: none">
            <h3><a name="news"></a>СМИ</h3>
            @foreach($newsSources as $newsCol)
                <div class="col-md-4">
                    @foreach($newsCol as $news)
                        <div class="row" style="margin-top: 10px;">
                            <li>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <img src="{{ asset($news->logo_url) }}">
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-7" style="margin-top: 6px;">
                                    <a href="#">{{ $news->name }}</a>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-3" style="margin-top: 6px;">
                                <span style="color: #666; position: absolute;">
                                    <i class="fa fa-star" aria-hidden="true"></i>{{ $news->rounded_rating }}
                                </span>
                                </div>
                            </li>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </ol>
    </div>

    <hr>

    <div class="row">
        <ol style="list-style-type: none">
            <h3><a name="politic"></a>Политики</h3>
            @foreach($politicians as $politicCol)
                <div class="col-md-4">
                    @foreach($politicCol as $politic)
                        <div class="row" style="margin-top: 20px;">
                            <li>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <img src="{{ asset($politic->photo_url) }}" width="100%" style="border-radius: 50%; max-width: 100px;">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-8" style="margin-top: 35px;">
                                    <a href="{{ route('politician', ['id' => $politic->id]) }}">{{ $politic->full_name }}</a>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-4" style="margin-top: 35px;">
                                    <span style="color: #666; position: absolute">
                                        <i class="fa fa-star" aria-hidden="true"></i>{{ $politic->rounded_rating }}
                                    </span>
                                </div>
                            </li>
                        </div>

                    @endforeach
                </div>
            @endforeach
        </ol>
    </div>

@endsection