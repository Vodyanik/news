@extends('frontend.layout')

@section('main_content')
    <section class="columns">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <article class="politician file">
                    <header>
                        <div class="metadata">
                            <div class="metadata-list">
                                <span class="metadata-item category">ПОЛИТИК</span>
                                <span class="metadata-item rating">

                                            </span>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="birthday"><span class="years-old">{{ str_replace(' назад', '', $politician->birthday->diffForHumans())}}</span></div>
                                <h2 class="h2 file-heading">{{$politician->full_name}}</h2>
                                <div class="site">
                                    <a class="site" href="http://{{$politician->official_site_url}}">{{$politician->official_site_url}}</a>
                                </div>
                                <ul class="socials border bg-light">
                                    @if(!empty($politician->twitter_url))
                                        <li class="socials-item twitter">
                                            <a href="{{ $politician->twitter_url }}" style="background: #3399CC; color: white; width: 50px; height: 50px;"><i class="fa fa-twitter fa-3x" style="line-height: 50px;" aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                    @if(!empty($politician->facebook_url))
                                        <li class="socials-item facebook">
                                            <a href="{{ $politician->facebook_url }}" style="background: #336699; color: white; width: 50px; height: 50px;"><i class="fa fa-facebook-f fa-3x" style="line-height: 50px;" aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                </ul>
                                <div id="reit" style="background-image: url('{{ asset('images/reit.png') }}'); background-size: 100%; background-repeat: no-repeat; max-width: 550px; height: 50px; margin-top: 50px;">

                                </div>
                                <ul class="pairs-name-value">
                                    @if($politician->party)
                                        <li class="pairs-name-value-item">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="name">
                                                        В ПАРТИИ
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="value border" style="padding-top: 0px; line-height: 0px;">
                                                        <div class="row" style="margin-top: 20px;">
                                                            <img src="{{ $politician->party->logo_url }}" width="200px">
                                                            <span class="text" style="margin-top: 20px;"><a href="{{route('party', $politician->party->id)}}">«{{$politician->party->name}}» </a><i class="fa fa-star" aria-hidden="true" style="margin-left: 20px;"></i>&nbsp; {{$politician->party->rounded_rating}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                    {{--@if($politician->organization)--}}
                                        {{--<li class="pairs-name-value-item">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-sm-12">--}}
                                                    {{--<div class="name">--}}
                                                        {{--В ОРГАНИЗАЦИИ--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-12">--}}
                                                    {{--<div class="value border" style="padding-top: 0px; line-height: 0px;">--}}
                                                        {{--<div class="row">--}}
                                                            {{--<img src="{{ $politician->organization->logo_url }}" width="200px">--}}
                                                            {{--<span class="text" style="margin-top: 20px;"><a href="{{route('party', $politician->organization->id)}}">«{{$politician->organization->name}}» </a><i class="fa fa-star" aria-hidden="true" style="margin-left: 20px;"></i>&nbsp; {{$politician->organization->rounded_rating}}</span>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                    {{--@endif--}}
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="image" data-ratio="8:9" style="background-image: url('{{$politician->photo_url}}'); border: 3px solid #999999; border-radius: 3px;"></div>
                            </div>
                        </div>

                        <ul class="tabs-navigation nav nav-tabs">
                            <li class="tabs-navigation-item active"><a href="#biography">БИОГРАФИЯ</a></li>
                            <li class="tabs-navigation-item"><a href="#promises">ОБЕЩАНИЯ<span class="counters">5/17</span></a></li>
                        </ul>

                        <div class="tabs-content border tab-content">
                            <div id="biography" class="tabs-content-item biography tab-pane fade in active">
                                {!! $politician->biography !!}
                            </div>
                            <div id="promises" class="tabs-content-item promises tab-pane fade">
                                <ul class="marked-list-list">
                                    @foreach($politician->promises as $promise)
                                        <li class="marked-list-item p">
                                            <i class="fa @if($promise->confirmed) fa-check @elseif($promise->failed) fa-minus @else fa-ellipsis-h @endif" aria-hidden="true"></i>
                                            {{$promise->description}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </main>
                </article>
                {{--@include('frontend.news_footer')--}}
                <section class="previews">
                    <h2 class="h2 previews-heading text-center">{{ $politician->full_name }} новости:</h2>
                    <div class="row">
                        @foreach($politicianNews->take(3) as $news_item)
                            <div class="col-lg-4 col-sm-6">
                                <article class="previews-item">
                                    <span class="image" data-ratio="4:3" style="background-image: url({{ $news_item->image }})"></span>
                                    <main>
                                        <h3 class="h3 heading">
                                            <a href="{{route('news', [$news_item->slug])}}">
                                                {{$news_item->title}}
                                            </a>
                                        </h3>
                                    </main>
                                    <footer>
                                        <section class="tags">
                                            <ul class="tags-list">
                                                @foreach($news_item->tags as $tag)
                                                    <li class="tags-item"><a href="{{route('news_by_tag', $tag->name)}}">#{{$tag->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </section>
                                        <section class="metadata">
                                            <form action="" class="metadata-list">
                                                <a href="{{ route('like_news', $news_item->id) }}" class="metadata-item likes">
                                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                    {{$news_item->likes_count}}
                                                </a>
                                                <a href="{{route('dislike_news', $news_item->id)}}" class="metadata-item dislikes">
                                                    <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                                    {{$news_item->dislikes_count}}
                                                </a>
                                                <span class="metadata-item published pull-right">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    {{$news_item->created_at}}
                                                    </span>
                                            </form>
                                        </section>
                                    </footer>
                                </article>
                            </div>
                        @endforeach()
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $( document ).ready(function() {
            const minReit = 1;
            const maxReit = 5;
            var width = $('#reit').width();
            var politicanReit = "{{ $politician->rating }}";

            var widthMark = width * politicanReit / 100;
            console.log(politicanReit);
            var reitMark = widthMark * 4 / width;

            var html = '<div style="margin-left: \n' + (widthMark - 12) + 'px;"><i class="fa fa-map-marker fa-3x" style="margin-top: -22px;" aria-hidden="true"></i><p style="margin-top: 10px;"><b>\n' + (reitMark + 1).toFixed(1) + '</b></p></div>\n';

            $('#reit').append(html);
        });
    </script>
@endsection