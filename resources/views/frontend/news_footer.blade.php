<section class="previews">
    <h2 class="h2 previews-heading text-center">Новости по теме</h2>
    <div class="row">
        {{--{{ dump($news->tags[0]->news()->orderBy('created_at', 'desc')->take(3)->get()) }}--}}
        {{--{{ dump($news->tags[0]->news()->where('id', '!=', $news->id)->orderBy('created_at', 'desc')->take(3)->get()) }}--}}

        {{--@if(isset($news))--}}
            {{--@foreach($news->tags[0]->news()->where('id', '!=', $news->id)->orderBy('created_at', 'desc')->take(3)->get() as $news_item)--}}
        {{--@endif--}}
        {{--@if(!isset($news))--}}
            {{--@foreach (App\News::published()->take(3)->get() as $news_item)--}}
        {{--@endif--}}
        {{--@foreach($news as $news_item)--}}

        <div class="hidden">
            {{ isset($news) ? $mews = $news->tags[0]->news()->where('id', '!=', $news->id)->orderBy('created_at', 'desc')->take(3)->get() : $mews = App\News::published()->take(3)->get() }}
        </div>

        @foreach($mews as $news_item)
            <div class="col-lg-4 col-sm-6">
                <article class="previews-item">
                    <span class="image" data-ratio="4:3" style="background-image: url({{ $news_item->image }})"></span>
                    <main>
                        <h3 class="h3 heading">
                            <a href="{{route('news', [$news_item->slug])}}">
                                {{$news_item->title}}
                            </a>
                        </h3>
                    </main>
                    <footer>
                        <section class="tags">
                            <ul class="tags-list">
                                @foreach($news_item->tags as $tag)
                                    <li class="tags-item"><a href="{{route('news_by_tag', $tag->name)}}">#{{$tag->name}}</a></li>
                                @endforeach
                            </ul>
                        </section>
                        <section class="metadata">
                            <form action="" class="metadata-list">
                                <a href="{{ route('like_news', $news_item->id) }}" class="metadata-item likes">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                    {{$news_item->likes_count}}
                                </a>
                                <a href="{{route('dislike_news', $news_item->id)}}" class="metadata-item dislikes">
                                    <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                    {{$news_item->dislikes_count}}
                                </a>
                                <span class="metadata-item published pull-right">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    {{$news_item->created_at}}
                                                    </span>
                            </form>
                        </section>
                    </footer>
                </article>
            </div>
        @endforeach()
    </div>
</section>