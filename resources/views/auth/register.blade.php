@extends('frontend.layout')

@section('title', 'Регистрация')

@section('main_content')

    <section class="columns">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <section class="registration">
                    <div class="form-container">
                        <div class="form-heading">
                            Регистрация
                        </div>
                        <div class="form-note">
                            Мы гарантируем анонимность введенных Вами данных.
                            Вся информация будет использоваться исключительно для аналитики.
                        </div>
                        <form role="form" method="POST" action="{{ url('/register') }}"
                              class="form-block border-bottom">
                            {{ csrf_field() }}

                            <div class="form-sub-heading">
                                Основная информация
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-md-push-8">
                                    <div class="form-avatar">

                                    </div>
                                </div>
                                <div class="col-md-8 col-md-pull-4">
                                    <div class="form-sub-block">
                                        <div class="form-row{{ $errors->has('name') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ИМЯ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-input">
                                                        <input type="text"
                                                               placeholder="Ваше имя"
                                                               id="name"
                                                               name="name"
                                                               value="{{ old('name') }}"
                                                               required
                                                               autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('email') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        E-MAIL
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-input">
                                                        <input type="email"
                                                               placeholder="Ваш e-mail"
                                                               id="email"
                                                               name="email"
                                                               value="{{ old('email') }}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-sub-block">
                                        <div class="form-row{{ $errors->has('name') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ПАРОЛЬ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-input">
                                                        <input type="password"
                                                               placeholder="Введите пароль"
                                                               id="password"
                                                               name="password"
                                                               value="{{ old('password') }}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('password-confirm') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ПОДТВЕРЖДЕНИЕ ПАРОЛЯ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-input">
                                                        <input type="password"
                                                               placeholder="Введите подтверждение пароля"
                                                               id="password-confirm"
                                                               name="password_confirmation"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
{{--
                                    <div class="form-sub-block">
                                        <div class="form-row{{ $errors->has('social_network_profile') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        СОЦИАЛЬНАЯ СЕТЬ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-input">
                                                        <input type="text"
                                                               placeholder="Введите адрес вашей страницы в соцсети"
                                                               id="social_network_profile"
                                                               name="social_network_profile"
                                                               value="{{ old('social_network_profile') }}"
                                                               >
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('social_network_profile'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('social_network_profile') }}</strong>
                                        </span>
                                        @endif
                                        </div>
                                    </div>

                                    <div class="form-sub-block">
                                        <div class="form-row{{ $errors->has('sex') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ПОЛ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('sex', App\User::$sex_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('sex'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('sex') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('birthday') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ДЕНЬ РОЖДЕНИЯ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-input">
                                                        <input id="birthday"
                                                               type="date"
                                                               class="datepicker"
                                                               name="birthday">
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('birthday'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('education') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ОБРАЗОВАНИЕ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('education', App\User::$education_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('education'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('education') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('income') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ДОХОД
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('income', App\User::$income_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('income'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('income') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('family_status') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        СЕМЕЙНОЕ ПОЛОЖЕНИЕ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('family_status', App\User::$family_status_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('family_status'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('family_status') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('children') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ДЕТИ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('children', App\User::$children_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('children'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('children') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('employment') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ЗАНЯТОСТЬ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('employment', App\User::$employment_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('employment'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('employment') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('employment_spheres_id') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        СФЕРА ДЕЯТЕЛЬНОСТИ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('employment_spheres_id', App\EmploymentSphere::all()->pluck('name', 'id'), null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('employment_spheres_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('employment_spheres_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('election_activity') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        АКТИВНОСТЬ НА ВЫБОРАХ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('election_activity', App\User::$election_activity_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('election_activity'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('election_activity') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('settlement_type') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        МЕСТО ЖИТЕЛЬСТВА
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('settlement_type', App\User::$settlement_type_mapping, null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('settlement_type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('settlement_type') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-row{{ $errors->has('region_id') ? ' error' : '' }}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">
                                                        ОБЛАСТЬ
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-element form-select">
                                                        {{ Form::select('region_id', App\Region::all()->pluck('name', 'id'), null) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('region_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('region_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
--}}
                                    <div class="form-sub-block">
                                        <div class="form-element form-button blue inline">
                                            <button type="submit">СОЗДАТЬ ПРОФИЛЬ</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form action=""
                              class="form-block">
                            <div class="form-sub-heading">
                                Привяжите профиль к социальным сетям
                            </div>
                            <div class="form-note">
                                Для быстрой авторизации
                            </div>
                            <ul class="socials border bg-light">
                                <li class="socials-item vk">
                                    <a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                                </li>
                                <li class="socials-item facebook">
                                    <a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i></a>
                                </li>
                                <li class="socials-item twitter">
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true

"></i></a>
                                </li>
                            </ul>
                            <div class="form-sub-block">
                                <div class="form-element form-button blue inline">
                                    <button>ПРИВЯЗАТЬ ПРОФИЛЬ</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection
