@extends('frontend.layout')

@section('title', 'Регистрация')

@section('main_content')

<section class="columns">
        <div class="row">
            <div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-xs-offset-1 col-xs-10">
                <div class="form-container">
                    <form role="form" method="POST" action="{{ url('/login') }}"
                          class="form-container form-block border-bottom">
                        {{ csrf_field() }}
                        <div class="form-heading space-bottom-25">
                            Войти в систему
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-row{{ $errors->has('email') ? ' error' : '' }}">
                                    <div class="form-label">
                                        ЛОГИН
                                    </div>
                                    <div class="form-element form-input">
                                        <input placeholder="Введите ваш логин"
                                               id="email"
                                               type="email"
                                               name="email"
                                               value="{{ old('email') }}"
                                               required>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-row{{ $errors->has('password') ? ' error' : '' }}">
                                    <div class="form-label">
                                        ПАРОЛЬ
                                    </div>
                                    <div class="form-element form-input">
                                        <input id="password" type="password" name="password" placeholder="Введите ваш пароль" required>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-label">
                                    &nbsp;
                                </div>
                                <div class="form-element form-button blue">
                                    <button type="submit">ВОЙТИ</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Запомнить меня
                                </label>
                            </div>
                        </div>
                        <div class="form-row">
                            <a class="form-link" href="{{ url('/password/reset') }}">Вспомнить пароль</a>
                        </div>
                    </form>
                </div>
                <div class="form-container">
                    <form action="" class="form-container form-block">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-heading">
                                    Зарегистрироваться
                                </div>
                                <div class="form-note">
                                    Добавляйте в закладки, участвуйте в обсуждениях и выражайте своё мнение
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-element form-button blue">
                                    <a href="{{ url('/register') }}">СОЗДАТЬ ПРОФИЛЬ</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection
