
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./quill');
require('jquery-datetimepicker/build/jquery.datetimepicker.full');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('tag-editor', require('./components/TagEditor.vue'));

$('document').ready(function(){
    $('.chosen').chosen({
        no_results_text: 'Совпадения не найдены',
        placeholder_text_multiple: 'Выберите одну или несколько опций',
        placeholder_text_single: 'Выберите опцию'
    });

    var datepicker = $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1900:+0',
        dateFormat: "yy-mm-dd"
    }).attr('readonly', true);

    $.datetimepicker.setLocale('ru');
    $('.datetimepicker').datetimepicker({
        format:'Y-m-d H:i:s',
        mask: true
    });

    new Vue({
        el: '.vue_app'
    });
});