<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            'Винницкая',
            'Волынская',
            'Днепропетровская',
            'Донецкая',
            'Житомирская',
            'Закарпатская',
            'Запорожская',
            'Івано-Франковская',
            'Киевская',
            'г.Киев',
            'Кировоградская',
            'Луганская',
            'Львовская',
            'Николаевская',
            'Одесская',
            'Полтавская',
            'Ровненская',
            'Сумская',
            'Тернопольськая',
            'Харковская',
            'Херсонская',
            'Хмельницкая',
            'Черкасская',
            'Черновецкая',
            'Черниговская',
        ];

        foreach ($regions as $region){
            \App\Region::create(['name' => $region])->save();
        }
    }
}
