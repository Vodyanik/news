<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionsTableSeeder::class);
        $this->call(EmploymentSpheresTableSeeder::class);
        $this->call(NewsSourceSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(PartySeeder::class);
        $this->call(PoliticianSeeder::class);
        $this->call(OrganizationSeeder::class);
        $this->call(PromiseSeeder::class);

        $this->call(AdminUsersSeeder::class);

        $this->call(FakeUsersSeeder::class);
        $this->call(FakeLikesSeeder::class);
    }
}
