<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Главный админ';
        $user->email = 'admin@spektr.org.ua';
        $user->password = bcrypt('admin_pass');
        $user->role = 'admin';
        $user->save();
    }
}
