<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\News::class, 20)->create();

        $tags = App\Tag::all()->pluck('id')->toArray();
        $faker = Faker\Factory::create();

        App\News::all()->each(function($news) use ($faker, $tags){
            $news->tags()->attach($faker->randomElements($tags, $faker->numberBetween(1,7)));
            $news->save();
        });
    }
}
