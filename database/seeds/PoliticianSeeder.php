<?php

use Illuminate\Database\Seeder;
use App\Politician;

class PoliticianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Remove all politicians
        foreach (Politician::all() as $p) {
            $p->delete();
        }

        // Fill new data
        $fh = fopen(__DIR__ . '/../politicians.csv', 'r');
        while($row = fgetcsv($fh, null, ';')) {
            $attributes = [
                'party_id' => $row[0],
                'first_name' => $row[1],
                'middle_name' => empty($row[3]) ? '' : $row[3],
                'last_name' => $row[2],
                'birthday' => \Carbon\Carbon::today(),
                'ideology' => '',
                'program' => '',
                'biography' => '',
                'official_site_url' => '',
                'facebook_url' => '',
                'twitter_url' => '',
                'photo_url' => '',
                'use_fake_rating' => false,
                'fake_rating' => 50,
            ];
            Politician::create($attributes);
        }
    }
}
