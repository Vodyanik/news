<?php

use Illuminate\Database\Seeder;

class FakeLikesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i < 500; $i++) {
            $attrs = [
                'news_id' => \App\News::all()->random(1)->id,
                'user_id' => \App\User::all()->random(1)->id,
                'reaction' => $faker->randomElement(['like', 'dislike'])
            ];

            try {
                \App\Like::create($attrs);
            } catch (Throwable $e) {

            }
        }

    }
}
