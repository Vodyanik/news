<?php

use Illuminate\Database\Seeder;
use App\ActivityField;

class ActivityFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fields = [
            'Образование',
            'Здравоохранение',
            'Военная сфера',
            'Экономика',
            'Политика (внешняя/внутренняя)',
            'Экология',
            'Социальная и гуманитарная сферы',
            'Энергетика',
            'Транспорт и связь',
            'АПК'
        ];

        foreach ($fields as $field_name) {
            ActivityField::create([
                'name' => $field_name
            ]);
        }
    }
}
