<?php

use Illuminate\Database\Seeder;
use App\Promise;
use App\Tag;
use App\Party;

class PromiseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $fh = fopen(__DIR__ . '/../promises.csv', 'r');
        while($row = fgetcsv($fh, null, ';')) {
            $attributes = [
                'quote' => $row[1],
                'description' => $row[2],
                'failed' => $row[5] == 'failed',
                'confirmed' => $row[5] == 'completed',
                'proof_url' => $row[6],
                'comments' => $row[7]
            ];
            $activity_fields = array_map('trim', explode(',', $row[3]));
            $promise = Promise::create($attributes);
            $promise->activity_fields()->sync($activity_fields);
            $tags = array_map('trim', explode(',', $row[5]));
            foreach ($tags as $tag_text) {
                $tag_id = Tag::where('name', '=', $tag_text)->first();
                if ($tag_id) {
                    $promise->tags()->attach($tag_id);
                } else {
                    $tag = Tag::create(['name' => $tag_text]);
                    $promise->tags()->attach($tag->id);
                }
            }
            $party_id = $row[0];
            if (!empty($party_id) && !empty(Party::find($party_id))){
                $promise->parties()->sync([$party_id]);
            }
        }
    }
}
