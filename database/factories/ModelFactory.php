<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'name' => $faker->name . ' (fake)',
        'role' => 'visitor',
        'sex' => $faker->randomElement(['male', 'female']),
        'profile_confirmed' => $faker->boolean(),
        'social_network_profile' => $faker->url,
        'social_network_profile_confirmed' => $faker->boolean(),
        'birthday' => $faker->date(),
        'education' => $faker->randomElement(['a','b','c','d','e']),
        'income' => $faker->randomElement(['low', 'middle', 'high']),
        'family_status' => $faker->randomElement(['widowed','single','married','civil marriage', 'divorced']),
        'children' => $faker->randomElement(['no', 'one', 'two', 'many']),
        'employment' => $faker->randomElement(['student', 'unemployed', 'employed', 'retired']),
        'election_activity' => $faker->randomElement(['0%', '20%', '40%', '60%', '80%', '100%']),
        'settlement_type' => $faker->randomElement(['city', 'village']),
    ];
});

$factory->define(App\NewsSource::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'site_url' => $faker->domainName,
        'logo_path' => $faker->imageUrl(),
        'rating' => $faker->randomFloat(1,0,100),
        'use_fake_rating' => $faker->boolean(),
        'fake_rating' => $faker->randomFloat(1,0,100)
    ];
});

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

$factory->define(App\News::class, function (Faker\Generator $faker) {
    $title = $faker->sentence();
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'preview' => implode('<br />', $faker->sentences(4)),
        'body' => implode('<br />', $faker->sentences(32)),
        'status' => $faker->randomElement(['new', 'edited', 'published']),
        'rating' => $faker->randomFloat(2, 0, 100),
        'news_source_id' => App\NewsSource::all()->random(1)->id,
        'image' => $faker->imageUrl()
    ];
});

$factory->define(App\Politician::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'birthday' => $faker->date(),
        'ideology' => '<p>' . implode('</p><p>', $faker->paragraphs(3)) . '</p>',
        'program' => '<p>' . implode('</p><p>', $faker->paragraphs(10)) . '</p>',
        'biography' => '<p>' . implode('</p><p>', $faker->paragraphs(20)) . '</p>',
        'official_site_url' => $faker->domainName,
        'facebook_url' => 'http://facebook.com/' . $faker->word,
        'twitter_url' => 'http://twitter.com/' . $faker->word,
        'photo_url' => $faker->imageUrl(),
        'use_fake_rating' => $faker->boolean(),
        'fake_rating' => $faker->randomFloat(1,0,100),
        'party_id' => $faker->boolean(80) ? App\Party::all()->random(1)->id : null
    ];
});

$factory->define(App\Party::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'creation_date' => $faker->date(),
        'ideology' => $faker->words(4, true),
        'program' => $faker->realText(12000),
        'history' => $faker->realText(12000),
        'official_site_url' => $faker->domainName,
        'facebook_url' => 'http://facebook.com/' . $faker->word,
        'twitter_url' => 'http://twitter.com/' . $faker->word,
        'logo_url' => $faker->imageUrl(),
        'use_fake_rating' => $faker->boolean(),
        'fake_rating' => $faker->randomFloat(1,0,100)
    ];
});

$factory->define(App\Organization::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'creation_date' => $faker->date(),
        'history' => $faker->realText(12000),
        'official_site_url' => $faker->domainName,
        'facebook_url' => 'http://facebook.com/' . $faker->word,
        'twitter_url' => 'http://twitter.com/' . $faker->word,
        'logo_url' => $faker->imageUrl(),
        'use_fake_rating' => $faker->boolean(),
        'fake_rating' => $faker->randomFloat(1,0,100)
    ];
});


$factory->define(App\Promise::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->text()
    ];
});