<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsHasPoliticalSubjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_politician', function (Blueprint $table) {
            $table->unsignedInteger('news_id')->index();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->unsignedInteger('politician_id')->index();
            $table->foreign('politician_id')->references('id')->on('politicians')->onDelete('cascade');
        });
        Schema::create('news_party', function (Blueprint $table) {
            $table->unsignedInteger('news_id')->index();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->unsignedInteger('party_id')->index();
            $table->foreign('party_id')->references('id')->on('parties')->onDelete('cascade');
        });
        Schema::create('news_organization', function (Blueprint $table) {
            $table->unsignedInteger('news_id')->index();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->unsignedInteger('organization_id')->index();
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_politician', function (Blueprint $table) {
            $table->dropForeign(['news_id']);
            $table->dropForeign(['politician_id']);
        });
        Schema::drop('news_politician');

        Schema::table('news_party', function (Blueprint $table) {
            $table->dropForeign(['news_id']);
            $table->dropForeign(['party_id']);
        });
        Schema::drop('news_party');

        Schema::table('news_organization', function (Blueprint $table) {
            $table->dropForeign(['news_id']);
            $table->dropForeign(['organization_id']);
        });
        Schema::drop('news_organization');
    }
}
