<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersHasProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->enum('role', ['visitor', 'editor', 'lead_editor', 'admin'])->default('visitor');

            $table->enum('sex', ['male', 'female'])->nullable()->default(null);
            $table->boolean('profile_confirmed')->default(false);
            $table->string('social_network_profile')->nullable()->default(null);
            $table->boolean('social_network_profile_confirmed')->default(false);
            $table->date('birthday')->nullable()->default(null);
            $table->enum('education', ['a','b','c','d','e'])->nullable()->default(null);
            $table->enum('income', ['low', 'middle', 'high'])->nullable()->default(null);
            $table->enum('family_status', ['widowed','single','married','civil marriage', 'divorced'])->nullable()->default(null);
            $table->enum('children',['no', 'one', 'two', 'many'])->nullable()->default(null);
            $table->enum('employment', ['student', 'unemployed', 'employed', 'retired'])->nullable()->default(null);
            $table->enum('election_activity', ['0%', '20%', '40%', '60%', '80%', '100%'])->nullable()->default(null);
            $table->enum('settlement_type', ['city', 'village'])->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $fields = [
                'role',
                'sex',
                'profile_confirmed',
                'social_network_profile',
                'social_network_profile_confirmed',
                'birthday',
                'education',
                'income',
                'family_status',
                'children',
                'employment',
                'election_activity',
                'settlement_type',
            ];

            $table->dropColumn($fields);
        });
    }
}
