<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubjectsHasIcons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        foreach(['parties', 'organizations'] as $table_name) {
            Schema::table($table_name, function (Blueprint $table) {
                $table->string('icon_url')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach(['parties', 'organizations'] as $table_name) {
            Schema::table($table_name, function (Blueprint $table) {
                $table->dropColumn('icon_url');
            });
        }
    }
}
