<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->date('creation_date');
            $table->text('ideology')->nullable();
            $table->text('program')->nullable();
            $table->text('history')->nullable();
            $table->string('official_site_url')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('logo_url')->nullable();
            $table->boolean('use_fake_rating')->nullable();
            $table->float('fake_rating')->nullable();
            $table->unsignedInteger('leader_id')->nullable();
            $table->foreign('leader_id')->references('id')->on('politicians')->onDelete('set null');
            //4. Лидер (ID политика)
            //5. Количество мест в парламенте
            //6. Массив этих депутатов (ID политика)
            //14. Массив обещаний - выполнений
            //15. Общее число обещаний (целое)
            //16. Число выполнений (целое)
            //17. Число “НЕ выполено” (целое)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parties', function (Blueprint $table) {
            $table->dropForeign(['leader_id']);
        });
        Schema::dropIfExists('parties');
    }
}
