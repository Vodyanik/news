<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('news_id');
            $table->unsignedInteger('user_id');
            $table->string('old_status')->nullable()->default(null);
            $table->string('new_status')->nullable()->default(null);
            $table->timestamps();


            $table->foreign('news_id')->references('id')->on('news');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actions_logs', function (Blueprint $table) {
            $table->dropForeign(['news_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('actions_logs');
    }
}
