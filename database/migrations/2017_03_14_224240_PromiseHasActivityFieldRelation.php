<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PromiseHasActivityFieldRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_field_promise', function (Blueprint $table) {
            $table->unsignedInteger('promise_id')->index();
            $table->foreign('promise_id')->references('id')->on('promises')->onDelete('cascade');
            $table->unsignedInteger('activity_field_id')->index();
            $table->foreign('activity_field_id')->references('id')->on('activity_fields')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_field_promise', function (Blueprint $table) {
            $table->dropForeign(['promise_id']);
            $table->dropForeign(['activity_field_id']);
        });
        Schema::dropIfExists('activity_field_promise');
    }
}
