<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GuestsCanLike extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('likes', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropIndex('likes_user_id_news_id_unique');
            DB::statement('ALTER TABLE `likes` CHANGE `user_id` `user_id` INT(10)  UNSIGNED  NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('likes', function (Blueprint $table) {
            DB::statement('ALTER TABLE `likes` CHANGE `user_id` `user_id` INT(10)  UNSIGNED NOT NULL');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unique(['user_id', 'news_id']);
        });
    }
}
