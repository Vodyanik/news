<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubjectsHasRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('politicians', function (Blueprint $table) {
            $table->float('rating')->default(0);
        });
        Schema::table('parties', function (Blueprint $table) {
            $table->float('rating')->default(0);
        });
        Schema::table('organizations', function (Blueprint $table) {
            $table->float('rating')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('politicians', function (Blueprint $table) {
            $table->dropColumn('rating');
        });
        Schema::table('parties', function (Blueprint $table) {
            $table->dropColumn('rating');
        });
        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn('rating');
        });
    }
}
