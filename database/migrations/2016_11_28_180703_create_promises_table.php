<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promises', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('description');
            $table->text('quote')->nullable();
            $table->string('proof_url')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->boolean('failed')->default(false);
        });


        Schema::create('promise_tag', function (Blueprint $table) {
            $table->unsignedInteger('promise_id')->index();
            $table->foreign('promise_id')->references('id')->on('promises')->onDelete('cascade');
            $table->unsignedInteger('tag_id')->index();
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promise_tag', function (Blueprint $table) {
            $table->dropForeign(['promise_id']);
            $table->dropForeign(['tag_id']);
        });
        Schema::dropIfExists('promise_tag');
        Schema::dropIfExists('promises');
    }
}
