<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsTableDropRationOptionsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('rating_option_has_origin');
            $table->dropColumn('rating_option_header_is_fine');
            $table->dropColumn('rating_option_photo_is_fine');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('news', function (Blueprint $table) {
            $table->boolean('rating_option_has_origin')->default(false);
            $table->boolean('rating_option_header_is_fine')->default(false);
            $table->boolean('rating_option_photo_is_fine')->default(false);
        });
    }
}
