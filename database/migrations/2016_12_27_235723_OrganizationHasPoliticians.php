<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrganizationHasPoliticians extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_politician', function (Blueprint $table) {
            $table->unsignedInteger('organization_id')->index();
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
            $table->unsignedInteger('politician_id')->index();
            $table->foreign('politician_id')->references('id')->on('politicians')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organization_politician', function (Blueprint $table) {
            $table->dropForeign(['politician_id']);
            $table->dropForeign(['organization_id']);
        });
        Schema::drop('organization_politician');
    }
}
