<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('title');
            $table->text('preview');
            $table->text('body');
            $table->boolean('rating_option_has_origin')->default(false);
            $table->boolean('rating_option_header_is_fine')->default(false);
            $table->boolean('rating_option_photo_is_fine')->default(false);

            $table->enum('status', ['new', 'edited', 'published'])->default('new');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
