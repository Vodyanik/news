<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('icon_url')->nullable()->default(null);
            $table->timestamps();
        });
        Schema::table('news', function (Blueprint $table) {
            $table->unsignedInteger('activity_field_id')->nullable()->default(null);
            $table->foreign('activity_field_id')->references('id')->on('activity_fields')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropForeign(['activity_field_id']);
            $table->dropColumn(['activity_field_id']);
        });
        Schema::dropIfExists('activity_fields');
    }
}
