<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubjectsHasPromises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('politician_promise', function (Blueprint $table) {
            $table->unsignedInteger('promise_id')->index();
            $table->foreign('promise_id')->references('id')->on('promises')->onDelete('cascade');
            $table->unsignedInteger('politician_id')->index();
            $table->foreign('politician_id')->references('id')->on('politicians')->onDelete('cascade');
        });
        Schema::create('party_promise', function (Blueprint $table) {
            $table->unsignedInteger('party_id')->index();
            $table->foreign('party_id')->references('id')->on('parties')->onDelete('cascade');
            $table->unsignedInteger('promise_id')->index();
            $table->foreign('promise_id')->references('id')->on('promises')->onDelete('cascade');
        });
        Schema::create('organization_promise', function (Blueprint $table) {
            $table->unsignedInteger('organization_id')->index();
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
            $table->unsignedInteger('promise_id')->index();
            $table->foreign('promise_id')->references('id')->on('promises')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('politician_promise', function (Blueprint $table) {
            $table->dropForeign(['promise_id']);
            $table->dropForeign(['politician_id']);
        });
        Schema::drop('politician_promise');

        Schema::table('party_promise', function (Blueprint $table) {
            $table->dropForeign(['promise_id']);
            $table->dropForeign(['party_id']);
        });
        Schema::drop('party_promise');

        Schema::table('organization_promise', function (Blueprint $table) {
            $table->dropForeign(['promise_id']);
            $table->dropForeign(['organization_id']);
        });
        Schema::drop('organization_promise');
    }
}
