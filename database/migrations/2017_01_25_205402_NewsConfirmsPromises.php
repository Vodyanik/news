<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsConfirmsPromises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_promise', function (Blueprint $table) {
            $table->unsignedInteger('news_id')->index();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->unsignedInteger('promise_id')->index();
            $table->foreign('promise_id')->references('id')->on('promises')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_promise', function (Blueprint $table) {
            $table->dropForeign(['news_id']);
            $table->dropForeign(['promise_id']);
        });
        Schema::drop('news_promise');
    }
}
