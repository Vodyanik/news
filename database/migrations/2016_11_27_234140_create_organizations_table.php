<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->string('creation_date')->nullable();
            $table->text('history')->nullable();
            $table->string('official_site_url')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('logo_url')->nullable();
            $table->boolean('use_fake_rating')->nullable();
            $table->float('fake_rating')->nullable();
            $table->unsignedInteger('leader_id')->nullable();

            $table->foreign('leader_id')->references('id')->on('politicians')->onCascade('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->dropForeign(['leader_id']);
        });
        Schema::dropIfExists('organizations');
    }
}
