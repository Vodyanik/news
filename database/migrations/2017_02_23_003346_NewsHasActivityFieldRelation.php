<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsHasActivityFieldRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_field_news', function (Blueprint $table) {
            $table->unsignedInteger('news_id')->index();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->unsignedInteger('activity_field_id')->index();
            $table->foreign('activity_field_id')->references('id')->on('activity_fields')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_field_news', function (Blueprint $table) {
            $table->dropForeign(['news_id']);
            $table->dropForeign(['activity_field_id']);
        });
        Schema::dropIfExists('activity_field_news');
    }
}
