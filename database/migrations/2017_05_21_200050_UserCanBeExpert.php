<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserCanBeExpert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
          ALTER TABLE `users` 
          CHANGE `role` `role` ENUM('visitor','expert','editor','lead_editor','admin')  
          NOT NULL  DEFAULT 'visitor'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("UPDATE `users` SET `role` = 'visitor' WHERE `role` = 'expert'");
        DB::statement("
          ALTER TABLE `users` 
          CHANGE `role` `role` ENUM('visitor','editor','lead_editor','admin')  
          NOT NULL  DEFAULT 'visitor'
        ");
    }
}
