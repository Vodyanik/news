<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $fillable = [
        'activity_field_id',
        'region_id',
        'text'
    ];

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function activity_field()
    {
        return $this->belongsTo('App\ActivityField');
    }
}
