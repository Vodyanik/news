<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promise extends Model
{
    protected $fillable = [
        'description',
        'quote',
        'proof_url',
        'confirmed',
        'failed',
        'comments'
    ];

    public function politicians()
    {
        return $this->belongsToMany('App\Politician');
    }

    public function parties()
    {
        return $this->belongsToMany('App\Party');
    }

    public function organizations()
    {
        return $this->belongsToMany('App\Organization');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function news()
    {
        return $this->belongsToMany('App\News');
    }

    public function activity_fields()
    {
        return $this->belongsToMany('App\ActivityField');
    }

    public function getTagIdsAttribute()
    {
        return $this->tags()->pluck('id')->toArray();
    }

    public static function getConfirmed()
    {
        return self::where('confirmed', '=', 1)->get();
    }

    public static function getFailed()
    {
        return self::where('failed', '=', 1)->get();
    }

    public static function getUnConfirmed()
    {
        return self::where('confirmed', '=', 0)->where('failed', '=', 0)->get();
    }
}
