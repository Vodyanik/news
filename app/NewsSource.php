<?php

namespace App;

use App\Traits\EntityRatingTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use File;
use Image;
use Illuminate\Database\Eloquent\Builder;

class NewsSource extends Model
{
    use EntityRatingTrait;

    protected $fillable = [
        'name',
        'site_url',
        'logo_url',
        'use_fake_rating',
        'fake_rating'
    ];

    public function news()
    {
        return $this->hasMany('App\News');
    }

    public static function scopeTop5(Builder $query)
    {
        return $query->take(5)->orderBy('rating', 'desc')->get();
    }

    public function getLogoFormNameAttribute()
    {
        return 'news_source_logo';
    }

    public function setLogo(Request $request)
    {
        if ($request->hasFile($this->logo_form_name)) {
            $image = $request->file($this->logo_form_name);
            $image_path =  implode('/', ['news_source_logos']);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . str_slug($this->name) . '.png';
            Image::make($image->getRealPath())->resize(null, 30, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path($filename), 100);
            $this->logo_url = $filename;
        }
    }

    public function updateRating()
    {
        $likes = $this->news()->withCount('likes')->get()->sum('likes_count');
        $dislikes = $this->news()->withCount('dislikes')->get()->sum('dislikes_count');
        if ($likes + $dislikes <= 0) $dislikes = 1;
        $rating = round(100*$likes/($likes + $dislikes));
        $this->rating = $rating;
        $this->save();
    }

}
