<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use File;
use Image;

class ActivityField extends Model
{

    protected $fillable = [
        'name',
        'icon_url'
    ];

    public function news()
    {
        return $this->belongsToMany('App\News');
    }

    public function promises()
    {
        return $this->belongsToMany('App\Promise');
    }

    public function getIconFormNameAttribute()
    {
        return 'activity_field_icon';
    }

    public function setIconImage(Request $request)
    {
        if ($request->hasFile($this->icon_form_name)) {
            $image = $request->file($this->icon_form_name);
            $image_path =  implode('/', ['activity_field_icons']);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . str_slug($this->name) . '.png';
            Image::make($image->getRealPath())->resize(30, 30, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path($filename), 100);
            $this->icon_url = $filename;
        }
    }

    public static function getSelectOptions()
    {
        return self::all()->sortBy('name')->pluck('name', 'id');
    }
}
