<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use File;
use Image;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'role',
        'sex',
        'profile_confirmed',
        'social_network_profile',
        'social_network_profile_confirmed',
        'birthday',
        'education',
        'income',
        'family_status',
        'children',
        'employment',
        'election_activity',
        'settlement_type',
        'region_id',
        'employment_spheres_id',
        'photo_url'
    ];

    protected $dates = ['birthday'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $income_mapping = [
        'low' => 'Низкий',
        'middle' => 'Средний',
        'high' => 'Высокий',
    ];

    public static $education_mapping = [
        'a' => '2 и > ВО',
        'b' => 'Высшее',
        'c' => 'Неоконченное высшее',
        'd' => 'Среднее специалльное',
        'e' => 'Среднее',
    ];

    public static $role_mapping = [
        'visitor' => 'Посетитель',
        'expert' => 'Эксперт',
        'editor' => 'Редактор',
        'lead_editor' => 'Главный редактор',
        'admin' => 'Админ',
    ];
    public static $sex_mapping = [
        'male' => 'Мужской',
        'female' => 'Женский',
    ];
    public static $family_status_mapping = [
        'widowed' => 'Вдовец/Вдова',
        'single' => 'Холост/Не замужем',
        'married' => 'Женат/Замужем',
        'civil marriage' => 'В гражданском браке',
        'divorced' => 'Разведен/Разведена',
    ];
    public static $children_mapping = [
        'no' => 'Нет',
        'one' => 'Один',
        'two' => 'Два',
        'many' => 'Многодетный',
    ];
    public static $employment_mapping = [
        'student' => 'Учащийся',
        'unemployed' => 'Нетрудоустроенный',
        'employed' => 'Трудоустроенный',
        'retired' => 'На пенсии',
    ];
    public static $election_activity_mapping = [
        '0%' => '0%',
        '20%' => '20%',
        '40%' => '40%',
        '60%' => '60%',
        '80%' => '80%',
        '100%' => '100%',
    ];
    public static $settlement_type_mapping = [
        'city' => 'Город',
        'village' => 'Село'
    ];

    public static $photo_form_name = 'user_photo';

    public function getIsAdminAttribute()
    {
        return $this->role == 'admin';
    }

    public function getIsBackendUserAttribute()
    {
        return $this->role !== 'visitor';
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function employment_sphere()
    {
        return $this->belongsTo('App\EmploymentSphere');
    }

    public function action_logs()
    {
        return $this->hasMany('ActionsLog');
    }

    public function reactions()
    {
        return $this->hasMany('App\Like');
    }

    public function likes()
    {
        return $this->reactions()->where('reaction', '=', 'like');
    }

    public function dislikes()
    {
        return $this->reactions()->where('reaction', '=', 'dislike');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\Bookmark');
    }

    public function getPhotoFormNameAttribute()
    {
        return self::$photo_form_name;
    }

    public function setPhoto(Request $request)
    {
        if ($request->hasFile($this->photo_form_name)) {
            $image = $request->file($this->photo_form_name);
            $image_path =  implode('/', ['user_photos']);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . str_slug($this->full_name) . '.png';
            Image::make($image->getRealPath())->save(public_path($filename), 100);
            $this->photo_url = $filename;
        }
    }

    public function expert_news_source()
    {
        if (empty($this->news_source_id)) {
            $this->createRelatedNewsSource();
        }
        return $this->belongsTo('App\NewsSource', 'news_source_id');
    }

    protected function createRelatedNewsSource()
    {
        $ns = new NewsSource();
        $ns->name = $this->name;
        $ns->site_url = route('expert_profile', $this->id);
        $ns->save();

        $this->news_source_id = $ns->id;
        $this->save();
    }
}
