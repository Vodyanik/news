<?php

namespace App;

use App\News;
use App\User;

use Illuminate\Database\Eloquent\Model;

class ActionsLog extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function news()
    {
        return $this->belongsTo('App\News');
    }

    public static function logAction(News $news, User $user)
    {
        if (empty($news->id)) return true;

        $entry = new self();
        $entry->news_id = $news->id;
        $entry->user_id = $user->id;
        $entry->old_status = $news->getOriginal('status');
        $entry->new_status = $news->status;
        return $entry->save();
    }
}
