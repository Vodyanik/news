<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];

    public function news()
    {
        return $this->belongsToMany('App\News');
    }

    public function getNewsCountAttribute()
    {
        return $this->news->count();
    }

    public static function getSelectValues()
    {
        return self::all()->pluck('name', 'id')->toArray();
    }
}
