<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function news()
    {
        return $this->belongsToMany('App\News');
    }

    public static function getSelectOptions()
    {
        return self::all()->sortBy('name')->pluck('name', 'id');
    }
}
