<?php

namespace App;

use App\Traits\EntityRatingTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use File;
use Image;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Party extends Model
{

    use EntityRatingTrait;

    protected $fillable = [
        'name',
        'creation_date',
        'ideology',
        'program',
        'history',
        'official_site_url',
        'facebook_url',
        'twitter_url',
        'logo_url',
        'use_fake_rating',
        'fake_rating',
        'leader_id',
        'icon_url',
        'parliament_seats'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'creation_date'
    ];

    public function news()
    {
        return $this->belongsToMany('App\News');
    }

    public function promises()
    {
        return $this->belongsToMany('App\Promise');
    }

    public function confirmed_promises()
    {
        return $this->belongsToMany('App\Promise')->where('confirmed', '=', true);
    }

    public function failed_promises()
    {
        return $this->belongsToMany('App\Promise')->where('failed', '=', true);
    }

    public function getPromiseIdAttribute()
    {
        return $this->promises->pluck('id')->toArray();
    }

    public function politicians()
    {
        return $this->hasMany('App\Politician');
    }

    public static function getList()
    {
        $result = [
            '0' => 'Беспартийный'
        ];
        foreach(self::all()->pluck('name', 'id')->toArray() as $k=>$v){
            $result[$k] = $v;
        };
        return $result;
    }

    public function leader()
    {
        return $this->hasOne('App\Politician', 'id', 'leader_id');
    }

    public function getConfirmedPromisesAttribute()
    {
        return $this->confirmed_promises()->get();
    }
    public function getFailedPromisesAttribute()
    {
        return $this->failed_promises()->get();
    }

    public function getIconFormNameAttribute()
    {
        return 'party_icon';
    }

    public function setIconImage(Request $request)
    {
        if ($request->hasFile($this->icon_form_name)) {
            $image = $request->file($this->icon_form_name);
            $image_path =  implode('/', ['party_icons']);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . str_slug($this->name) . '.png';
            Image::make($image->getRealPath())->resize(null, 30, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path($filename), 100);
            $this->icon_url = $filename;
        }
    }

    public function getLogoFormNameAttribute()
    {
        return 'party_logo';
    }

    public function setLogo(Request $request)
    {
        if ($request->hasFile($this->logo_form_name)) {
            $image = $request->file($this->logo_form_name);
            $image_path =  implode('/', ['party_logos']);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . str_slug($this->name) . '.png';
            Image::make($image->getRealPath())->save(public_path($filename), 100);
            $this->logo_url = $filename;
        }
    }

    public function setCreationDateAttribute($value)
    {
        $this->attributes['creation_date'] = Carbon::parse($value);
    }

    public function setFakeRatingAttribute($value){
        $this->attributes['fake_rating'] = empty(intval($value)) ? 0 : intval($value);
    }

    public static function scopeTop5(Builder $query)
    {
        return $query->take(5)->orderBy('rating', 'desc')->get();
    }
}
