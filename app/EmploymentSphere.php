<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentSphere extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
