<?php

namespace App\Traits;

use \Illuminate\Http\Request;
use Illuminate\Contracts\Queue\QueueableEntity;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;

/**
 * Class SearchableTrait
 * @package App\Traits
 */
trait SearchableTrait {

    protected $search_fields = [];

    /**
     * @param QueryBuilder| QueueableEntity $query
     * @param Request $request
     * @return mixed
     */
    public function build_search_query($query, Request $request)
    {
        if (empty($this->searchable_fields)) $this->searchable_fields = [];
        if (empty($this->pagination_params)) $this->pagination_params = [];

        $search_array = $request->get('search_fields', []);

        foreach ($this->searchable_fields as $field => $comparator) {
            if (!empty($search_array[$field])) {
                $value = $search_array[$field];
                $query = $query->where($field, $comparator, $comparator == 'like' ? "%{$value}%" : $value);
                $this->search_fields[$field] = $value;
                $this->pagination_params[$field] = $value;
            } else {
                $this->search_fields[$field] = null;
            }
        }
        return $query;
    }
}