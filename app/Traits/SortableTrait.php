<?php

namespace App\Traits;

use \Illuminate\Http\Request;
use Illuminate\Contracts\Queue\QueueableEntity;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;

/**
 * Class SortableTrait
 * @package App\Traits
 */
trait SortableTrait {

    protected $search_fields = [];

    /**
     * @param QueueableEntity|QueryBuilder $query
     * @param Request $request
     * @return mixed
     */
    public function build_sort_query($query, Request $request)
    {
        if (empty($this->pagination_params)) $this->pagination_params = [];

        if ($request->has('sort_by') && in_array( $request->get('sort_by'), $this->sortable_fields)){

            $sort_order = $request->has('sort_order') && $request->get('sort_order') == 'asc' ? 'asc' : 'desc';
            $this->pagination_params['sort_order'] = $request->has('sort_order') && $request->get('sort_order') == 'asc' ? 'desc' : 'asc';

            $query = $query->orderBy($request->get('sort_by'), $sort_order);
            $this->pagination_params['sort_by'] = $request->get('sort_by');
        }

        return $query;
    }
}