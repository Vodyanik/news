<?php

namespace App\Traits;

trait EntityRatingTrait {

    public function getRoundedRatingAttribute()
    {
        return number_format($this->rating * 4 / 100 + 1, 1, ',', '');
    }

    public function updateRating()
    {
        if ($this->use_fake_rating) {
            $rating = $this->fake_rating + (mt_rand(0, 1) == 1 ? -1 : 1)*mt_rand(0,1);
            if ($rating > 100) $rating = 100;
        } else {
            $likes = $this->news()->withCount('likes')->get()->sum('likes_count');
            $dislikes = $this->news()->withCount('dislikes')->get()->sum('dislikes_count');
            if ($likes + $dislikes <= 0) $dislikes = 1;
            $rating = round(100*$likes/($likes + $dislikes));
        }
        $this->rating = $rating;
        $this->save();
    }
}