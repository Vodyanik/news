<?php

namespace App;

use App\Traits\EntityRatingTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use File;
use Image;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Politician extends Model
{
    use EntityRatingTrait;

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'birthday',
        'ideology',
        'program',
        'biography',
        'official_site_url',
        'facebook_url',
        'twitter_url',
        'photo_url',
        'icon_url',
        'use_fake_rating',
        'fake_rating',
        'party_id',
        'organization_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'birthday'
    ];

    public function news()
    {
        return $this->belongsToMany('App\News');
    }

    public function promises()
    {
        return $this->belongsToMany('App\Promise');
    }

    public function confirmed_promises()
    {
        return $this->belongsToMany('App\Promise')->where('confirmed', '=', true);
    }

    public function failed_promises()
    {
        return $this->belongsToMany('App\Promise')->where('failed', '=', true);
    }

    public function getPromiseIdAttribute()
    {
        return $this->promises->pluck('id')->toArray();
    }

    public function getFullNameAttribute()
    {
        return "$this->last_name $this->first_name $this->middle_name";
    }

    public function getShortNameAttribute()
    {
        return implode(' ', [
            $this->last_name,
            mb_strtoupper(mb_substr($this->first_name, 0, 1)) . '.',
            !empty($this->middle_name) ? mb_strtoupper(mb_substr($this->middle_name, 0, 1)) . '.' : ''
        ]);
    }

    public function party()
    {
        return $this->belongsTo('App\Party');
    }

    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    public function setPartyIdAttribute($value)
    {
        $this->attributes['party_id'] = empty($value) ? null : $value;
    }

    public function getPartyNameAttribute()
    {
        return $this->party()->exists() ? $this->party->name : 'Беспартийный';
    }

    public function getConfirmedPromisesAttribute()
    {
        return $this->confirmed_promises()->get();
    }
    public function getFailedPromisesAttribute()
    {
        return $this->failed_promises()->get();
    }

    public function getPhotoFormNameAttribute()
    {
        return 'news_image';
    }

    public function setPhoto(Request $request)
    {
        if ($request->hasFile($this->photo_form_name)) {
            $image = $request->file($this->photo_form_name);
            $image_path =  implode('/', ['politician_photos']);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . str_slug($this->full_name) . '.png';
            Image::make($image->getRealPath())->save(public_path($filename), 100);
            $this->photo_url = $filename;
        }
    }

    public function getIconFormNameAttribute()
    {
        return 'politician_icon';
    }

    public function setIconImage(Request $request)
    {
        if ($request->hasFile($this->icon_form_name)) {
            $image = $request->file($this->icon_form_name);
            $image_path =  implode('/', ['politician_icons']);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . str_slug($this->full_name) . '.png';
            Image::make($image->getRealPath())->resize(null, 30, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path($filename), 100);
            $this->icon_url = $filename;
        }
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = Carbon::parse($value);
    }

    public function setFakeRatingAttribute($value){
        $this->attributes['fake_rating'] = empty(intval($value)) ? 0 : intval($value);
    }

    public static function scopeTop5(Builder $query)
    {
        return $query->take(5)->orderBy('rating', 'desc')->get();
    }

    public static function getSelectOptions()
    {
        return self::all()->sortBy('name')->pluck('full_name', 'id');
    }
}
