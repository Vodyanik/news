<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use File;
use Image;
use Illuminate\Support\Facades\Auth;
use Roumen\Feed\Feed;
use App\Traits\EntityRatingTrait;

class News extends Model
{

    use EntityRatingTrait;

    protected $dates = [
        'created_at',
        'updated_at',
        'publication_date'
    ];

    protected $fillable = [
        'title',
        'preview',
        'body',
        'news_source_id',
        'original_news_url',
        'status',
        'slug',
        'publication_date',
        'fact_checking_level',
        'show_on_slider'
    ];



    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function activity_fields()
    {
        return $this->belongsToMany('App\ActivityField');
    }

    public function news_source()
    {
        return $this->belongsTo('App\NewsSource');
    }

    public function parties()
    {
        return $this->belongsToMany('App\Party');
    }
    public function organizations()
    {
        return $this->belongsToMany('App\Organization');
    }
    public function politicians()
    {
        return $this->belongsToMany('App\Politician');
    }

    public function promises()
    {
        return $this->belongsToMany('App\Promise');
    }

    public function likes()
    {
        return $this->hasMany('App\Like')->where('reaction', '=', 'like');
    }
    public function dislikes()
    {
        return $this->hasMany('App\Like')->where('reaction', '=', 'dislike');
    }

    public function regions()
    {
        return $this->belongsToMany('App\Region');
    }

    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }
    public function getDislikesCountAttribute()
    {
        return $this->dislikes()->count();
    }

    public function user_like_reaction()
    {
        $result = false;
        $reaction = null;
        if (Auth::check()){
            $reaction = $this->hasMany('App\Like')->where('user_id', '=', Auth::id())->first();
        }
        if (!empty($reaction)) {
            $result = $reaction->reaction;
        }
        if (empty($result)) {
            if ($reaction = session()->get(Like::reaction_session_key($this->id))) {
                $result = $reaction;
            }
        }
        return $result;
    }

    public function is_bookmarked()
    {
        $user_id = !empty($user = \auth()->user()) ? $user->id : 0;
        return Bookmark::where('news_id', '=', $this->id)
                ->where('user_id', '=', $user_id)->exists();
    }

    public function action_logs()
    {
        return $this->hasMany('ActionsLog');
    }

    public function getImageFormNameAttribute()
    {
        return 'news_image';
    }

    public function setImage(Request $request)
    {
        if ($request->hasFile($this->image_form_name)) {
            $image = $request->file($this->image_form_name);
            if (empty($this->created_at)){
                $this->created_at = Carbon::now();
            }
            $image_path =  implode('/', ['news_images', $this->created_at->year, $this->created_at->month]);
            if (!File::exists($image_path)) {
                File::makeDirectory($image_path, 0775, true);
            }
            $filename  = '/' . $image_path . '/' . $this->slug . '.jpg';
            Image::make($image->getRealPath())->resize(1200, 700, function($constraint){
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save(public_path($filename), 80);
            $this->image = $filename;
            $filename_tn  = '/' . $image_path . '/tn_' . $this->slug . '.jpg';
            Image::make($image->getRealPath())->resize(740, 555, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path($filename_tn), 80);
            $this->tn_image = $filename_tn;
        }
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', 'published')->
            whereDate('publication_date', '<=', Carbon::now())
            ->orderBy('publication_date', 'desc')->orderBy('id', 'desc');
    }

    public function scopeOpinions(Builder $query)
    {
        return $this->scopePublished($query)->where('type', 'opinion');
    }

    public function scopeMyOpinions(Builder $query)
    {
        return $this->scopeOpinions($query)->
            orderBy('id', 'desc')->
            where('expert_id', '=' , auth()->user()->id);
    }

    public function scopeForSlider(Builder $query)
    {
        return $this->scopePublished($query)->where('show_on_slider', '=', true);
    }

    public static function getRSSFeed()
    {
        $feed = new Feed();
        // cache the feed for 60 minutes with custom cache key "feedNewsKey"
        $feed ->setCache(0, 'global_rss_feed');

        // check if there is cached feed and build new only if is not
        if (!$feed ->isCached())
        {
            // creating rss feed with our most recent 20 records in news table
            $news = self::published()->take(20)->get();

            // set your feed's title, description, link, pubdate and language
            $feed ->title = 'Свежие политические новости';
            $feed ->description = '';
            $feed ->logo = url('/images/logo.png');
            $feed ->link = url('news_feed');
            $feed ->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'
            $feed ->pubdate = $news[0]->created_at; // date of latest news
            $feed ->lang = 'ru';
            $feed ->setShortening(true); // true or false
            $feed ->setTextLimit(200); // maximum length of description text

            foreach ($news as $n)
            {
                // set item's title, author, url, pubdate, description and content
                $feed->add(
                    $n->title,
                    $n->original_news_url,
                    route('news', $n->slug),
                    $n->created_at,
                    $n->preview,
                    $n->body,
                    [
                        'url' => url($n->tn_image ?: '/images/logo.png'),
                    ]
                );
            }
        }

        // return your feed ('atom' or 'rss' format)
        return $feed ->render('rss');
    }

    public function updateRatings()
    {
        $news_rating = $this->likes_count / ($this->likes_count + $this->dislikes_count);
        $news_rating = round($news_rating * 100);
        $this->rating = $news_rating;
        $this->save();

        $this->news_source->updateRating();
        foreach ($this->parties as $party) {
            $party->updateRating();
        }
        foreach ($this->politicians as $politician) {
            $politician->updateRating();
        }
        foreach ($this->organizations as $organization) {
            $organization->updateRating();
        }
    }

}
