<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function news()
    {
        return $this->belongsTo('App\News');
    }

    public function scopeLatest(Builder $query)
    {
        return $query->with('news')->orderBy('created_at', 'desc')->take(12);
    }

    public static function get_latest()
    {
        return self::latest()->get();
    }
}
