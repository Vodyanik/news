<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use League\Flysystem\Exception;

class Like extends Model
{
    protected $fillable = [
        'news_id',
        'user_id',
        'reaction'
    ];

    public function news()
    {
        return $this->belongsTo('App\News');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public static function reaction_session_key($news_id)
    {
        return implode('.', ['news', $news_id, 'reaction']);
    }
}
