<?php

namespace App\Http\Middleware;

use Closure;

class BackendUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if (!$user) {
            return response('Forbidden area', 401);
        }
        if (!in_array($user->role, ['editor','lead_editor','admin'])) {
            return response('Forbidden area', 401);
        }
        return $next($request);
    }
}
