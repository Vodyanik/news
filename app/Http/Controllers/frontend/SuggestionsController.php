<?php

namespace App\Http\Controllers\frontend;

use App\ActivityField;
use App\Region;
use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestionsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $suggestion = new Suggestion();
        if (empty($request->get('text'))) {
            return redirect(back())->withErrors('Текст предложения не может быть пустым');
        }
        $suggestion->text = $request->get('text');
        if ($region = Region::find($request->get('region_id'))){
            $suggestion->region_id = $region->id;
        }
        if ($activity_field = ActivityField::find($request->get('activity_field_id'))){
            $suggestion->activity_field_id = $activity_field->id;
        }
        $suggestion->save();

        return view('frontend.suggestions.thanks');
    }

}
