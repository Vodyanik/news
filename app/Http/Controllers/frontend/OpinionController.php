<?php

namespace App\Http\Controllers\frontend;

use App\Http\Requests\OpinionRequest;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OpinionController extends Controller
{
    /**
     *
     * @param OpinionRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OpinionRequest $request)
    {

        $news = new News();
        $data = $request->all();
        $news->fill($data);

        $news->preview = '';
        $news->slug = str_slug($data['title']);
        $news->expert_id = auth()->user()->id;
        $news->type = 'opinion';
        $news->news_source_id = auth()->user()->expert_news_source->id;

        $news->save();

        return redirect(route('my_account'))->with('message', 'Мнение будет опубликовано после модерации');
    }

}
