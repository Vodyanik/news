<?php

namespace App\Http\Controllers\frontend;

use App\NewsSource;
use App\Party;
use App\Politician;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReitingController extends Controller
{

    public function index()
    {
        $parties = Party::orderBy('rating', 'desc')->get();
        $partiesCount = $parties->count();
        if ($partiesCount % 3 == 0) {
            $parties = $parties->chunk($partiesCount / 3);
        } else {
            $parties = $parties->chunk($partiesCount / 3 + 1);
        }

        $politicians = Politician::orderBy('rating', 'desc')->get();
        $politiciansCount = $politicians->count();
        if ($politiciansCount % 3 == 0) {
            $politicians = $politicians->chunk($politiciansCount / 3);
        } else {
            $politicians = $politicians->chunk($politiciansCount / 3 + 1);
        }

        $newsSources = NewsSource::orderBy('rating', 'desc')->get();
        $newsSourcesCount = $newsSources->count();
        if ($newsSourcesCount % 3 == 0) {
            $newsSources = $newsSources->chunk($newsSourcesCount / 3);
        } else {
            $newsSources = $newsSources->chunk($newsSourcesCount / 3 + 1);
        }

        return view('frontend.reitings', compact('parties', 'politicians', 'newsSources'));
    }

}
