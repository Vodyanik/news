<?php

namespace App\Http\Controllers\frontend;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Like;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{

    private function add_reaction(Request $request, $news_id, $reaction)
    {
        $user_id = Auth::id();
        $likes_cnt = 0;
        $dislikes_cnt = 0;

        $news = News::find($news_id);
        if ( empty($news) || $news->status !== 'published') {
            return [];
        }

        if ($sess_reaction = $request->session()->get(Like::reaction_session_key($news_id), false)) {
            if ($sess_reaction == $reaction){
                return [];
            } else {
                if (empty($user_id)){
                    $like = Like::where('user_id', '=', null)->where('news_id', '=', $news_id)->
                          where('reaction', '=', $reaction == 'like' ? 'dislike' : 'like')->first();
                    if ($like) $like->delete();
                }
                $request->session()->forget(Like::reaction_session_key($news_id));
            }
        }

        $like = null;
        if (Auth::check()) $like = Like::where('user_id', '=', $user_id)->where('news_id', '=', $news_id)->first();
        if (empty($like)) $like = new Like();
        $like->fill([
            'news_id' => $news_id,
            'user_id' => $user_id,
            'reaction' => $reaction
        ]);

        $result = $like->save();
        $news = News::find($news_id);
        $news->updateRatings();
        $likes_cnt = $news->likes_count;
        $dislikes_cnt = $news->dislikes_count;

        $request->session()->put(Like::reaction_session_key($news_id), $reaction);


        return [
            'my_reaction' => $reaction,
            'result' => $result,
            'likes' => $likes_cnt,
            'dislikes' => $dislikes_cnt
        ];

    }

    public function like(Request $request, $news_id)
    {
        return $this->add_reaction($request, $news_id, 'like');
    }

    public function dislike(Request $request, $news_id)
    {
        return $this->add_reaction($request, $news_id, 'dislike');
    }
}
