<?php

namespace App\Http\Controllers\frontend;

use App\Comment;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;

class CommentController extends Controller
{
    public function store(Request $request, $id)
    {
        $comment = new Comment;
        $comment->user_id = $request->user()->id;
        $comment->news_id = $id;
        $comment->text = $request->text;
        $comment->save();

        return redirect()->intended(session('intended'));
    }

    public function ajaxLoadComments($news_id, $page_id)
    {
        $page = $page_id;
        $page = $page + 1;

        $data = Comment::with('user')->where('news_id', $news_id)->orderBy('updated_at', 'desc')->paginate(5, ['*'], 'page', $page);

        return $data;
    }
}
