<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $fields = [
            'role',
            'sex',
            'profile_confirmed',
            'social_network_profile',
            'social_network_profile_confirmed',
            'birthday',
            'education',
            'income',
            'family_status',
            'children',
            'employment',
            'election_activity',
            'settlement_type',
            'region_id',
            'employment_spheres_id'
        ];

        foreach ($fields as $field) {
            if (empty($data[$field])) $data[$field] = null;
        }


        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'sex' => $data['sex'],
            'social_network_profile' => $data['social_network_profile'],
            'birthday' => !empty($data['birthday']) ? Carbon::parse($data['birthday']) : null,
            'education' => $data['education'],
            'income' => $data['income'],
            'family_status' => $data['family_status'],
            'children' => $data['children'],
            'employment' => $data['employment'],
            'election_activity' => $data['election_activity'],
            'settlement_type' => $data['settlement_type'],
            'region_id' => $data['region_id'],
            'employment_spheres_id' => $data['employment_spheres_id'],
        ]);
    }
}
