<?php

namespace App\Http\Controllers\backend;

use App\ActionsLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\User;
use App\Traits\SortableTrait;
use App\Traits\SearchableTrait;

class ActionsLogController extends Controller
{

    use SortableTrait, SearchableTrait;

    protected $pagination_params = [];

    protected $sortable_fields = [
        'news.id',
        'news.title',
        'users.name',
        'old_status',
        'new_status',
        'actions_log.created_at',
    ];

    protected $searchable_fields = [
        'news.id' => '=',
        'news.title' => 'like',
        'users.name' => 'like',
        'old_status' => 'like',
        'new_status' => 'like',
        'actions_log.created_at' => 'like',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new ActionsLog();
        $query = $query->join('users', 'user_id', '=', 'users.id');
        $query = $query->join('news', 'news_id', '=', 'news.id');
        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);
        $log_entries = $query->paginate(50);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        return view('backend.news_actions_log.index', compact('log_entries', 'pagination_params', 'search_fields'));
    }

}
