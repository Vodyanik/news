<?php

namespace App\Http\Controllers\backend;

use App\ActionsLog;
use App\ActivityField;
use App\Organization;
use App\Party;
use App\Politician;
use App\Promise;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\SortableTrait;
use App\Traits\SearchableTrait;

use App\News;
use App\NewsSource;
use App\Tag;

class NewsController extends Controller
{
    use SortableTrait, SearchableTrait;

    protected $pagination_params = [];

    protected $sortable_fields = [
        'id',
        'title',
        'status',
        'preview',
        'rating',
        'created_at',
        'updated_at'
    ];

    protected $searchable_fields = [
        'id' => '=',
        'title' => 'like',
        'preview' => 'like',
        'status' => '=',
        'min_rating' => '<=',
        'max_rating' => '>='
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new News();

        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);

        $news = $query->orderBy('created_at', 'desc')->paginate(10);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        return view('backend.news.index', compact('news', 'pagination_params', 'search_fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news = new News;
        $news->publication_date = Carbon::now();
        $tags = Tag::all();
        $news_sources = NewsSource::all();
        $organizations = Organization::all();
        $parties = Party::all();
        $politicians = Politician::all();
        $activity_fields = ActivityField::all();
        return view('backend.news.edit', compact('news', 'tags', 'news_sources', 'organizations', 'parties', 'politicians', 'activity_fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new News;
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);
        if (empty($data['confirmed_promises'])) $data['confirmed_promises'] = [];
        if (empty($data['failed_promises'])) $data['failed_promises'] = [];
        if (empty($data['show_on_slider'])) $data['show_on_slider'] = false;

        $news->fill($data);
        ActionsLog::logAction($news, $request->user());
        $news->setImage($request);

        $news->save();

        foreach(['tags','organizations','parties','politicians', 'activity_fields', 'regions'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $news->activity_fields()->sync($data['activity_fields']);
        $news->tags()->sync($data['tags']);
        $news->organizations()->sync($data['organizations']);
        $news->parties()->sync($data['parties']);
        $news->politicians()->sync($data['politicians']);
        $news->regions()->sync($data['regions']);
        $news->promises()->sync(array_merge($data['confirmed_promises'], $data['failed_promises']));

        foreach ($news->promises as $promise) {
            if ($promise->news->count() == 1) {
                if ($promise->confirmed == true && !in_array($promise->id, $data['confirmed_promises'])) {
                    $promise->confirmed = false;
                    $promise->save();
                }
                if ($promise->failed == true && !in_array($promise->id, $data['failed_promises'])) {
                    $promise->failed = false;
                    $promise->save();
                }
            }
        }

        $confirmed_promises = Promise::findMany($data['confirmed_promises']);
        foreach ($confirmed_promises as $promise){
            $promise->confirmed = true;
            $promise->save();
        }

        $failed_promises = Promise::findMany($data['failed_promises']);
        foreach ($failed_promises as $promise){
            $promise->failed = true;
            $promise->save();
        }

        return redirect(route('news.index'))->with(['success' => 'Новость создана']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        $tags = Tag::all();
        $news_sources = NewsSource::all();
        $organizations = Organization::all();
        $parties = Party::all();
        $politicians = Politician::all();
        $activity_fields = ActivityField::all();
        return view('backend.news.edit', compact('news', 'tags', 'news_sources', 'organizations', 'parties', 'politicians', 'activity_fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::findOrFail($id);
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);
        if (empty($data['confirmed_promises'])) $data['confirmed_promises'] = [];
        if (empty($data['failed_promises'])) $data['failed_promises'] = [];
        if (empty($data['show_on_slider'])) $data['show_on_slider'] = false;


        $news->fill($data);
        ActionsLog::logAction($news, $request->user());
        $news->setImage($request);

        $news->save();
        foreach(['tags','organizations','parties','politicians', 'activity_fields', 'regions'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $news->activity_fields()->sync($data['activity_fields']);
        $news->tags()->sync($data['tags']);
        $news->organizations()->sync($data['organizations']);
        $news->parties()->sync($data['parties']);
        $news->politicians()->sync($data['politicians']);
        $news->regions()->sync($data['regions']);
        $news->promises()->sync(array_merge($data['confirmed_promises'], $data['failed_promises']));

        foreach ($news->promises as $promise) {
            if ($promise->news->count() == 1) {
                if ($promise->confirmed == true && !in_array($promise->id, $data['confirmed_promises'])) {
                    $promise->confirmed = false;
                    $promise->save();
                }
                if ($promise->failed == true && !in_array($promise->id, $data['failed_promises'])) {
                    $promise->failed = false;
                    $promise->save();
                }
            }
        }

        $confirmed_promises = Promise::findMany($data['confirmed_promises']);
        foreach ($confirmed_promises as $promise){
            $promise->confirmed = true;
            $promise->save();
        }

        $failed_promises = Promise::findMany($data['failed_promises']);
        foreach ($failed_promises as $promise){
            $promise->failed = true;
            $promise->save();
        }

        return redirect(route('news.index'))->with(['notification' => 'Новость обновлена']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->tags()->detach();
        $news->organizations()->detach();
        $news->parties()->detach();
        $news->politicians()->detach();
        $news->delete();
        return redirect(route('news.index'))->with(['notification' => 'Новость удалена']);
    }

    public function getRSSFeed()
    {
        return News::getRSSFeed();
    }
}
