<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Traits\SearchableTrait;
use App\Traits\SortableTrait;

class TagsController extends Controller
{

    use SortableTrait, SearchableTrait;

    protected $pagination_params = [];

    protected $sortable_fields = [
        'id',
        'name',
        'news_count',
        'rating'
    ];

    protected $searchable_fields = [
        'id' => '=',
        'name' => 'like',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Tag::withCount('news');

        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);

        $tags = $query->paginate(20);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        return view('backend.tags.index', compact('tags', 'pagination_params', 'search_fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tag = new Tag;
        return view('backend.tags.edit', compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = new Tag;
        $data = $request->all();
        $tag->fill($data)->save();
        return redirect(route('tags.index'))->with(['success' => 'Тег создан']);
    }

    public function storeAjax(Request $request)
    {
        return Tag::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view('backend.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);
        $data = $request->all();
        $tag->fill($data)->save();
        return redirect(route('tags.index'))->with(['notification' => 'Тег обновлен']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->news()->detach();
        $tag->delete();
        return redirect(route('tags.index'))->with(['notification' => 'Тег удален']);
    }
}
