<?php

namespace App\Http\Controllers\backend;

use App\Organization;
use App\Promise;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Politician;
use App\Traits\SortableTrait;
use App\Traits\SearchableTrait;

class OrganizationController extends Controller
{

    use SortableTrait, SearchableTrait;

    protected $pagination_params = [];

    protected $sortable_fields = [
        'id',
        'name',
        'creation_date',
        'official_site_url',
        'promises_count',
        'confirmed_promises_count',
        'failed_promises_count',
    ];

    protected $searchable_fields = [
        'id' => '=',
        'name' => 'like',
        'creation_date' => 'like'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = Organization::withCount([
            'promises',
            'confirmed_promises',
            'failed_promises'
        ]);

        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);

        $organizations = $query->paginate(10);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        return view('backend.organizations.index', compact('organizations', 'pagination_params', 'search_fields'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organization = new Organization();
        $organization->creation_date = Carbon::now();
        $promises = Promise::all()->pluck('description', 'id');
        $politicians = Politician::all()->pluck('full_name', 'id')->toArray();
        $politicians = array_merge([' - '], $politicians);
        return view('backend.organizations.edit', compact('organization', 'promises', 'politicians'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $organization = new Organization();
        if (empty($data['leader_id'])) $data['leader_id'] = null;
        $data = $request->all();
        $organization->fill($data);
        $organization->setLogo($request);
        $organization->setIconImage($request);
        $organization->save();

        foreach(['promises', 'politicians'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $organization->promises()->sync($data['promises']);
        $organization->politicians()->sync($data['politicians']);

        return redirect(route('organization.index'))->with(['success' => 'Организация создана']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organization = Organization::findOrFail($id);
        $promises = Promise::all()->pluck('description', 'id');
        $politicians = Politician::all()->pluck('full_name', 'id')->toArray();
        $politicians = array_merge([' - '], $politicians);
        return view('backend.organizations.edit', compact('organization', 'promises', 'politicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var Organization $organization */
        $organization = Organization::findOrFail($id);
        $data = $request->all();
        if (empty($data['leader_id'])) $data['leader_id'] = null;
        $organization->fill($data);
        $organization->setLogo($request);
        $organization->setIconImage($request);
        $organization->save();

        $organization->updateRating();

        foreach(['promises', 'politicians'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $organization->promises()->sync($data['promises']);
        $organization->politicians()->sync($data['politicians']);

        return redirect(route('organization.index'))->with(['success' => 'Организация обновлена']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organization = Organization::findOrFail($id);
        $organization->news()->detach();
        $organization->promises()->detach();
        $organization->delete();
        return redirect(route('organization.index'))->with(['success' => 'Организация удалена']);
    }
}
