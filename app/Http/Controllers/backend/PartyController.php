<?php

namespace App\Http\Controllers\backend;

use App\Party;
use App\Politician;
use App\Promise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\SortableTrait;
use App\Traits\SearchableTrait;

class PartyController extends Controller
{
    use SortableTrait, SearchableTrait;

    protected $pagination_params = [];

    protected $sortable_fields = [
        'id',
        'name',
        'creation_date',
        'official_site_url',
        'promises_count',
        'confirmed_promises_count',
        'failed_promises_count'
    ];

    protected $searchable_fields = [
        'id' => '=',
        'name' => 'like',
        'creation_date' => 'like'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Party::withCount([
            'promises',
            'confirmed_promises',
            'failed_promises'
        ]);

        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);

        $parties = $query->paginate(10);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        return view('backend.parties.index', compact('parties', 'pagination_params', 'search_fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $party = new Party();
        $promises = Promise::all()->pluck('description', 'id');
        $politicians = Politician::all()->pluck('full_name', 'id')->toArray();
        $politicians = array_merge([' - '], $politicians);
        return view('backend.parties.edit', compact('party', 'promises', 'politicians'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $party = new Party();
        $data = $request->all();
        if (empty($data['leader_id'])) $data['leader_id'] = null;
        $party->fill($data);
        $party->setIconImage($request);
        $party->setLogo($request);
        $party->save();

        foreach(['promises'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $party->promises()->sync($data['promises']);

        return redirect(route('party.index'))->with(['success' => 'Партия создана']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $party = Party::findOrFail($id);
        $promises = Promise::all()->pluck('description', 'id');
        $politicians = Politician::all()->pluck('full_name', 'id')->toArray();
        $politicians = array_merge([' - '], $politicians);
        return view('backend.parties.edit', compact('party', 'promises', 'politicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var Party $party */
        $party = Party::findOrFail($id);
        $data = $request->all();
        if (empty($data['leader_id'])) $data['leader_id'] = null;
        $party->fill($data);
        $party->setLogo($request);
        $party->setIconImage($request);
        $party->save();

        $party->updateRating();

        foreach(['promises'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $party->promises()->sync($data['promises']);

        return redirect(route('party.index'))->with(['success' => 'Партия обновлена']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $party = Party::findOrFail($id);
        $party->news()->detach();
        $party->promises()->detach();
        $party->delete();
        return redirect(route('party.index'))->with(['success' => 'Партия удалена']);
    }
}
