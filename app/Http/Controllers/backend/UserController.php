<?php

namespace App\Http\Controllers\backend;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Traits\SearchableTrait;
use App\Traits\SortableTrait;

class UserController extends Controller
{
    use SortableTrait, SearchableTrait;

    protected $sortable_fields = [
        'id',
        'name',
        'email',
        'birthday',
        'role',
    ];

    protected $searchable_fields = [
        'id' => '=',
        'name' => 'like',
        'email' => 'like',
        'birthday' => 'like',
        'role' => '='
    ];

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new User();
        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);

        $users = $query->paginate(10);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        return view('backend.users.index', compact('users', 'search_fields', 'pagination_params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        return view('backend.users.edit', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = new User();
        if (!empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }
        if (!empty($data['birthday'])) $data['birthday'] = Carbon::parse($data['birthday']);
        $data = $request->all();
        $user->fill($data)->save();

        return redirect(route('user.index'))->with(['success' => 'Пользователь создан']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('backend.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->all();
        if (!empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }
        if (!empty($data['birthday'])){
            $data['birthday'] = Carbon::parse($data['birthday']);
        } else {
            if (isset($data['birthday'])) unset($data['birthday']);
        }
        $user->fill($data)->save();

        return redirect(route('users.index'))->with(['success' => 'Пользователь обновлен']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();
        return redirect(route('users.index'))->with(['success' => 'Пользователь удален']);
    }
}
