<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\SortableTrait;
use App\Traits\SearchableTrait;
use App\Comment;

class CommentController extends Controller
{
    use SortableTrait, SearchableTrait;

    protected $pagination_params = [];

    protected $sortable_fields = [
        'id',
        'news_id',
        'text',
        'status',
        'created_at',
        'updated_at'
    ];

    protected $searchable_fields = [
        'id' => '=',
        'news_id' => '=',
        'user_id' => '=',
        'status' => '=',
    ];

    public function index(Request $request)
    {
        $query = new Comment();

        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);

        $comments = $query->with(['news', 'user'])->orderBy('created_at', 'desc')->paginate(10);

        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;

        return view('backend.comments.index', compact('comments', 'pagination_params', 'search_fields'));
    }

    public function edit($id)
    {
        $comment = Comment::find($id);

        return view('backend.comments.edit', compact('comment'));
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        $comment->text = $request->text;
        if ($request->status == 'on'){
            $comment->status = 1;
        } else {
            $comment->status = 0;
        }
        $comment->update();

        return redirect()->back();
    }

    public function destroy($id)
    {
        Comment::find($id)->delete();

        return redirect()->back();
    }
}
