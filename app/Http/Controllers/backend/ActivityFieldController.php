<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\ActivityField;

class ActivityFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.activity_fields.index',['activity_fields' => ActivityField::paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activity_field = new ActivityField();
        return view('backend.activity_fields.edit', compact('activity_field'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $af = new ActivityField();
        $this->validate($request, [
            'name' => 'required|unique:activity_fields|max:255'
        ]);
        $data = $request->all();
        $af->fill($data);
        $af->setIconImage($request);
        $af->save();

        return redirect(route('activity_fields.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity_field = ActivityField::findOrFail($id);
        return view('backend.activity_fields.edit', compact('activity_field'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $af = ActivityField::findOrFail($id);
        $this->validate($request, [
            'name' => ['required', Rule::unique('activity_fields')->ignore($af->id)]
        ]);
        $data = $request->all();
        $af->fill($data);
        $af->setIconImage($request);
        $af->save();

        return redirect(route('activity_fields.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $af = ActivityField::findOrFail($id);
        $af->news()->detach();
        $af->promises()->detach();
        $af->delete();
        return redirect(route('activity_fields.index'))->with(['notification' => 'Сфера удалена']);
    }
}
