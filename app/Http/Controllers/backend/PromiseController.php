<?php

namespace App\Http\Controllers\backend;

use App\Promise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promises = Promise::paginate(10);
        return view('backend.promises.index', compact('promises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promise = new Promise();
        return view('backend.promises.edit', compact('promise'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $promise = new Promise();
        $data = $request->all();
        $data['confirmed'] = !empty($request->get('promise_confirmed'));
        $data['failed'] = !empty($request->get('promise_failed'));
        $promise->fill($data)->save();
        if (!isset($data['tags'])) $data['tags'] = [];
        $promise->tags()->sync($data['tags']);
        $promise->activity_fields()->sync($data['activity_fields']);
        return redirect(route('promise.index'))->with(['success' => 'Обещание создано']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promise = Promise::findOrFail($id);
        return view('backend.promises.edit', compact('promise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promise = Promise::findOrFail($id);
        $data = $request->all();
        $data['confirmed'] = !empty($request->get('promise_confirmed'));
        $data['failed'] = !empty($request->get('promise_failed'));
        $promise->fill($data)->save();
        if (!isset($data['tags'])) $data['tags'] = [];
        $promise->tags()->sync($data['tags']);
        $promise->activity_fields()->sync($data['activity_fields']);
        return redirect(route('promise.index'))->with(['success' => 'Обещание обновлено']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promise = Promise::findOrFail($id);
        $promise->organizations()->detach();
        $promise->parties()->detach();
        $promise->politicians()->detach();
        $promise->delete();
        return redirect(route('promise.index'))->with(['success' => 'Обещание удалено']);
    }

    public function getAjaxCreateForm()
    {
        return view('backend.promises.edit_ajax');
    }

    public function storeAjax(Request $request)
    {
        $promise = new Promise();
        $data = $request->all();
        $promise->fill($data)->save();
        $promise->activity_fields()->sync($data['activity_fields']);
        return $promise;
    }
}
