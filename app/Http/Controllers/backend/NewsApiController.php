<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Party;
use App\Politician;
use App\Organization;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;

class NewsApiController extends Controller
{
    public function getSubjects($news_id)
    {
        $news = News::findOrFail($news_id);
        $parties = $news->parties->pluck('id');
        $politicians = $news->politicians->pluck('id');
        $organizations = $news->organizations->pluck('id');
        return compact('parties','politicians','organizations');
    }

    public function getPromises(Request $request)
    {

        $response = [
            'parties' => [],
            'politicians' => [],
            'organizations' => [],
            'news_promises' => []
        ];

        if ($request->has('parties')){
            $parties = Party::findMany($request->get('parties'));
            foreach ($parties as $party) {
                foreach ($party->promises as $promise) {
                    $response['parties'][$party->name][$promise->id] = [
                        'description' => $promise->description,
                        'confirmed' => $promise->confirmed,
                        'failed' => $promise->failed
                    ];
                }
            }
        };

        if ($request->has('politicians')){
            $politicians = Politician::findMany($request->get('politicians'));
            foreach ($politicians as $politician) {
                foreach ($politician->promises as $promise) {
                    $response['politicians'][$politician->full_name][$promise->id] = [
                        'description' => $promise->description,
                        'confirmed' => $promise->confirmed,
                        'failed' => $promise->failed
                    ];
                }
            }
        };

        if ($request->has('organizations')){
            $organizations = Organization::findMany($request->get('politicians'));
            foreach ($organizations as $organization) {
                foreach ($organization->promises as $promise) {
                    $response['organizations'][$organization->name][$promise->id] = [
                        'description' => $promise->description,
                        'confirmed' => $promise->confirmed,
                        'failed' => $promise->failed
                    ];
                }
            }
        };

        if ($request->has('news_id') && !empty($news_id = $request['news_id'])) {
            $news = News::findOrFail($news_id);
            $response['news_promises'] = $news->promises->pluck('id');
        }

        return $response;
    }
}
