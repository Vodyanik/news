<?php

namespace App\Http\Controllers\backend;

use App\Politician;
use App\Party;
use App\Promise;
use App\Traits\SearchableTrait;
use App\Traits\SortableTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PoliticianController extends Controller
{

    use SortableTrait, SearchableTrait;

    protected $sortable_fields = [
        'id',
        'last_name',
        'birthday',
        'party_id',
        'official_site_url',
        'promises_count',
        'confirmed_promises_count',
        'failed_promises_count'
    ];

    protected $searchable_fields = [
        'id' => '=',
        'last_name' => 'like',
        'birthday' => 'like',
        'party_id' => '=',
        'official_site_url' => 'like'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Politician::withCount([
            'promises',
            'confirmed_promises',
            'failed_promises'
        ]);;
        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);
        $politicians = $query->paginate(10);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        $parties = array_merge([''], Party::all()->pluck('name', 'id')->toArray());
        return view('backend.politicians.index', compact('politicians', 'parties', 'search_fields', 'pagination_params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $politician = new Politician();
//        $parties = Party::all()->pluck('name', 'id');
        $parties = Party::getList();
        $promises = Promise::all()->pluck('description', 'id');
        return view('backend.politicians.edit', compact('politician', 'promises', 'parties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $politician = new Politician();
        $data = $request->all();
        $politician->fill($data);
        $politician->setPhoto($request);
        $politician->setIconImage($request);
        $politician->save();

        foreach(['promises'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $politician->promises()->sync($data['promises']);

        return redirect(route('politician.index'))->with(['success' => 'Политик создан']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $politician = Politician::findOrFail($id);
        $parties = Party::getList();
        $promises = Promise::all()->pluck('description', 'id');
        return view('backend.politicians.edit', compact('politician', 'promises', 'parties'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var Politician $politician */
        $politician = Politician::findOrFail($id);
        $data = $request->all();
        $politician->fill($data);
        $politician->setPhoto($request);
        $politician->setIconImage($request);
        $politician->save();
        $politician->updateRating();

        foreach(['promises'] as $field) {
            if (!isset($data[$field])) $data[$field] = [];
        }
        $politician->promises()->sync($data['promises']);

        return redirect(route('politician.index'))->with(['success' => 'Политик обновлен']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $politician = Politician::findOrFail($id);
        $politician->news()->detach();
        $politician->promises()->detach();
        $politician->delete();
        return redirect(route('politician.index'))->with(['success' => 'Политик удален']);
    }
}
