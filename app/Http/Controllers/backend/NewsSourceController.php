<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\NewsSource;
use App\Traits\SearchableTrait;
use App\Traits\SortableTrait;

class NewsSourceController extends Controller
{
    use SortableTrait, SearchableTrait;

    protected $pagination_params = [];

    protected $sortable_fields = [
        'id',
        'name',
        'site_url',
        'rating'
    ];

    protected $searchable_fields = [
        'id' => '=',
        'name' => 'like',
        'site_url' => 'like',
    ];


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new NewsSource();

        $query = $this->build_search_query($query, $request);
        $query = $this->build_sort_query($query, $request);

        $news_sources = $query->paginate(10);
        $search_fields = $this->search_fields;
        $pagination_params = $this->pagination_params;
        return view('backend.news_sources.index', compact('news_sources', 'pagination_params', 'search_fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news_source = new NewsSource;
        return view('backend.news_sources.edit', compact('news_source'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news_source = new NewsSource;
        $data = $request->all();
        $data['use_fake_rating'] = !empty($data['use_fake_rating']);
        $news_source->fill($data);
        $news_source->setLogo($request);
        $news_source->save();
        return redirect(route('news_source.index'))->with(['success' => 'Источник создан']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news_source = NewsSource::findOrFail($id);
        return view('backend.news_sources.edit', compact('news_source'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var NewsSource $news_source */
        $news_source = NewsSource::findOrFail($id);
        $data = $request->all();
        $data['use_fake_rating'] = !empty($data['use_fake_rating']);
        $news_source->fill($data);
        $news_source->setLogo($request);
        $news_source->save();
        $news_source->updateRating();
        return redirect(route('news_source.index'))->with(['notification' => 'Источник обновлен']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news_source = NewsSource::findOrFail($id);
        $news_source->news()->detach();
        $news_source->delete();
        return redirect(route('news_source.index'))->with(['notification' => 'Источник удален']);
    }
}
