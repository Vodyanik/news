const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var paths = {
    'jquery': 'node_modules/jquery/',
    'bootstrap': 'node_modules/bootstrap-sass/assets/',
    'fontawesome': 'node_modules/fontawesome/',
    'quill': 'node_modules/quill/',
    'chosen': 'node_modules/chosen-js/',
}

elixir(mix => {
    mix
    .copy(paths.quill + 'dist/quill.snow.css', 'public/css')
    .copy(paths.bootstrap + 'stylesheets/', 'resources/assets/sass')
    .copy(paths.bootstrap + 'fonts/', 'public/fonts')
    .copy(paths.bootstrap + 'javascripts/bootstrap.js', 'public/js/vendor/bootstrap.js')
    .copy(paths.chosen + 'chosen.jquery.js', 'public/js')
    .copy(paths.chosen + 'chosen.css', 'public/css')
    .copy(paths.chosen + 'chosen-sprite.png', 'public/css')
    .sass('app.scss')
    .webpack('app.js');
});
