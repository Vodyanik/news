<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/backend/news/getSubjects/{id}', [
    'uses' => 'backend\NewsApiController@getSubjects',
    'as' => 'api.news.getSubjects'
]);
Route::get('/backend/news/getPromises', [
    'uses' => 'backend\NewsApiController@getPromises',
    'as' => 'api.news.getPromises'
]);
Route::get('/backend/promises/getAjaxCreateForm', [
    'uses' => 'backend\PromiseController@getAjaxCreateForm',
    'as' => 'api.promises.getAjaxCreateForm'
]);
Route::post('/backend/promises/storeAjax', [
    'uses' => 'backend\PromiseController@storeAjax',
    'as' => 'api.promises.storeAjax'
]);

Route::post('/backend/tag/storeAjax', [
    'uses' => 'backend\TagsController@storeAjax',
    'as' => 'api.tag.storeAjax'
]);

