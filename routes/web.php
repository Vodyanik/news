<?php

Route::get('/', function () {
    $news = App\News::published()->take(12)->get();
    $slider_news = App\News::forSlider()->take(6)->get();
    return view('frontend.main', compact('news', 'slider_news'));
});

Route::group(['middleware' => ['cors']], function(){
    Route::get('/ajax/news/after/{id}', function($last_shown_id){
        $news = App\News::published()->where('id', '<', $last_shown_id)->take(6)->get();
        return [
            'content' => view('frontend.partials.news_block', compact('news'))->render(),
            'last_shown_news_id' => $news->last()->id,
            'count' => $news->count()
        ];
    })->name('ajax_more_news');

    Route::get('/ajax/news/filter/{last_shown_id?}',function(Illuminate\Http\Request $request, $last_shown_id = 0){
        $last_shown_id = intval($last_shown_id);
        if ($last_shown_id < 1) {
            $last_shown_id = NULL;
        }

        $parties = $request->has('party') ? $request->get('party') : null;
        $regions = $request->has('region') ? $request->get('region') : null;
        $activities = $request->has('field') ? $request->get('field') : null;
        $organizations = $request->has('organization') ? $request->get('organization') : null;
        $confirmed_only = $request->has('confirmed_only');

        $news = \App\News::published();
        if (!empty($parties)) {
            $news = $news->whereHas('parties', function($q) use ($parties) {
                $q->whereIn('id', $parties);
            });
        }
        if (!empty($organizations)) {
            $news = $news->whereHas('organizations', function($q) use ($organizations) {
                $q->whereIn('id', $organizations);
            });
        }
        if (!empty($activities)) {
            $news = $news->whereHas('activity_fields', function($q) use ($activities) {
                $q->whereIn('id', $activities);
            });
        }
        if ($confirmed_only) {
            $news = $news->where('fact_checking_level', '=', 'high');
        }
        if(!empty($regions) && (!in_array('0', $regions))) {
            $news = $news->whereHas('regions', function($q) use ($regions) {
                $q->whereIn('id', $regions);
            });
        }

        if (!empty($last_shown_id)) {
            $news = $news->where('id', '<', $last_shown_id);
        }

        $news = $news->take(12)->get();
        return [
            'parties' => $parties,
            'regions' => $regions,
            'activities' => $activities,
            'organizations' => $organizations,
            'confirmed_only' => $confirmed_only,
            'last_shown_id' => $last_shown_id,
            'content' => view('frontend.partials.news_block', compact('news'))->render(),
            'count' => $news->count()
        ];
    })->name('filtered_news');
});



Route::get('/news/search', function (Illuminate\Http\Request $request){
    $search = $request->has('search') ? $request->get('search') : '';
    if (empty($search)) {
        return redirect('/');
    }
    $page = $request->has('page') ? intval($request->get('page')) : 1;
    if ($page < 1) $page = 1;
    $news = \App\News::published()->
        where('title', 'like', '%'.$search.'%')->
        orWhere('preview', 'like', '%'.$search.'%')->
        orWhere('body', 'like', '%'.$search.'%')->
        orWhereHas('politicians', function($q) use ($search) {
            $q->where('first_name', 'like', "%{$search}%");
        })->
        orWhereHas('parties', function($q) use ($search) {
            $q->where('name', 'like', "%{$search}%");
        })->
        orWhereHas('organizations', function($q) use ($search) {
            $q->where('name', 'like', "%{$search}%");
        })->
        forPage($page, 12);
//    return $news->toSql();
//    return $news->get();
    $news = $news->get();
    return view('frontend.news_search_result', compact('news'));
})->name('news_search');

Route::get('/news/by_tag/{tag_name}/{page?}', function ($tag_name, $page = 0){
    $page = intval($page);
    if ($page < 1) $page = 1;
    $tag = \App\Tag::where('name', '=', $tag_name)->first();
    if (!empty($tag)) {
        $news = $tag->news()->published()->
        forPage($page, 12)->get();
    } else {
        $news = [];
    }
    return view('frontend.news_search_result', compact('news'));
})->name('news_by_tag');



Route::get('/take_an_action', function () {
    return view('frontend.action');
})->name('take_an_action_page');

Route::get('/about_us', function () {
    return view('frontend.about_us');
})->name('about_us_page');

Route::get('/make_suggestion', function () {
    $suggestion = new \App\Suggestion();
    return view('frontend.suggestions.form', compact('suggestion'));
})->name('make_suggestion');
Route::resource('/frontend/suggestion', 'frontend\SuggestionsController', ['only' => 'store']);

Route::get('/opinions', function(){
    $opinions = \App\News::opinions()->get();
    return view('frontend.opinions', compact('opinions'));
})->name('opinions');

Route::post('/my_account/opinion', 'frontend\OpinionController@store')->name('post_opinion');

Route::get('/forum', function () {
    return redirect("http://forum.smartnews.in.ua/");
})->name('forum_main');

Route::get('/news_feed', 'backend\NewsController@getRSSFeed')->name('news_feed');

Route::get('/news/{slug}', function($slug){
    $news = App\News::where('slug', '=', $slug)->first();
    $comments = App\Comment::where('news_id', $news->id)->orderBy('updated_at', 'desc')->paginate(5);
    return view('frontend.single_news', compact('news', 'comments'));
})->name('news');

Route::get('news/load-more-comments/{news_id}/{page_id}', 'frontend\CommentController@ajaxLoadComments');

Route::get('/single_news/{id}', function ($id) {
    $news = App\News::findOrFail($id);
    return view('frontend.single_news', compact('news'));
})->name('single_news');
Route::get('/politician/{id}', function ($id) {
    $politician = App\Politician::findOrFail($id);
    $politicianNews = \App\News::where('title', 'LIKE', '%' . $politician->last_name . '%')->orderBy('created_at', 'desc')->get();
    return view('frontend.politician', compact('politician', 'politicianNews'));
})->name('politician');
Route::get('/party/{id}', function ($id) {
    $party = App\Party::findOrFail($id);
    return view('frontend.party', compact('party'));
})->name('party');
Route::get('/organization/{id}', function ($id) {
    $organization = App\Organization::findOrFail($id);
    $organizationNews = \App\News::where('title', 'LIKE', '%' . $organization->name . '%')->orderBy('created_at', 'desc')->get();
    return view('frontend.organization', compact('organization', 'organizationNews'));
})->name('organization');

// Reiting
Route::get('reiting', ['as' => 'index.reiting', 'uses' => 'frontend\ReitingController@index']);


Route::get('/expert/{id}', function($id){

})->name('expert_profile');

Route::get('/news/like/{news_id}', 'frontend\LikesController@like')->name('like_news');
Route::get('/news/dislike/{news_id}', 'frontend\LikesController@dislike')->name('dislike_news');

Route::group(['middleware' => ['auth']], function() {
    Route::get('/news/likes/{id}', function($id){
        $likes = \App\News::find($id)->user_like_reaction;
        return $likes;
    });
    Route::get('/my_account', function(){
        $user = auth()->user();
        $user_id = $user->id;
        $bookmarks = \App\Bookmark::get_latest();
        $my_opinions = \App\News::myOpinions()->take(6)->get();
        $tags = App\Tag::whereHas(
            'news',
            function($q1) use ($user_id){
                $q1->whereHas(
                    'likes',
                    function($q) use ($user_id) {
                        $q->where('user_id', '=', $user_id);
                    });
            })->pluck('name','id');
        return view('frontend.user', compact('user', 'tags','bookmarks','my_opinions'));
    })->name('my_account');
    Route::get('/ajax/my_account/party_stats', function(){
        $parties = App\Party::withCount(
            [
                'news' => function($q){
                    $q->whereHas(
                        'likes',
                        function($q){
                            $q->whereHas('user', function($q){
                                $q->where('id', '=', auth()->user()->id);
                            });
                        }
                    );
                }
            ]
        )->get();
        return ['keys' => $parties->pluck('name'), 'values' => $parties->pluck('news_count')];
    });
    Route::get('/ajax/my_account/bookmark/{news_id}', function($news_id){
        $result = [
            'status' => 'error'
        ];
        $user_id = auth()->user()->id;
        $bookmark = \App\Bookmark::where('user_id', '=', $user_id)->where('news_id', '=', $news_id);
        if ($bookmark->exists()){
            $deleted = $bookmark->delete();
            if ($deleted) {
                $result = [
                    'status' => 'removed'
                ];
            }
        } else {
            if (\App\News::where('id', '=', $news_id)->exists()){
                $bookmark = new \App\Bookmark;
                $bookmark->user_id = $user_id;
                $bookmark->news_id = $news_id;
                if ($bookmark->save()){
                    $result = [
                        'status' => 'created',
                    ];
                }
            }
        }
        return $result;
    });

    Route::get('comment/{id}', ['as' => 'front.comment', 'uses' => 'frontend\CommentController@store']);
});

Route::group(['middleware' => ['auth','backend_user']], function(){
    Route::get('/backend', function () {
        return view('backend.layout');
    });
    Route::resource('/backend/news_source', 'backend\NewsSourceController');
    Route::resource('/backend/news', 'backend\NewsController');
    Route::resource('/backend/tags', 'backend\TagsController');
    Route::resource('/backend/politician', 'backend\PoliticianController');
    Route::resource('/backend/party', 'backend\PartyController');
    Route::resource('/backend/organization', 'backend\OrganizationController');
    Route::resource('/backend/promise', 'backend\PromiseController');
    Route::resource('/backend/users', 'backend\UserController');
    Route::resource('/backend/news_changes_log', 'backend\ActionsLogController', ['only' => 'index']);
    Route::resource('/backend/activity_fields', 'backend\ActivityFieldController');
    Route::resource('/backend/suggestions', 'backend\SuggestionsController');
    Route::resource('/backend/comments', 'backend\CommentController');
});
Auth::routes();
